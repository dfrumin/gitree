(** Core 'computational' operations on itrees: lambda, function application, arithmetic, etc *)
From iris.algebra Require cofe_solver.
From iris.base_logic Require Export base_logic.
From iris.prelude Require Import options.
From gitree Require Import prelude ugitree.core.

Section lambda.
  Local Opaque laterO_ap Tick.

  Context {Σ : opsInterp}.
  Notation IT := (IT Σ).

  Program Definition IF : IT -n> IT -n> IT -n> IT := λne t t1 t2,
    get_nat (λne n, if Nat.ltb 0 n then t1 else t2) t.
  Next Obligation.
    intros _ t1 t2 n x y ->. done.
  Qed.
  Next Obligation. solve_proper. Qed.
  Next Obligation. solve_proper. Qed.
  Next Obligation. solve_proper. Qed.
  (* XXX How to make a proper proper instance? *)
  (* Global Instance IF_Proper : Proper ((≡) ==> (≡)) (IF). *)

  Program Definition IF_last : IT -n> IT -n> IT -n> IT := λne t1 t2 t, IF t t1 t2.
  Next Obligation. solve_proper. Qed.
  Next Obligation. solve_proper. Qed.
  Next Obligation. solve_proper. Qed.


  (** A non-strict application, does not recurse under the effects of the argument *)
  Program Definition APP : IT -n> laterO IT -n> IT := λne f x,
    get_fun (λne f, Tau $ laterO_ap f x) f.
  Next Obligation. repeat intro. by repeat f_equiv. Qed.
  Next Obligation.
    repeat intro. repeat f_equiv. intro. solve_proper.
  Qed.
  Next Obligation.
    repeat intro. repeat f_equiv.
    repeat intro.               solve_proper.
  Qed.
  Program Definition Ppa : laterO IT -n> IT -n> IT := λne x f, APP f x.
  Next Obligation. solve_proper. Qed.
  Next Obligation.
    repeat intro. repeat f_equiv. repeat intro. simpl.
    repeat f_equiv. intro. solve_proper.
  Qed.

  (** Strict version of APP *)
  Program Definition APP' : IT -n> IT -n> IT := λne f, get_val (APP f ◎ NextO).
  Next Obligation.
    intros n f1 f2 Hf. by repeat f_equiv.
  Qed.

  (** We define the interpretation of NatOp in two stages.
      First, we recurse under ticks and visitors in both arguments, and only then perform the op
   *)
  Program Definition NATOP (f : nat → nat → nat) : IT -n> IT -n> IT := λne t1 t2,
      get_val (λne v2,
        get_val (λne v1,
                   get_nat2 (λ n1 n2, Nat (f n1 n2)) v1 v2) t1) t2.
  Next Obligation. solve_proper. Qed.
  Next Obligation.
    repeat intro. repeat f_equiv. solve_proper.
  Qed.
  Next Obligation. solve_proper. Qed.
  Next Obligation.
    Opaque get_nat2 Nat.
    repeat intro. simpl. repeat f_equiv; eauto.
    intro. simpl.  solve_proper.
  Qed.

  Lemma APP_Err x : APP Err x ≡ Err.
  Proof. simpl. by rewrite get_fun_err. Qed.
  Lemma APP_Fun f x : APP (Fun f) x ≡ Tau $ laterO_ap f x.
  Proof. simpl. rewrite get_fun_fun. done. Qed.
  Lemma APP_Tick t x : APP (Tick t) x ≡ Tick $ APP t x.
  Proof.
     rewrite {1}/APP /=.
    by rewrite get_fun_tick.
  Qed.
  Lemma APP_Tick_n n t x : APP (Tick_n n t) x ≡ Tick_n n $ APP t x.
  Proof.
    induction n; eauto. rewrite APP_Tick. rewrite IHn. done.
  Qed.
  Lemma APP_Vis op i k x : APP (Vis op i k) x ≡ Vis op i (laterO_map (Ppa x) ◎ k).
  Proof.
    rewrite {1}/APP /=.
    rewrite get_fun_vis. repeat f_equiv.
    intro f. simpl. reflexivity.
  Qed.
  Lemma APP'_Err f : APP' f Err ≡ Err.
  Proof. simpl. by rewrite get_val_err. Qed.
  Lemma APP'_Nat f x : APP' f (Nat x) ≡ APP f (Next (Nat x)).
  Proof. simpl. rewrite get_val_nat. done. Qed.
  Lemma APP'_Fun f x : APP' f (Fun x) ≡ APP f (Next (Fun x)).
  Proof. simpl. rewrite get_val_fun. done. Qed.
  Lemma APP'_Tick_r f t : APP' f (Tick t) ≡ Tick $ APP' f t.
  Proof. by rewrite get_val_tick. Qed.
  Lemma APP'_Tick_r_n  f n t : APP' f (Tick_n n t) ≡ Tick_n n $ APP' f t.
  Proof.
    induction n; eauto. by rewrite APP'_Tick_r IHn.
  Qed.
  Lemma APP'_Vis_r f op i k : APP' f (Vis op i k) ≡ Vis op i (laterO_map (APP' f) ◎ k).
  Proof. by rewrite get_val_vis. Qed.
  Lemma APP_APP'_ITV' α (βv : ITV Σ) :
    APP' α (IT_of_V βv) ≡ APP α (Next (IT_of_V βv)).
  Proof.
    destruct βv as [n|f]; simpl.
    - rewrite APP'_Nat//.
    - rewrite APP'_Fun//.
  Qed.
  (* XXX: the names here are weird *)
  Lemma APP_APP'_ITV α β :
    AsVal β → APP' α β ≡ APP α (Next β).
  Proof.
    intros [βv <-].
    rewrite APP_APP'_ITV'//.
  Qed.

  Lemma IF_Err t1 t2 : IF Err t1 t2 ≡ Err.
  Proof. unfold IF. simpl. by rewrite get_nat_err. Qed.
  Lemma IF_True n t1 t2 :
    0 < n → IF (Nat n) t1 t2 ≡ t1.
  Proof.
    intro Hn. unfold IF. simpl.
    rewrite get_nat_nat.
    assert (0 <? n = true) as ->; last by eauto.
    by apply Nat.ltb_lt.
  Qed.
  Lemma IF_False n t1 t2 :
    0 ≥ n → IF (Nat n) t1 t2 ≡ t2.
  Proof.
    intro Hn. unfold IF. simpl.
    rewrite get_nat_nat.
    assert (0 <? n = false) as ->; last by eauto.
    by apply Nat.ltb_ge.
  Qed.
  Lemma IF_Tick t t1 t2 :
    IF (Tick t) t1 t2 ≡ Tick (IF t t1 t2).
  Proof. rewrite {1}/IF /=. apply get_nat_tick. Qed.
  Lemma IF_Tick_n n t t1 t2 :
    IF (Tick_n n t) t1 t2 ≡ Tick_n n (IF t t1 t2).
  Proof.
    induction n; eauto. by rewrite IF_Tick IHn.
  Qed.
  Lemma IF_Vis op i k t1 t2 :
    IF (Vis op i k) t1 t2 ≡ Vis op i (laterO_map (IF_last t1 t2) ◎ k).
  Proof.
    rewrite {1}/IF /=.
    rewrite get_nat_vis. repeat f_equiv.
    by intro.
  Qed.

  Lemma NATOP_Err_r f t1 : NATOP f t1 Err ≡ Err.
  Proof. simpl. by rewrite get_val_err. Qed.
  Lemma NATOP_Err_l f β : AsVal β → NATOP f Err β ≡ Err.
  Proof.
    intros ?. simpl.
    rewrite get_val_ITV /= get_val_err //.
  Qed.
  Lemma NATOP_Nat n1 n2 f :
    NATOP f (Nat n1) (Nat n2) ≡ Nat (f n1 n2).
  Proof.
    simpl.
    rewrite get_val_nat/= get_val_nat/=.
    Transparent get_nat2.
    rewrite /get_nat2/=.
    by rewrite !get_nat_nat.
    Opaque get_nat2.
  Qed.
  Lemma NATOP_Tick_r t1 t2 f :
    NATOP f t1 (Tick t2) ≡ Tick $ NATOP f t1 t2.
  Proof.
    simpl. rewrite get_val_tick//.
  Qed.
  Lemma NATOP_Tick_n_r t1 t2 f n :
    NATOP f t1 (Tick_n n t2) ≡ Tick_n n $ NATOP f t1 t2.
  Proof.
    induction n; eauto. rewrite NATOP_Tick_r.
    rewrite IHn. done.
  Qed.
  Lemma NATOP_ITV_Tick_l t1 β f :
    AsVal β →
    NATOP f (Tick t1) β ≡ Tick $ NATOP f t1 β.
  Proof.
    intros ?. simpl. rewrite get_val_ITV/=.
    rewrite get_val_tick. f_equiv.
    rewrite get_val_ITV. done.
  Qed.
  Lemma NATOP_ITV_Tick_n_l t1 β f n :
    AsVal β →
    NATOP f (Tick_n n t1) β ≡ Tick_n n $ NATOP f t1 β.
  Proof.
    intros Hb.
    induction n; eauto. rewrite NATOP_ITV_Tick_l.
    rewrite IHn. done.
  Qed.
  Lemma NATOP_Vis_r t1 op i k f :
    NATOP f t1 (Vis op i k) ≡ Vis op i (laterO_map (NATOP f t1) ◎ k).
  Proof.
    simpl. rewrite get_val_vis. f_equiv. solve_proper.
  Qed.
  Lemma NATOP_ITV_Vis_l op i k β f :
    AsVal β →
    NATOP f (Vis op i k) β ≡
    Vis op i (laterO_map (flipO (NATOP f) β) ◎ k).
  Proof.
    intros ?. simpl. rewrite get_val_ITV/=.
    rewrite get_val_vis. repeat f_equiv.
    intro y. simpl. rewrite get_val_ITV//.
  Qed.

  Global Instance IF_Proper : Proper ((≡) ==> (≡)) IF.
  Proof. apply ne_proper. apply _. Qed.
  Global Instance APP_Proper : Proper ((≡) ==> (≡)) APP.
  Proof. apply ne_proper. apply _. Qed.
  Global Instance APP'_Proper : Proper ((≡) ==> (≡)) APP'.
  Proof. apply ne_proper. apply _. Qed.
  Global Instance NATOP_Proper f : Proper ((≡) ==> (≡)) (NATOP f).
  Proof. apply ne_proper. apply _. Qed.
  (* XXX!!! *)
  (* Global Instance APP_Proper : Proper ((≡) ==> (≡) ==> (≡)) APP. *)

End lambda.
Opaque APP APP' IF.
