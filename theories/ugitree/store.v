From iris.algebra Require Import gmap.
From iris.algebra Require cofe_solver.
From iris.base_logic Require Export base_logic.
From iris.prelude Require Import options.
From iris.heap_lang Require Import locations.
From gitree Require Import prelude ugitree.core.

Opaque laterO_map.
(* why do i need to repeat this?*)
Opaque Nat Fun Tau Err Vis.

(** * Example opInterps **)
Notation locO := (leibnizO loc).

Program Definition NewLocΣ : opInterp := {|
  Ins := (▶ ∙)%OF;
  Outs := locO;
|}.
Program Definition LoadΣ : opInterp :=  {|
  Ins := locO;
  Outs := (▶ ∙)%OF;
|}.
Program Definition StoreΣ : opInterp :=  {|
  Ins := (locO * ▶ ∙)%OF;
  Outs := unitO;
|}.

Definition storeΣ : opsInterp := @[NewLocΣ; LoadΣ; StoreΣ].
Section constructors.
  Notation IT := (IT storeΣ).
  Definition stateO := locO -n> IT.
  Program Definition update_state : stateO -n> locO -n> IT -n> stateO
    := λne s l v l', if (decide (l = l')) then v else s l'.
  Next Obligation.
    intros s l v n l1 l2 ->. case_decide; auto.
  Qed.
  Next Obligation. solve_proper. Qed.
  Next Obligation.
    repeat intro; simpl. repeat case_decide; simplify_eq/=; solve_proper.
  Qed.
  Next Obligation.
    repeat intro; simpl. repeat case_decide; simplify_eq/=; solve_proper.
  Qed.
  Opaque update_state.

  Notation stateM := ((stateO -n> prodO stateO IT)).
  Local Instance stateT_inhab : Inhabited stateM.
  Proof.
    simple refine (populate (λne s, (s, Err))).
    solve_proper.
  Qed.
  Instance stateM_cofe : Cofe stateM.
  Proof. unfold stateM. apply _. Qed.

  Program Definition reify_vis ( op : opid storeΣ ) :
   oFunctor_car (Ins (storeΣ op)) (sumO IT stateM) (prodO IT stateM) -n>
     (oFunctor_car (Outs (storeΣ op)) (prodO IT stateM) (sumO IT stateM) -n> laterO (prodO IT stateM)) -n> stateM.
  Proof.
    unfold opid in op. simpl in op.
    set (P:=λ (op : fin 3), oFunctor_car (Ins (storeΣ op)) (sumO IT stateM) (prodO IT stateM) -n> (oFunctor_car (Outs (storeΣ op)) (prodO IT stateM) (sumO IT stateM) -n> laterO (prodO IT stateM)) -n> stateM).
    apply (Fin.caseS' op P); unfold P; clear P.
    { simpl. (* newloc ignore *)
      simple refine (λne i k s, (s, Vis (Fin.F1 : opid storeΣ) (Orig_Vis_Inp _ _ i) (Orig_Vis_Cont _ _ k))).
      + apply _.
      + solve_proper.
      + solve_proper.
      + apply _.
      + solve_proper. }
    clear op. intro op.
    set (P:=λ (op : fin 2), oFunctor_car (Ins (storeΣ (FS op))) (sumO IT stateM) (prodO IT stateM) -n> (oFunctor_car (Outs (storeΣ (FS op))) (prodO IT stateM) (sumO IT stateM) -n> laterO (prodO IT stateM)) -n> stateM).
    apply (Fin.caseS' op P); unfold P; clear P.
    { (* load *)
      simpl.
      simple refine (λne (l : locO) (k : laterO (sum IT stateM) -n> _) (s : stateO),
                        let t := s l in (s, _)).
      - simple refine (Tau $ laterO_map fstO $ k (Next $ inlO t)).
      - solve_proper.
      - solve_proper.
      - solve_proper.
    }
    clear op. intro op.
    set (P:=λ (op : fin 1), oFunctor_car (Ins (storeΣ (FS (FS op)))) (sumO IT stateM) (prodO IT stateM) -n> (oFunctor_car (Outs (storeΣ (FS (FS op)))) (prodO IT stateM) (sumO IT stateM) -n> laterO (prodO IT stateM)) -n> stateM).
    apply (Fin.caseS' op P); unfold P; clear P.
    { (* store *) simpl.
      simple refine (λne (lv : prodO _ _) (k : unitO -n> _) (s : stateO),
                      let l := fstO lv in
                      let v := laterO_map fstO $ sndO lv in
                      _
                    ).
      - simple refine (update_state s l (Tau v), Tau $ laterO_map fstO $ k ()).
      - solve_proper.
      - solve_proper.
      - solve_proper.
    }
    apply Fin.case0.
  Defined.


  Program Definition reify_fun : laterO (sumO IT stateM -n> prodO IT stateM) -n> stateM :=
    λne f s, (s, Fun $ laterO_map (λne f, fstO ◎ f ◎ inlO) f).
  Next Obligation. solve_proper. Qed.
  Next Obligation. solve_proper. Qed.
  Next Obligation. solve_proper. Qed.


  Program Definition reify_tau : laterO (prodO IT stateM) -n> stateM :=
    λne x s, (s, Tau $ laterO_map fstO x).
  Next Obligation. solve_proper. Qed.
  Next Obligation. solve_proper. Qed.


  Program Definition unr : stateM -n>
    sumO (sumO (sumO (sumO natO unitO) (laterO (stateM -n> stateM))) (laterO stateM))
      (sigTO (λ op : opid storeΣ, prodO (oFunctor_apply (Ins (storeΣ op)) stateM) (oFunctor_apply (Outs (storeΣ op)) stateM -n> laterO stateM))).
  Proof. simple refine (λne d, inl (inl (inl (inr ())))). Qed.

  Program Definition reify_err : stateM := λne s, (s, Err).
  Next Obligation. solve_proper. Qed.

  Program Definition reify_nat : natO -n> stateM := λne n s, (s, Nat n).
  Next Obligation. solve_proper. Qed.
  Next Obligation. solve_proper. Qed.

  Definition reify : IT -n> stateM
    := IT_rec1 _
               reify_err
               reify_nat
               reify_fun
               reify_tau
               reify_vis
               unr.
End constructors.

Definition oFunctor_transp {F G : oFunctor} (p : F = G) X `{Cofe X} : oFunctor_apply F X -n> oFunctor_apply G X :=
  eq_rect _ (λ G, oFunctor_apply F X -n> oFunctor_apply G X) idfun G p.

Definition vis {Σ : opsInterp} (I : opInterp) {H : inO Σ I} :
  oFunctor_apply (Ins I) (IT Σ) -n>
  (oFunctor_apply (Outs I) (IT Σ) -n> laterO (IT Σ)) -n>
    IT Σ.
Proof.
  set (op:=inO_id H).
  set (Ho:=inO_prf (I:=I)).
  simple refine (λne i k, Vis op _ _).
  - apply (oFunctor_transp (F:=(Ins I))).
    { exact (ap _ Ho). }
    simpl. exact i.
  - apply (ccompose k).
    apply (oFunctor_transp (G:=(Outs I))).
    { exact (ap _ (sym Ho)). }
  - solve_proper.
  - solve_proper.
Defined.

Definition NewLoc {Σ : opsInterp} {H : inO Σ NewLocΣ} :
  (laterO (IT Σ)) -n> (locO -n> laterO (IT Σ)) -n> IT Σ
  := vis NewLocΣ.

Lemma get_fun_NewLoc {Σ : opsInterp} {H : inO Σ NewLocΣ} x k f :
  get_fun f (NewLoc x k) ≡ NewLoc x (laterO_map (get_fun f) ◎ k).
Proof.
  rewrite /NewLoc /vis get_fun_vis. simpl.
  by rewrite ccompose_assoc.
Qed.
