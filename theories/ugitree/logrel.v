From Equations Require Import Equations.
From iris.proofmode Require Import classes tactics.
From iris.si_logic Require Export bi siprop.
From gitree Require Import prelude.
From gitree.ugitree Require Import core lambda simple_io.

Opaque Nat Fun Tau Err Vis.
Opaque APP APP' IF NATOP.
Opaque INPUT.
Opaque Tick.

Section logrel.
  Notation IT := (IT ioΣ).
  Notation ITV := (ITV ioΣ).
  Canonical Structure exprO S := leibnizO (expr S).
  Canonical Structure valO S := leibnizO (val S).

  Program Definition logrel_expr_pre {S}
          (logrel_val : valO S -n> IT -n> siProp)
          (self : exprO S -n> IT -n> stateO -n> siProp)
    : exprO S -n> IT -n> stateO -n> siProp := λne e α σ,
      ( (∃ (v : val S), ⌜prim_steps e σ (Val v) σ (0,0)⌝ ∧ logrel_val v α)
      ∨ (∃ (β : IT) e1 e2,
           α ≡ Tick β ∧ ⌜prim_steps e σ e1 σ (0,0)⌝
                      ∧ ⌜prim_step e1 σ e2 σ (1,0)⌝
                      ∧  ▷ self e2 β σ)
      ∨ (∃ k (β : IT) e1 e2 σ2,
           α ≡ INPUT k ∧
           reify (INPUT k) σ ≡ (σ2, Tick β) ∧ ⌜prim_steps e σ e1 σ (0,0)⌝
                                    ∧ ⌜prim_step e1 σ e2 σ2 (1,1)⌝
                                    ∧  ▷ self e2 β σ2))%I.
  Next Obligation. solve_proper. Qed.
  Next Obligation. solve_proper. Qed.
  Next Obligation. solve_proper. Qed.

  Global Instance logrel_expr_pre_contractive {S} V : Contractive (@logrel_expr_pre S V).
  Proof. solve_contractive. Qed.

  Definition logrel_expr {S} V := fixpoint (@logrel_expr_pre S V).

  Lemma logrel_expr_unfold {S} (e : expr S) V σ α :
  logrel_expr V e α σ ≡
      ( (∃ (v : val S), ⌜prim_steps e σ (Val v) σ (0,0)⌝ ∧ V v α)
      ∨ (∃ (β : IT) e1 e2,
           α ≡ Tick β ∧ ⌜prim_steps e σ e1 σ (0,0)⌝
                      ∧ ⌜prim_step e1 σ e2 σ (1,0)⌝
                      ∧  ▷ logrel_expr V e2 β σ)
      ∨ (∃ k (β : IT) e1 e2 σ2,
           α ≡ INPUT k ∧
           reify (INPUT k) σ ≡ (σ2, Tick β) ∧ ⌜prim_steps e σ e1 σ (0,0)⌝
                                    ∧ ⌜prim_step e1 σ e2 σ2 (1,1)⌝
                                    ∧  ▷ logrel_expr V e2 β σ2))%I.
  Proof.
    unfold logrel_expr. apply (fixpoint_unfold (logrel_expr_pre V)).
  Qed.

  Program Definition logrel_nat {S} : valO S -n> IT -n> siProp :=
    λne v α, (∃ (n : nat), ⌜v = Lit n⌝ ∧ α ≡ Nat n)%I.
  Next Obligation. solve_proper. Qed.
  Next Obligation. solve_proper. Qed.


Lemma logrel_expr_step_zero {S} V (e1 e2 : expr S) σ1 σ2 :
  prim_step e1 σ1 e2 σ2 (0,0) →
  (⊢ ∀ α, logrel_expr V e2 α σ2 → logrel_expr V e1 α σ1)%I.
Proof.
  intros Hst. assert (σ2 = σ1) as ->.
  { symmetry. eapply prim_step_pure; eauto. }
  iIntros (α) "H".
  rewrite {1}logrel_expr_unfold.
  iDestruct "H" as "[H|[H|H]]".
  - iDestruct "H" as (v) "[%Hst2 Hv]".
    rewrite logrel_expr_unfold.
    iLeft. iExists v. iSplit; eauto.
    iPureIntro. change 0 with (0+0). econstructor; eauto.
 - iDestruct "H" as (β u1 u2) "(H & %Hst1 & %Hst2 & Hl)".
    rewrite (logrel_expr_unfold e1).
    iRight. iLeft.
    iExists β,u1,u2. repeat iSplit; eauto.
    iPureIntro. change 0 with (0+0). econstructor; eauto.
  - iDestruct "H" as (k β u1 u2 σ') "(Hk & H & %Hst1 & %Hst2 & Hl)".
    rewrite (logrel_expr_unfold e1).
    iRight. iRight.
    iExists k,β,u1,u2,σ'. repeat iSplit; eauto.
    iPureIntro. change 0 with (0+0). econstructor; eauto.
Qed.

Lemma logrel_expr_steps_zero {S} V (e1 e2 : expr S) σ1 σ2 :
  prim_steps e1 σ1 e2 σ2 (0,0) →
  (⊢ ∀ α, logrel_expr V e2 α σ2 → logrel_expr V e1 α σ1)%I.
Proof.
  remember (0,0) as nm.
  intros Hst.
  induction Hst; eauto. simplify_eq/=.
  iIntros (α) "H".
  iPoseProof (IHHst with "H") as "H".
  { f_equal; lia. }
  iApply (logrel_expr_step_zero with "H"); eauto.
  assert (n1 = 0) as -> by lia. (*  eauto using Nat.eq_add_0 *)
  assert (m1 = 0) as -> by lia.
  eauto.
Qed.

Lemma logrel_expr_step_tick {S} V (e1 e2 : expr S) σ :
  prim_step e1 σ e2 σ (1,0) →
  (⊢ ∀ α, ▷ logrel_expr V e2 α σ → logrel_expr V e1 (Tick α) σ)%I.
Proof.
  intros Hst. iIntros (α) "H".
  rewrite (logrel_expr_unfold e1).
  iRight. iLeft. iExists α,_,_. iFrame "H".
  repeat iSplit; eauto. iPureIntro.
  by econstructor.
Qed.

(** Definition by recursion on the type *)
Program Fixpoint logrel_val {S} (τ : ty) : valO S -n> IT -n> siProp :=
  match τ with
  | Tnat => logrel_nat
  | Tarr τ1 τ2 => λne vf α, ∃ f, α ≡ Fun f ∧
      ∀ v β σ, logrel_val τ1 v β → logrel_expr (logrel_val τ2) (App (Val vf) (Val v)) (APP' α β) σ
  end%I.
Next Obligation.
  repeat intro. repeat (f_equiv; intro).
  by repeat f_equiv.
Defined.
Next Obligation.
  repeat intro. simpl. repeat (f_equiv; intro).
  by  repeat f_equiv.
Defined.

Definition logrel {S} (τ : ty) : exprO S -n> IT -n> stateO -n> siProp := logrel_expr (logrel_val τ).

Lemma logrel_adequate {S} α (e : expr S) σ σ' k n m :
  ssteps α σ (Nat k) σ' (n,m) →
  (⊢ logrel Tnat e α σ)%I →
  prim_steps e σ (Val (Lit k)) σ' (n,m).
Proof.
  intros Hst Hlog.
  cut (⊢ ▷^n ⌜prim_steps e σ (Val (Lit k)) σ' (n,m)⌝ : siProp)%I.
  { intros HH.
    eapply siProp.pure_soundness.
    by eapply (laterN_soundness _ n). }
  iPoseProof Hlog as "#H". iRevert "H". clear Hlog.
  iRevert (Hst).
  iInduction n as [|n] "IH" forall (e α σ m); iIntros (Hst) "#H".
  - inversion Hst; simplify_eq/=. iAssert (α ≡ Nat k)%I as "Ha"; first by eauto.
    iRewrite "Ha" in "H".
    rewrite logrel_expr_unfold.
    iDestruct "H" as "[H|[H|H]]".
    (* only the first case matters *)
    -- iDestruct "H" as (v) "[%Hst2 Hv]".
       simpl. iDestruct "Hv" as (k' ->) "Hk".
       iPoseProof (Nat_inj' with "Hk") as "%".
       fold_leibniz. subst k'.
       eauto.
    -- iDestruct "H" as (β u1 u2) "(H & %Hst1 & %Hst2 & Hl)".
       iExFalso. iApply (IT_nat_tick_ne with "H").
    -- iDestruct "H" as (ko β u1 u2 σ2) "(Hk & H & %Hst1 & %Hst2 & Hl)".
       iExFalso. iApply (IT_nat_vis_ne with "Hk").
  - inversion_clear Hst; simplify_eq/=.
    induction H as [α β σ Ha Hss | α β ko σ1 σ2 Ha Hσ Hss]; simplify_eq/=.
    -- rewrite logrel_expr_unfold. iDestruct "H" as "[H|[H|H]]".
       (* only the second case matters *)
       * iDestruct "H" as (v) "[%Hst2 Hv]".
         rewrite Ha.
         simpl. iDestruct "Hv" as (k' ->) "Hk".
         iExFalso. iApply (IT_nat_tick_ne k' β).
         by iApply internal_eq_sym.
       * iDestruct "H" as (β' u1 u2) "(H & %Hst1 & %Hst2 & Hl)".
         rewrite Ha.
         iNext. iRewrite -"H" in  "Hl". iSpecialize ("IH" with "[//] Hl").
         iNext. iDestruct "IH" as %IH. iPureIntro.
         eapply (prim_steps_app (0,0) (1+n,m2)); eauto.
         eapply (prim_steps_app (1,0) (n,m2)); eauto.
         by apply prim_step_steps.
       * iDestruct "H" as (ko β' u1 u2 σ'2) "(Hk & H & %Hst1 & %Hst2 & Hl)".
         rewrite Ha.
         iExFalso. iApply (IT_tick_vis_ne with "Hk").
    -- rewrite logrel_expr_unfold. iDestruct "H" as "[H|[H|H]]".
       (* only the third case matters *)
       * iDestruct "H" as (v) "[%Hst2 Hv]".
         rewrite Ha.
         simpl. iDestruct "Hv" as (k' ->) "Hk".
         iExFalso. iApply (IT_nat_vis_ne k').
         by iApply internal_eq_sym.
       * iDestruct "H" as (β' u1 u2) "(H & %Hst1 & %Hst2 & Hl)".
         rewrite Ha.
         iExFalso. iApply (IT_tick_vis_ne β').
         by iApply internal_eq_sym.
       * iDestruct "H" as (ko' β' u1 u2 σ'2) "(Hk & H & %Hst1 & %Hst2 & Hl)".
         iAssert ((σ'2, Tick β') ≡ (σ2, Tick β))%I as "#He".
         { iRewrite -"H". iRewrite -"Hk".
           rewrite -Hσ. iPureIntro. solve_proper. }
         iPoseProof (prod_equivI with "He") as "[He1 He2]". simpl.
         iNext. iRewrite "He2" in "Hl". iRewrite "He1" in "Hl".
         iSpecialize ("IH" with "[] Hl").
         { eauto. }
         iNext. iDestruct "IH" as %IH.
         iDestruct "He1" as %->.
         iPureIntro.
         eapply (prim_steps_app (0,0) (1+n,1+m2)); eauto.
         eapply (prim_steps_app (1,1) (n,m2)); eauto.
         by apply prim_step_steps.
Qed.

Lemma logrel_of_val {S} τ (v : val S) α σ :
  ⊢ logrel_val τ v α → logrel τ (Val v) α σ.
Proof.
  iIntros "H". rewrite logrel_expr_unfold.
  iLeft. iExists v. iFrame. iPureIntro.
  constructor.
Qed.

(** Everything that we related at the value relation are semantic values *)
Lemma logrel_val_ITV {S} τ (v : val S) α :
  ⊢ logrel_val τ v α → ∃ βv, α ≡ IT_of_V βv.
Proof.
  destruct τ; simpl.
  - iDestruct 1 as (n ->) "H". iRewrite "H".
    iExists (NatV n). done.
  - iDestruct 1 as (n) "[H _]". iRewrite "H".
    iExists (FunV n). done.
Qed.

(** We also have a separate description of "semantic contexts" *)
Inductive sectx_item :=
| AppLSCtx : ITV → sectx_item
| AppRSCtx : IT → sectx_item
| NatOpLSCtx : nat_op → ITV → sectx_item
| NatOpRSCtx : nat_op → IT → sectx_item
| IfSCtx : IT → IT → sectx_item
.
Definition sectx := list (sectx_item).

Definition fill_sectx_item (K' : sectx_item) : IT → IT := λ α,
  match K' with
  | AppLSCtx v => APP' α (IT_of_V v)
  | AppRSCtx β => APP' β α
  | NatOpLSCtx op βv => NATOP (do_natop op) α (IT_of_V βv)
  | NatOpRSCtx op β => NATOP (do_natop op) β α
  | IfSCtx β1 β2 => IF α β1 β2
  end.
Definition fill_sectx (K : sectx) (α : IT)  : IT := foldl (flip fill_sectx_item) α K.
Global Instance fill_sectx_item_ne Ks : NonExpansive (fill_sectx_item Ks).
Proof. solve_proper. Qed.
Global Instance fill_sectx_ne Ks : NonExpansive (fill_sectx Ks).
Proof. induction Ks; solve_proper. Qed.
Global Instance fill_sectx_item_proper Ks : Proper ((≡) ==> (≡)) (fill_sectx_item Ks).
Proof. solve_proper. Qed.
Global Instance fill_sectx_proper Ks : Proper ((≡) ==> (≡)) (fill_sectx Ks).
Proof. induction Ks; solve_proper. Qed.
Definition fill_sectx_itemO K : IT -n> IT := OfeMor (fill_sectx_item K).
Definition fill_sectxO K : IT -n> IT := OfeMor (fill_sectx K).

Lemma fill_sectx_item_tick Ksi α :
  fill_sectx_item Ksi (Tick α) ≡ Tick $ fill_sectx_item Ksi α.
Proof.
  destruct Ksi; cbn-[Tick].
  - rewrite !APP_APP'_ITV.
    apply APP_Tick.
  - apply APP'_Tick_r.
  - apply NATOP_ITV_Tick_l.
  - apply NATOP_Tick_r.
  - apply IF_Tick.
Qed.

Lemma fill_sectx_tick Ks α :
  fill_sectx Ks (Tick α) ≡ Tick $ fill_sectx Ks α.
Proof.
  revert α. induction Ks; intro α; simpl; first done.
  rewrite -IHKs. f_equiv.
  apply fill_sectx_item_tick.
Qed.

Lemma fill_sectx_item_INPUT Ks k :
  fill_sectx_item Ks (INPUT k) ≡ INPUT (laterO_map (fill_sectx_itemO Ks) ◎ k).
Proof.
  destruct Ks; cbn-[INPUT IF].
  - rewrite !APP_APP'_ITV.
    rewrite APP_INPUT. f_equiv.
    intro. cbn-[laterO_map]. repeat f_equiv.
    intro. simpl. by rewrite APP_APP'_ITV.
  - rewrite APP'_INPUT. f_equiv. solve_proper.
  - rewrite NATOP_INPUT_l. f_equiv. solve_proper.
  - rewrite NATOP_INPUT_r. f_equiv. solve_proper.
  - rewrite IF_INPUT. f_equiv. solve_proper.
Qed.

Lemma fill_sectx_INPUT Ks k :
  fill_sectx Ks (INPUT k) ≡ INPUT (laterO_map (fill_sectxO Ks) ◎ k).
Proof.
  revert k. induction Ks as [|Ki Ks]; intro k; simpl.
  - f_equiv. intro x. symmetry. eapply laterO_map_id.
  - trans (INPUT (laterO_map (fill_sectxO Ks) ◎ (laterO_map (fill_sectx_itemO Ki) ◎ k))); last first.
    { f_equiv. intro x. simpl. rewrite -later_map_compose.
      f_equiv. }
    rewrite -IHKs. f_equiv.
    apply fill_sectx_item_INPUT.
Qed.

Lemma logrel_bind {S} (K : ectx S) (Ks : sectx) e α τ1 V2 σ :
  ⊢ logrel_expr (logrel_val τ1) e α σ →
    (∀ σ' v βv, logrel_val τ1 v (IT_of_V βv) →
                    logrel_expr V2 (fill K (Val v)) (fill_sectx Ks (IT_of_V βv)) σ') →
    logrel_expr V2 (fill K e) (fill_sectx Ks α) σ.
Proof.
  iIntros "H1 H2".
  iLöb as "IH" forall (e α σ).
  rewrite {1}(logrel_expr_unfold e).
  iDestruct "H1" as "[H1 | [H1 | H1]]".
  - iDestruct "H1" as (v1 Hst1) "H1". simpl.
    iApply (logrel_expr_steps_zero _ _ (fill K (Val v1))).
    { by apply (prim_steps_ctx K). }
    destruct τ1.
    + iSimpl in "H1".
      iDestruct "H1" as (n ->) "H1". iRewrite "H1".
      iApply ("H2" $! _ (Lit n) (NatV n) with "[]").
      iExists n. eauto.
    + iSimpl in "H1".
      iDestruct "H1" as (f) "[#Hf H1]". iRewrite "Hf".
      iApply ("H2" $! _  v1 (FunV f) with "[H1]").
      simpl. iExists f. iRewrite "Hf" in "H1". iSplit; eauto.
  - iDestruct "H1" as (β1 u1 t1) "(Ha1 & % & % & H1)".
    rewrite {1}(logrel_expr_unfold (fill K _)).
    iRight. iLeft.
    iExists (fill_sectx Ks β1), (fill K u1), (fill K t1).
    iRewrite "Ha1". repeat iSplit; eauto.
    + iPureIntro. apply fill_sectx_tick.
    + iPureIntro. by apply (prim_steps_ctx K).
    + iPureIntro. by apply (prim_step_ctx K).
    + iNext. iApply ("IH" with "H1 H2").
  - iDestruct "H1" as (k β1 u1 t1 σ2) "(#Hk & Ha1 & % & % & H1)".
    rewrite {1}(logrel_expr_unfold (fill K _)).
    iRight. iRight.
    iExists (laterO_map (fill_sectxO Ks) ◎ k), (fill_sectx Ks β1), (fill K u1), (fill K t1), σ2.
    iRewrite "Hk".
    repeat iSplit; eauto.
    + iPureIntro. apply fill_sectx_INPUT.
    + iApply (reify_input_cont _ (fill_sectxO Ks) with "Ha1").
    + iPureIntro. by apply (prim_steps_ctx K).
    + iPureIntro. by apply (prim_step_ctx K).
    + iNext. iApply ("IH" with "H1 H2").
Qed.

(** compatibility lemmas *)

(* a matching list of closing substitutions *)
Inductive subs2 : scope → Type :=
| emp_subs2 : subs2 []
| cons_subs2 {S} : val [] → ITV → subs2 S → subs2 (()::S)
.

Equations subs_of_subs2 {S} (ss : subs2 S) : subs S [] :=
  subs_of_subs2 emp_subs2 v => idsub v;
  subs_of_subs2 (cons_subs2 t α ss) Vz := Val t;
  subs_of_subs2 (cons_subs2 t α ss) (Vs v) := subs_of_subs2 ss v.

Equations its_of_subs2 {S} (ss : subs2 S) : interp_scope S :=
  its_of_subs2 emp_subs2 := ();
  its_of_subs2 (cons_subs2 t α ss) := (IT_of_V α, its_of_subs2 ss).

Equations list_of_subs2 {S} (ss : subs2 S) : list (val []*ITV) :=
  list_of_subs2 emp_subs2 := [];
  list_of_subs2 (cons_subs2 v α ss) := (v,α)::(list_of_subs2 ss).

Lemma subs_of_emp_subs2 : subs_of_subs2 emp_subs2 ≡ idsub.
Proof. intros v. dependent elimination v. Qed.

Definition subs2_valid {S} (Γ : tyctx S) (ss : subs2 S) : siProp :=
  ([∧ list] τx ∈ zip (list_of_tyctx Γ) (list_of_subs2 ss),
    logrel_val (τx.1) (τx.2.1) (IT_of_V τx.2.2))%I.

Definition logrel_valid {S} (Γ : tyctx S) (e : expr S) (α : interp_scope S -n> IT) (τ : ty) : siProp :=
  (∀ ss σ, subs2_valid Γ ss → logrel τ
                                     (subst_expr e (subs_of_subs2 ss))
                                     (α (its_of_subs2 ss))
                                     σ)%I.

Lemma compat_var {S} (Γ : tyctx S) (x : var S) τ :
  typed_var Γ x τ → ⊢ logrel_valid Γ (Var x) (interp_var x) τ.
Proof.
  intros Hx. iIntros (ss σ) "Hss".
  simp subst_expr.
  iInduction Hx as [|Hx] "IH".
  - dependent elimination ss. simp subs_of_subs2.
    simp interp_var. rewrite /subs2_valid.
    simp list_of_tyctx list_of_subs2 its_of_subs2. simpl.
    iDestruct "Hss" as "[Hv Hss]". by iApply logrel_of_val.
  - dependent elimination ss. simp subs_of_subs2.
    simp interp_var. rewrite /subs2_valid.
    simp list_of_tyctx list_of_subs2 its_of_subs2. simpl.
    iDestruct "Hss" as "[Hv Hss]". by iApply "IH".
Qed.

Lemma compat_if {S} (Γ : tyctx S) (e0 e1 e2 : expr S) α0 α1 α2 τ :
  ⊢ logrel_valid Γ e0 α0  Tnat→
    logrel_valid Γ e1 α1 τ →
    logrel_valid Γ e2 α2 τ →
    logrel_valid Γ (If e0 e1 e2) (interp_if α0 α1 α2) τ.
Proof.
  iIntros "H0 H1 H2". iIntros (ss σ) "#Hss".
  iSpecialize ("H0" $! ss σ with "Hss").
  pose (s := (subs_of_subs2 ss)). fold s.
  simp subst_expr. simpl.
  iApply (logrel_bind [IfCtx (subst_expr e1 s) (subst_expr e2 s)]
                       [IfSCtx (α1 (its_of_subs2 ss)) (α2 (its_of_subs2 ss))] with "H0").
  iIntros (σ' v βv). iDestruct 1 as (n ->) "Hn".
  iRewrite "Hn". simpl.
  destruct (decide (0 < n)).
  - iApply (logrel_expr_step_zero _ _ (subst_expr e1 s)).
    { apply (Ectx_step' []). constructor; auto. }
    iAssert (internal_eq (IF (Nat n) (α1 (its_of_subs2 ss)) (α2 (its_of_subs2 ss))) (α1 _)) as "Hi".
    { iPureIntro. apply (IF_True n _ _). auto. }
    iRewrite "Hi". by iApply "H1".
  - iApply (logrel_expr_step_zero _ _ (subst_expr e2 s)).
    { apply (Ectx_step' []). constructor; lia. }
    iAssert (internal_eq (IF (Nat n) (α1 (its_of_subs2 ss)) (α2 (its_of_subs2 ss))) (α2 _)) as "Hi".
    { iPureIntro. apply (IF_False n (α1 (its_of_subs2 ss)) (α2 (its_of_subs2 ss))). lia. }
    iRewrite "Hi". by iApply "H2".
Qed.

Lemma compat_recv {S} Γ (e : expr (()::()::S)) τ1 τ2 α :
  ⊢ logrel_valid (consC (Tarr τ1 τ2) (consC τ1 Γ)) e α τ2 →
    logrel_valid Γ (Val $ RecV e) (interp_rec α) (Tarr τ1 τ2).
Proof.
  iIntros "#H". iIntros (ss σ) "#Hss".
  pose (s := (subs_of_subs2 ss)). fold s.
  pose (env := (its_of_subs2 ss)). fold env.
  simp subst_expr.
  iApply logrel_of_val. iLöb as "IH". iSimpl.
  pose (f := (ir_unf α env)).
  iAssert (interp_rec α env ≡ Fun (Next f))%I as "Hf".
  { iPureIntro. apply interp_rec_unfold. }
  iExists (Next f). iSplit; eauto.
  iIntros (w β σ') "#Hw".
  iDestruct (logrel_val_ITV with "Hw") as (βv) "Hbv".
  iRewrite "Hbv" in "Hw". iRewrite "Hbv".
  iAssert (internal_eq (APP' (interp_rec α env) (IT_of_V βv)) (Tick (ir_unf α env β)))
    as "Htick".
  { iRewrite "Hbv". iPureIntro. trans (APP' (Fun (Next f)) (IT_of_V βv)).
    - f_equiv. rewrite interp_rec_unfold. done.
    - rewrite APP_APP'_ITV.
      rewrite APP_Fun. simpl. done.
  } iRewrite "Htick".
  iApply logrel_expr_step_tick.
  { apply (Ectx_step' []). by constructor. }
  iNext.
  pose (ss' := cons_subs2 (RecV (subst_expr e (subs_lift (subs_lift s)))) (FunV (Next (ir_unf α env))) (cons_subs2 w βv  ss)).
  iSpecialize ("H" $! ss' σ' with "[Hss]").
  { rewrite {2}/subs2_valid /ss'. simp list_of_tyctx list_of_subs2.
    cbn-[logrel_val]. iFrame "Hss Hw". fold f. iRewrite -"Hf".
    by iApply "IH". }
  iClear "IH Hss Hw Htick".
  unfold ss'. simpl. simp its_of_subs2. fold f env.
  iRewrite "Hf". iRewrite -"Hbv" in "H". simpl.
  assert ((subst2 (subst_expr e (subs_lift (subs_lift s)))
                  (Val (RecV (subst_expr e (subs_lift (subs_lift s))))) (Val w))
            = ((subst_expr e
             (subs_of_subs2
                (cons_subs2 (RecV (subst_expr e (subs_lift (subs_lift s))))
                   (FunV (Next f)) (cons_subs2 w βv ss)))))) as ->; last first.
  { iApply "H". }
  unfold subst2, s. generalize ((RecV (subst_expr e (subs_lift (subs_lift (subs_of_subs2 ss)))))) as vf.
  intros vf.
  rewrite subst_expr_appsub. apply subst_expr_proper=> v.
  unfold appsub.
  dependent elimination v.
  - rewrite subs_of_subs2_equation_2.
    simp subs_lift subst_expr. done.
  - rewrite subs_of_subs2_equation_3.
    simp subs_lift subst_expr.
    dependent elimination v.
    + rewrite subs_of_subs2_equation_2.
      simp subs_lift subst_expr. done.
    + rewrite subs_of_subs2_equation_3.
      simp subs_lift subst_expr.
      rewrite !subst_expr_lift.
      apply subst_expr_idsub.
Qed.

Lemma compat_rec {S} Γ (e : expr (()::()::S)) τ1 τ2 α :
  ⊢ logrel_valid (consC (Tarr τ1 τ2) (consC τ1 Γ)) e α τ2 →
    logrel_valid Γ (Rec e) (interp_rec α) (Tarr τ1 τ2).
Proof.
  iIntros "#H". iIntros (ss σ) "#Hss".
  pose (s := (subs_of_subs2 ss)). fold s.
  pose (env := (its_of_subs2 ss)). fold env.
  simp subst_expr.
  iApply (logrel_expr_step_zero _ _ (Val $ RecV (subst_expr e (subs_lift (subs_lift s))))).
  { apply (Ectx_step' []). by constructor. }
  iPoseProof (compat_recv with "H") as "Hv".
  iSpecialize ("Hv" with "Hss"). simp subst_expr.
  iApply "Hv".
Qed.

Lemma compat_app {S} Γ (e1 e2 : expr S) τ1 τ2 α1 α2 :
  ⊢ logrel_valid Γ e1 α1 (Tarr τ1 τ2) →
    logrel_valid Γ e2 α2 τ1 →
    logrel_valid Γ (App e1 e2) (interp_app α1 α2) τ2.
Proof.
  iIntros "H1 H2".  iIntros (ss σ) "#Hss".
  iSpecialize ("H2" $! ss σ with "Hss").
  pose (s := (subs_of_subs2 ss)). fold s.
  pose (env := its_of_subs2 ss). fold env.
  simp subst_expr.
  iApply (logrel_bind [AppRCtx (subst_expr e1 s)] [AppRSCtx (α1 env)] with "H2").
  iIntros (σ' v2 β2) "H2". iSimpl.
  iSpecialize ("H1" $! ss σ' with "Hss").
  iApply (logrel_bind [AppLCtx v2] [AppLSCtx β2] with "H1").
  iIntros (σ'' v1 β1) "H1". simpl.
  iDestruct "H1" as (f) "[Hα H1]".
  iApply ("H1" with "H2").
Qed.

Lemma compat_input {S} Γ :
  ⊢ logrel_valid Γ (Input : expr S) interp_input Tnat.
Proof.
  iIntros (ss σ) "Hss". rewrite logrel_expr_unfold.
  iRight. iRight.
  destruct (update_input σ) as [n σ'] eqn:Hinp.
  iExists _,(Nat n),Input,(Val (Lit n)),σ'.
  rewrite reify_input_eq//.
  repeat iSplit; eauto; try iPureIntro.
  - constructor.
  - apply (Ectx_step' []).
    by constructor.
  - iNext. iApply logrel_of_val.
    cbn-[Tick Nat]. iExists n; eauto.
Qed.

Lemma compat_natop {S} (Γ : tyctx S) e1 e2 α1 α2 op :
  ⊢ logrel_valid Γ e1 α1 Tnat →
    logrel_valid Γ e2 α2 Tnat →
    logrel_valid Γ (NatOp op e1 e2) (interp_natop op α1 α2) Tnat.
Proof.
  iIntros "H1 H2".  iIntros (ss σ) "#Hss".
  iSpecialize ("H2" $! ss σ with "Hss").
  pose (s := (subs_of_subs2 ss)). fold s.
  pose (env := its_of_subs2 ss). fold env.
  simp subst_expr.
  iApply (logrel_bind [NatOpRCtx op (subst_expr e1 s)] [NatOpRSCtx op (α1 env)] with "H2").
  iIntros (σ' v2 β2) "H2". iSimpl.
  iSpecialize ("H1" $! ss σ' with "Hss").
  iApply (logrel_bind [NatOpLCtx op v2] [NatOpLSCtx op β2] with "H1").
  iIntros (σ'' v1 β1) "H1". simpl.
  iDestruct "H1" as (n1 ->) "Hn1".
  iDestruct "H2" as (n2 ->) "Hn2".
  iRewrite "Hn1". iRewrite "Hn2".
  iAssert ((NATOP (do_natop op) (Nat n1) (Nat n2)) ≡ Nat (do_natop op n1 n2))%I as "Hr".
  { iPureIntro. apply NATOP_Nat. }
  iApply (logrel_expr_step_zero _ _ (Val (Lit (do_natop op n1 n2)))).
  { apply (Ectx_step' []). constructor.
    destruct op; simpl; eauto. }
  iApply (logrel_of_val Tnat (Lit (do_natop op n1 n2))).
  simpl. iExists _. iSplit; eauto.
Qed.

Lemma fundamental {S} (Γ : tyctx S) τ e :
  typed Γ e τ → ⊢ logrel_valid Γ e (interp_expr e) τ
with fundamental_val {S} (Γ : tyctx S) τ v :
  typed_val Γ v τ → ⊢ logrel_valid Γ (Val v) (interp_val v) τ.
Proof.
  - induction 1; simpl.
    + by apply fundamental_val.
    + by apply compat_var.
    + iApply compat_rec. iApply IHtyped.
    + iApply compat_app.
      ++ iApply IHtyped1.
      ++ iApply IHtyped2.
    + iApply compat_natop.
      ++ iApply IHtyped1.
      ++ iApply IHtyped2.
    + iApply compat_if.
      ++ iApply IHtyped1.
      ++ iApply IHtyped2.
      ++ iApply IHtyped3.
    + iApply compat_input.
  - induction 1; simpl.
    + iIntros (ss σ) "Hss". simp subst_expr. simpl.
      iApply (logrel_of_val Tnat (Lit n)). iExists n. eauto.
    + iApply compat_recv. by iApply fundamental.
Qed.

Theorem adequacy (e : expr []) (k : nat) σ σ' n m :
  typed empC e Tnat →
  ssteps (interp_expr e ()) σ (Nat k) σ' (n,m) →
  prim_steps e σ (Val $ Lit k) σ' (n,m).
Proof.
  intros Hty%fundamental Hst.
  eapply logrel_adequate; eauto.
  iPoseProof (Hty $! emp_subs2 σ) as "H".
  simp its_of_subs2.
  assert (subst_expr e (subs_of_subs2 emp_subs2) = e) as ->.
  { rewrite subs_of_emp_subs2. apply subst_expr_idsub. }
  iApply "H".
  unfold subs2_valid. simp list_of_tyctx list_of_subs2. simpl. done.
Qed.
End logrel.
