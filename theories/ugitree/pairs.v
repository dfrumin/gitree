(** Encoding of pairs *)
From Equations Require Import Equations.
From iris.proofmode Require Import classes tactics.
From gitree Require Import prelude.
From gitree.ugitree Require Import core lambda reductions.

Opaque laterO_map.
(* why do i need to repeat this?*)
Opaque Nat Fun Tau Err Vis Tick.
Opaque APP APP' IF.
Opaque get_val.

Section pairs.
  Variable (E : opsInterp).
  Notation IT := (IT E).
  Notation ITV := (ITV E).
  Context {stateO} (re : reify_eff E stateO).
  Notation reify := (reify re).
  Notation sstep := (sstep re).
  Notation ssteps := (ssteps re).

  Program Definition pairTV : IT -n> IT -n> IT := λne a b,
      Fun (Next $ λne f, APP' (APP' f a) b).
  Solve Obligations with repeat (repeat intro; simpl; repeat f_equiv); solve_proper.
  Program Definition pairT : IT -n> IT -n> IT := λne a b,
      get_val (λne v2, get_val (λne v1, pairTV v1 v2) a) b.
  Solve Obligations with repeat (repeat intro; simpl; repeat f_equiv); solve_proper.

  Program Definition proj1Tf : IT := (Fun $ Next (λne a, Fun $ Next $ λne b, a)).
  Solve Obligations with repeat (repeat intro; simpl; repeat f_equiv); solve_proper.
  Program Definition proj1T : IT -n> IT := λne t, APP' t proj1Tf.
  Solve Obligations with repeat (repeat intro; simpl; repeat f_equiv); solve_proper.

  Program Definition projT2f : IT := (Fun $ Next (λne a, Fun $ Next $ λne b, b)).
  Program Definition proj2T : IT -n> IT := λne t, APP' t projT2f.
  Solve Obligations with repeat (repeat intro; simpl; repeat f_equiv); solve_proper.

  Lemma pairT_tick_l α β : pairT α (Tick β) ≡ Tick $ pairT α β.
  Proof. unfold pairT. rewrite get_val_tick//. Qed.
  Lemma pairT_tick_r α β `{!AsVal β} :
    pairT (Tick α) β ≡ Tick $ pairT α β.
  Proof. unfold pairT. rewrite !get_val_ITV /= get_val_tick//. Qed.

  Lemma proj1T_tick α : proj1T (Tick α) ≡ Tick $ proj1T α.
  Proof.
    simpl. rewrite APP_APP'_ITV.
    rewrite APP_Tick. f_equiv.
    rewrite APP_APP'_ITV//.
  Qed.
  Lemma proj2T_tick α : proj2T (Tick α) ≡ Tick $ proj2T α.
  Proof.
    simpl. rewrite APP_APP'_ITV.
    rewrite APP_Tick. f_equiv.
    rewrite APP_APP'_ITV//.
  Qed.

  Lemma proj1T_pair α β `{!AsVal α, !AsVal β} : proj1T (pairT α β) ≡ Tick_n 3 α.
  Proof.
    cbn-[pairTV].
    simpl.
    trans (APP' (pairTV α β) proj1Tf).
    { do 2 f_equiv. rewrite get_val_ITV/=.
      rewrite get_val_ITV/=. done. }
    rewrite APP_APP'_ITV.
    rewrite APP_Fun/=.
    rewrite APP_APP'_ITV.
    Transparent Tick. simpl. do 2 f_equiv.
    unfold proj1Tf.
    set (f :=  Fun (Next (λne _ : IT, α))).
    trans (APP (Tick f) (Next β)).
    { f_equiv. f_equiv.
      rewrite APP_APP'_ITV.
      rewrite APP_Fun. simpl. repeat f_equiv.
      unfold f. f_equiv.
      unfold equiv, laterO, ofe_equiv.
      simpl. intro. reflexivity. }
    rewrite APP_Tick. simpl.
    f_equiv. f_equiv. unfold f.
    rewrite APP_Fun/=. done.
  Qed.
End pairs.
