From iris.algebra.lib Require Import excl_auth.
From iris.base_logic.lib Require Export own fancy_updates invariants.
From iris.proofmode Require Import base tactics classes modality_instances.
From gitree Require Import prelude.
From gitree.ugitree Require Import core.

Opaque laterO_map.
(* why do i need to repeat this?*)
Opaque Nat Fun Tau Err Vis.

(*** Reification of effects *)
Section reify.
  Notation "F ♯ E" := (oFunctor_apply F E) (at level 20, right associativity).

  Definition reify_eff (E : opsInterp) (stateF : oFunctor) := ∀ (op : opid E) (X : ofe) `{Cofe X},
    ((Ins (E op) ♯ X) * (stateF ♯ X) -n> optionO ((Outs (E op) ♯ X) * (stateF ♯ X)))%type.

  Inductive reifiers : opsInterp → Type :=
  | reifiers_nil : reifiers @[]
  | reifiers_cons
      (E : opsInterp) (stateF : oFunctor) (re : reify_eff E stateF)
      {Hinh : Inhabited (stateF ♯ unitO)}
      {Hcofe : forall X (HX : Cofe X), Cofe (stateF ♯ X)}
      {E'} (tail : reifiers E') : reifiers (opsInterp.app E E').

  Fixpoint reifiers_state {E} (rs : reifiers E) : oFunctor :=
    match rs with
    | reifiers_nil => unitO
    | reifiers_cons _ stateF _ tail => stateF * (reifiers_state tail)
    end.         
  #[export] Instance reifiers_state_cofe E (rs : reifiers E) X `{!Cofe X} :
    Cofe (reifiers_state rs ♯ X).
  Proof. induction rs; simpl; apply _. Qed.
  #[export] Instance reifiers_state_inhab E (rs : reifiers E) :
    Inhabited (reifiers_state rs ♯ unitO).
  Proof. induction rs; simpl; apply _. Qed.


  Definition reifiers_re {E} (rs : reifiers E) (op : opid E) (X : ofe) `{Cofe X} :
    ((Ins (E op) ♯ X) * (reifiers_state rs ♯ X) → optionO ((Outs (E op) ♯ X) * (reifiers_state rs ♯ X)))%type.
  Proof.
    revert op. induction rs=>op.
    { apply Empty_set_rect. exact op. }
    simpl reifiers_state.
    destruct op as [op|op]; simpl.
    - refine (λ '(i,(s,s_rest)),
               '(o,s') ← re op X _ (i, s);
               Some (o,(s',s_rest))).
    - refine (λ '(i,(s,s_rest)),
               '(o,s'_rest) ← IHrs op (i,s_rest);
               Some (o,(s,s'_rest))
             ).
  Defined.

  #[global] Instance reifiers_re_ne {E} (rs : reifiers E) (op : opid E) (X : ofe) `{Cofe X} :
    NonExpansive (reifiers_re rs op X).
  Proof.
    induction rs; simpl.
    { apply Empty_set_rect. exact op. }
    destruct op as [op|op]; simpl.
    - intros n [x1 [s1 s1_rst]]  [x2 [s2 s2_rst]] [Hx [Hs Hsrest]].
      apply option_mbind_ne; last solve_proper.
      intros [o1 s'1] [o2 s'2] [Ho Hs']. solve_proper.
    - intros n [x1 [s1 s1_rst]]  [x2 [s2 s2_rst]] [Hx [Hs Hsrest]].
      apply option_mbind_ne; last solve_proper.
      intros [o1 s'1] [o2 s'2] [Ho Hs']. solve_proper.
  Qed.
  #[global] Instance reifiers_re_proper {E} (rs : reifiers E) (op : opid E) (X : ofe) `{Cofe X} :
    Proper ((≡) ==> (≡)) (reifiers_re rs op X).
  Proof. apply ne_proper. apply _. Qed.

  Variable (E : opsInterp) (rs : reifiers E).
  Notation IT := (IT E).
  Notation ITV := (ITV E).
  Notation stateF := (reifiers_state rs).

  Notation stateM := ((stateF ♯ IT -n> (stateF ♯ IT) * IT))%type.
  #[local] Instance stateT_inhab : Inhabited stateM.
  Proof.
    simple refine (populate (λne s, (s, Err))).
    solve_proper.
  Qed.
  #[local] Instance stateM_cofe : Cofe stateM.
  Proof. unfold stateM. apply _. Qed.

  Program Definition reify_fun : laterO (sumO IT stateM -n> prodO IT stateM) -n> stateM :=
    λne f s, (s, Fun $ laterO_map (λne f, fstO ◎ f ◎ inlO) f).
  Next Obligation. solve_proper. Qed.
  Next Obligation. solve_proper. Qed.

  Program Definition reify_tau : laterO (prodO IT stateM) -n> stateM :=
    λne x s, (s, Tau $ laterO_map fstO x).
  Next Obligation. solve_proper. Qed.
  Next Obligation. solve_proper. Qed.
  Next Obligation. solve_proper. Qed.

  Program Definition reify_vis ( op : opid E ) :
   oFunctor_car (Ins (E op)) (sumO IT stateM) (prodO IT stateM) -n>
     (oFunctor_car (Outs (E op)) (prodO IT stateM) (sumO IT stateM) -n> laterO (prodO IT stateM)) -n> stateM.
  Proof.
    simpl.
    (* simple refine (_ ◎ oFunctor_map _ (inlO,fstO)). *)
    simple refine (λne i (k : _ -n> _) (s : stateF ♯ IT), _).
    - simple refine (let ns := reifiers_re rs op _ (oFunctor_map _ (inlO,fstO) i, s) in _).
      simple refine (from_option (λ ns,
                         let out2' := k $ oFunctor_map (Outs (E op)) (fstO,inlO) ns.1 in
                         (ns.2, Tau $ laterO_map fstO out2'))
                       (s, Err) ns).
    - intros n s1 s2 Hs. simpl. eapply (from_option_ne (dist n)); solve_proper.
    - intros n k1 k2 Hk s. simpl. eapply (from_option_ne (dist n)); solve_proper.
    - intros n i1 i2 Hi k s. simpl. eapply (from_option_ne (dist n)); solve_proper.
  Defined.

  Program Definition reify_err : stateM := λne s, (s, Err).
  Next Obligation. solve_proper. Qed.
  Program Definition reify_nat : natO -n> stateM := λne n s, (s, Nat n).
  Next Obligation. solve_proper. Qed.
  Next Obligation. solve_proper. Qed.

  Program Definition unr : stateM -n>
    sumO (sumO (sumO (sumO natO unitO) (laterO (stateM -n> stateM))) (laterO stateM))
      (sigTO (λ op : opid E, prodO (oFunctor_apply (Ins (E op)) stateM) (oFunctor_apply (Outs (E op)) stateM -n> laterO stateM))).
  Proof. simple refine (λne d, inl (inl (inl (inr ())))). Qed.

  Definition reify : IT -n> stateM
    := IT_rec1 _
               reify_err
               reify_nat
               reify_fun
               reify_tau
               reify_vis
               unr.
  Definition unreify : stateM -n> IT
    := IT_rec2 _
               reify_err
               reify_nat
               reify_fun
               reify_tau
               reify_vis
               unr.

  Lemma reify_fun_eq f σ :
    reify (Fun f) σ ≡ (σ, Fun f).
  Proof.
    rewrite /reify/=.
    trans (reify_fun (laterO_map (sandwich (Perr:=reify_err) (Pnat:=reify_nat)
                                           (Parr:=reify_fun) (Ptau:=reify_tau)
                                           (Pvis:=reify_vis) (Punfold:=unr)
                                           stateM) f) σ).
    { f_equiv. apply IT_rec1_fun. }
    simpl. repeat f_equiv.
    rewrite -laterO_map_compose.
    trans (laterO_map idfun f).
    { repeat f_equiv. intros g x. done. }
    apply laterO_map_id.
  Qed.

  Lemma reify_vis_eq op i o k σ σ' :
    reifiers_re rs op _ (i,σ) ≡ Some (o,σ') →
    reify (Vis op i k) σ ≡ (σ', Tau $ k o).
  Proof.
    intros Hst.
    trans (reify_vis op
             (oFunctor_map _ (sumO_rec idfun unreify,prod_in idfun reify) i)
             (laterO_map (prod_in idfun reify) ◎ k ◎ (oFunctor_map _ (prod_in idfun reify,sumO_rec idfun unreify)))
             σ).
    { f_equiv.
      apply IT_rec1_vis. }
    Opaque prod_in. simpl.
    pose (r := (reifiers_re rs op _
      (oFunctor_map (Ins (E op)) (inlO, fstO)
                    (oFunctor_map (Ins (E op)) (sumO_rec idfun unreify, prod_in idfun reify) i), σ))).
    fold r.
    assert (r ≡ reifiers_re rs op _ (i,σ)) as Hr'.
    { unfold r. f_equiv. f_equiv.
      rewrite -oFunctor_map_compose.
      etrans; last by apply oFunctor_map_id.
      repeat f_equiv; intro; done. }
    assert (r ≡ Some (o,σ')) as Hr.
    { by rewrite Hr' Hst. }
    trans (from_option (λ ns,
       (ns.2,
        Tau
          (laterO_map fstO
             (laterO_map (prod_in idfun reify)
                (k
                   (oFunctor_map (Outs (E op)) (prod_in idfun reify, sumO_rec idfun unreify)
                      (oFunctor_map (Outs (E op)) (fstO, inlO) ns.1)))))))
             (σ, Err) (Some (o,σ'))).
    { apply from_option_proper; solve_proper. }
    simpl. repeat f_equiv.
    rewrite -laterO_map_compose.
    rewrite -oFunctor_map_compose.
    etrans; last by apply laterO_map_id.
    repeat f_equiv.
    { intro; done. }
    etrans; last by apply oFunctor_map_id.
    repeat f_equiv; intro; done.
  Qed.

  Lemma reify_vis_None op i k σ :
    reifiers_re rs op _ (i,σ) ≡ None →
    reify (Vis op i k) σ ≡ (σ, Err).
  Proof.
    intros Hs.
    trans (reify_vis op
             (oFunctor_map _ (sumO_rec idfun unreify,prod_in idfun reify) i)
             (laterO_map (prod_in idfun reify) ◎ k ◎ (oFunctor_map _ (prod_in idfun reify,sumO_rec idfun unreify)))
             σ).
    { f_equiv.
      apply IT_rec1_vis. }
    simpl.
    pose (r := (reifiers_re rs op _
      (oFunctor_map (Ins (E op)) (inlO, fstO)
                    (oFunctor_map (Ins (E op)) (sumO_rec idfun unreify, prod_in idfun reify) i), σ))).
    fold r.
    assert (r ≡ reifiers_re rs op _ (i,σ)) as Hr'.
    { unfold r. f_equiv. f_equiv.
      rewrite -oFunctor_map_compose.
      etrans; last by apply oFunctor_map_id.
      repeat f_equiv; intro; done. }
    assert (r ≡ None) as Hr.
    { by rewrite Hr' Hs. }
    trans (from_option (λ ns,
       (ns.2,
        Tau
          (laterO_map fstO
             (laterO_map (prod_in idfun reify)
                (k
                   (oFunctor_map (Outs (E op)) (prod_in idfun reify, sumO_rec idfun unreify)
                      (oFunctor_map (Outs (E op)) (fstO, inlO) ns.1)))))))
             (σ, Err) None).
    { apply from_option_proper; solve_proper. }
    reflexivity.
  Qed.

  Lemma reify_vis_cont op i k1 k2 σ1 σ2 β
    {PROP : bi} `{!BiInternalEq PROP} :
    (reify (Vis op i k1) σ1 ≡ (σ2, Tick β) ⊢
     reify (Vis op i (laterO_map k2 ◎ k1)) σ1 ≡ (σ2, Tick (k2 β)) : PROP)%I.
  Proof.
    destruct (reifiers_re rs op _ (i,σ1)) as [[o σ2']|] eqn:Hre; last first.
    - rewrite reify_vis_None; last by rewrite Hre//.
      iIntros "Hr". iExFalso.
      iPoseProof (prod_equivI with "Hr") as "[_ Hk]".
      simpl. iApply (IT_tick_err_ne). by iApply internal_eq_sym.
    - rewrite reify_vis_eq; last first.
      { by rewrite Hre. }
      rewrite reify_vis_eq; last first.
      { by rewrite Hre. }
      iIntros "Hr".
      iPoseProof (prod_equivI with "Hr") as "[Hs Hk]".
      iApply prod_equivI. simpl. iSplit; eauto.
      iPoseProof (Tau_inj' with "Hk") as "Hk".
      iApply Tau_inj'. iRewrite "Hk".
      rewrite laterO_map_Next. done.
  Qed.

  Lemma reify_input_cont_inv op i (k1 : _ -n> laterO IT) (k2 : IT -n> IT) σ1 σ2 β
    {PROP : bi} `{!BiInternalEq PROP} :
    (reify (Vis op i (laterO_map k2 ◎ k1)) σ1 ≡ (σ2, Tick β)
     ⊢ ∃ α, reify (Vis op i k1) σ1 ≡ (σ2, Tick α) ∧ ▷ (β ≡ k2 α)
      : PROP)%I.
  Proof.
    destruct (reifiers_re rs op _ (i,σ1)) as [[o σ2']|] eqn:Hre; last first.
    - rewrite reify_vis_None; last by rewrite Hre//.
      iIntros "Hr". iExFalso.
      iPoseProof (prod_equivI with "Hr") as "[_ Hk]".
      simpl. iApply (IT_tick_err_ne). by iApply internal_eq_sym.
    - rewrite reify_vis_eq; last first.
      { by rewrite Hre. }
      iIntros "Hr". simpl.
      iPoseProof (prod_equivI with "Hr") as "[#Hs #Hk]".
      simpl.
      iPoseProof (Tau_inj' with "Hk") as "Hk'".
      destruct (Next_uninj (k1 o)) as [a Hk1].
      iExists (a).
      rewrite reify_vis_eq; last first.
      { by rewrite Hre. }
      iSplit.
      + iApply prod_equivI. simpl. iSplit; eauto.
        iApply Tau_inj'. done.
      + iAssert (laterO_map k2 (Next a) ≡ Next β)%I as "Ha".
        { iRewrite -"Hk'".
          iPureIntro. rewrite -Hk1. done. }
        iAssert (Next (k2 a) ≡ Next β)%I as "Hb".
        { iRewrite -"Ha". iPureIntro.
          rewrite laterO_map_Next. done. }
        iNext. by iApply internal_eq_sym.
  Qed.

  Lemma reify_is_always_a_tick i op k σ β σ' :
    reify (Vis i op k) σ ≡ (σ', β) → (∃ β', β ≡ Tick β') ∨ (β ≡ Err).
  Proof.
    destruct (reifiers_re rs i _ (op, σ)) as [[o σ'']|] eqn:Hre; last first.
    - rewrite reify_vis_None; last by rewrite Hre//.
      intros [_ ?]. by right.
    - rewrite reify_vis_eq;last by rewrite Hre.
      intros [? Ho].
      destruct (Next_uninj (k o)) as [lβ Hlb].
      left. exists (lβ).
      unfold Tick. simpl.
      rewrite -Hlb. symmetry. apply Ho.
  Qed.

End reify.
Arguments reify {E} rs.

(* Section reify_comb. *)
(*   Context {E1 E2 : opsInterp}. *)
(*   Context {state1 state2 : oFunctor}. *)
(*   Variable (r1 : reify_eff E1 state1) (r2 : reify_eff E2 state2). *)
(*   Notation "F ♯ E" := (oFunctor_apply F E) (at level 20, right associativity). *)

(*   Definition stateF : oFunctor := (state1 * state2)%OF. *)
(*   Definition n1 := opsInterp_len E1. *)
(*   Definition n2 := opsInterp_len E2. *)
    
(*   Definition reify_comb : reify_eff (@[E1; E2]) stateF. *)
(*   Proof. *)
(*     unfold reify_eff. unfold opid. *)
(*     intros op X HX. simpl in op. *)
(*     pose (n1 :=). fold n1 in op. *)
(*     pose (n2 := ). fold n2 in op. *)
(*     pose (P := λ (op : fin (n1 + (n2 + 0))), *)
(*             prodO (Ins (@[ E1; E2] op) ♯ X) (stateF ♯ X) -n> *)
(*                 optionO (prodO (Outs (@[ E1; E2] op) ♯ X) (stateF ♯ X))). *)
(*     apply (fin_add_inv P); unfold P; clear op P. *)
(*     - rewrite Nat.add_0_r. *)
(*       intro op. *)
(*       simple refine (λne '(i, (s1,s2)), _). *)
(*       jx *)
(*         Fin.L *)
(*           unfold opsInterp_len. simpl. *)
(*       intro op. unfold opid in op. *)
(*       unfold op *)



(*** Semantic reductions *)
Section sstep.
  Variable (E : opsInterp) (rs : reifiers E).
  Notation IT := (IT E).
  Notation ITV := (ITV E).
  Notation stateF := (reifiers_state rs).
  Notation "F ♯ E" := (oFunctor_apply F E) (at level 20, right associativity).
  Notation stateO := (stateF ♯ IT).

  
  (** ** Reductions at the Prop level *)
  Inductive sstep : IT → stateO → IT → stateO → Prop :=
  | sstep_tick α β σ σ' :
    α ≡ Tick β →
    σ ≡ σ' →
    sstep α σ β σ'
  | sstep_reify α op i k β σ1 σ2 :
    α ≡ Vis op i k →
    reify rs (Vis op i k) σ1 ≡ (σ2, Tick β) →
    sstep α σ1 β σ2.
  #[export] Instance sstep_proper : Proper ((≡) ==> (≡) ==> (≡) ==> (≡) ==> (iff)) sstep.
  Proof.
    intros α1 α2 Ha σ1 σ2 Hs β1 β2 Hb σ'1 σ'2 Hs'.
    simplify_eq/=. split.
    - induction 1.
      + eapply sstep_tick.
        ++ rewrite -Ha -Hb//.
        ++ rewrite -Hs' -Hs//.
      + eapply sstep_reify.
        ++ rewrite -Ha//.
        ++ rewrite -Hb -Hs' -Hs//.
    - induction 1.
      + eapply sstep_tick.
        ++ rewrite Ha Hb//.
        ++ rewrite Hs' Hs//.
      + eapply sstep_reify.
        ++ rewrite Ha//.
        ++ rewrite Hb Hs' Hs//.
  Qed.

  Inductive ssteps  : IT → stateO → IT → stateO → nat → Prop :=
  | ssteps_zero α β σ σ' : α ≡ β → σ ≡ σ' → ssteps α σ β σ' 0
  | ssteps_many α1 σ1 α2 σ2 α3 σ3 n2 :
    sstep α1 σ1 α2 σ2 →
    ssteps α2 σ2 α3 σ3 n2 →
    ssteps α1 σ1 α3 σ3 (1+n2).
  #[export] Instance ssteps_proper : Proper ((≡) ==> (≡) ==> (≡) ==> (≡) ==> (=) ==> (iff)) ssteps.
  Proof.
    intros α α' Ha σ σ' Hs β β' Hb σ2 σ2' Hs2 n1 n2 Hnm.
    fold_leibniz. simplify_eq/=.
    split.
    - intro HS. revert α' β' σ' σ2' Ha Hb Hs Hs2.
      induction HS=>α' β' σ'' σ2' Ha Hb Hs Hs2.
      + constructor.
        ++ rewrite -Ha -Hb//.
        ++ rewrite -Hs -Hs2//.
      + econstructor; last first.
        ++ eapply IHHS; eauto.
        ++ rewrite -Ha -Hs//.
    - intro HS. revert α β σ σ2 Ha Hb Hs Hs2.
      induction HS=>α' β' σ'' σ2' Ha Hb Hs Hs2.
      + constructor.
        ++ rewrite Ha Hb//.
        ++ rewrite Hs Hs2//.
      + econstructor; last first.
        ++ eapply IHHS; eauto.
        ++ rewrite Ha Hs//.
  Qed.

  Lemma ssteps_tick_n n α σ : ssteps (Tick_n n α) σ α σ n.
  Proof.
    induction n; first by constructor.
    change (S n) with (1+n).
    change (Tick_n (1+n) α) with (Tick $ Tick_n n α).
    econstructor; last eassumption.
    econstructor; reflexivity.
  Qed.
End sstep.

Arguments sstep {E} rs _ _ _ _.
Arguments ssteps {E} rs _ _ _ _ _.

Section istep.
  Variable (E : opsInterp) (rs : reifiers E).
  Notation IT := (IT E).
  Notation ITV := (ITV E).
  Notation stateF := (reifiers_state rs).
  Notation "F ♯ E" := (oFunctor_apply F E) (at level 20, right associativity).
  Notation stateO := (stateF ♯ IT).

  Context `{!invGS_gen hlc Σ}.
  Notation iProp := (iProp Σ).

  Program Definition istep :
    IT -n> stateO -n> IT -n> stateO -n> iProp :=
    λne α σ β σ', ((α ≡ Tick β ∧ σ ≡ σ')
                    ∨ (∃ op i k, α ≡ Vis op i k ∧ reify rs α σ ≡ (σ', Tick β)))%I.
  Next Obligation. solve_proper. Qed.
  Next Obligation. solve_proper. Qed.
  Next Obligation. solve_proper. Qed.
  Next Obligation. solve_proper. Qed.

  Program Definition isteps_pre
          (self : IT -n> stateO -n> IT -n> stateO -n> natO -n> iProp):
    IT -n> stateO -n> IT -n> stateO -n> natO -n> iProp :=
    λne α σ β σ' n, ((n ≡ 0 ∧ α ≡ β ∧ σ ≡ σ')
                 ∨ (∃ n' α0 σ0, n ≡ (1+n') ∧ istep α σ α0 σ0 ∧
                     ▷ self α0 σ0 β σ' n'))%I.
  Next Obligation. solve_proper. Qed.
  Next Obligation. solve_proper. Qed.
  Next Obligation. solve_proper. Qed.
  Next Obligation. solve_proper. Qed.
  Next Obligation. solve_proper. Qed.

  Global Instance isteps_pre_ne : NonExpansive isteps_pre.
  Proof. solve_proper. Qed.
  Global Instance isteps_pre_contractive : Contractive isteps_pre.
  Proof. solve_contractive. Qed.
  Definition isteps : IT -n> stateO -n> IT -n> stateO -n> natO -n> iProp
    := fixpoint isteps_pre.

  Lemma isteps_unfold α β σ σ' n :
    isteps α σ β σ' n ≡
      ((n ≡ 0 ∧ α ≡ β ∧ σ ≡ σ')
                 ∨ (∃ n' α0 σ0, n ≡ (1+n') ∧ istep α σ α0 σ0 ∧
                     ▷ isteps α0 σ0 β σ' n'))%I.
  Proof. unfold isteps. apply (fixpoint_unfold isteps_pre _ _ _ _ _) . Qed.


  (** Properties *)
  Lemma isteps_0 α β σ σ' :
    isteps α σ β σ' 0 ≡ (α ≡ β ∧ σ ≡ σ')%I.
  Proof.
    rewrite isteps_unfold. iSplit.
    - iDestruct 1 as "[(_ & $ & $)|H]".
      iExFalso.
      iDestruct "H" as (n' ? ?) "[% HH]".
      fold_leibniz. lia.
    - iDestruct 1 as "[H1 H2]". iLeft; eauto.
  Qed.
  Lemma isteps_S α β σ σ' k :
    isteps α σ β σ' (S k) ≡ (∃ α1 σ1, istep α σ α1 σ1 ∧ ▷ isteps α1 σ1 β σ' k)%I.
  Proof.
    rewrite isteps_unfold. iSplit.
    - iDestruct 1 as "[(% & _ & _)|H]"; first by fold_leibniz; lia.
      iDestruct "H" as (n' ? ?) "[% HH]".
      fold_leibniz. assert (k = n') as -> by lia.
      iExists _,_. eauto with iFrame.
    - iDestruct 1 as (α1 σ1) "[H1 H2]".
      iRight. iExists k,α1,σ1. eauto with iFrame.
  Qed.
  #[export] Instance isteps_persistent α σ β σ' k : Persistent (isteps α σ β σ' k).
  Proof.
    revert α σ. induction k as [|k]=> α σ.
    - rewrite isteps_0. apply _.
    - rewrite isteps_S. apply _.
  Qed.

  Lemma sstep_istep α β σ σ' :
    sstep rs α σ β σ' → (⊢ istep α σ β σ').
  Proof.
    inversion 1; simplify_eq/=.
    - iLeft. eauto.
    - iRight. iExists _,_,_. iSplit; eauto.
      iPureIntro.
      trans (reify rs (Vis op i k) σ); first solve_proper; eauto.
  Qed.
  Lemma ssteps_isteps α β σ σ' n :
    ssteps rs α σ β σ' n → (⊢ isteps α σ β σ' n).
  Proof.
    revert α σ. induction n=> α σ; inversion 1; simplify_eq /=.
    - rewrite isteps_unfold. iLeft. eauto.
    - rewrite isteps_unfold. iRight. iExists n, α2,σ2.
      repeat iSplit; eauto.
      + iApply sstep_istep; eauto.
      + iNext. iApply IHn; eauto.
  Qed.

  Local Lemma tick_safe_externalize α σ :
    (⊢ ∃ β σ', α ≡ Tick β ∧ σ ≡ σ' : iProp) → ∃ β σ', sstep rs α σ β σ'.
  Proof.
    intros Hprf.
    destruct (IT_dont_confuse α)
        as [Ha | [[n Ha] | [ [g Ha] | [[α' Ha]|[op [i [k Ha]]]] ]]].
    + exfalso. eapply uPred.pure_soundness.
      iPoseProof (Hprf) as "H".
      iDestruct "H" as (β σ') "[Ha Hs]". rewrite Ha.
      iApply (IT_tick_err_ne). by iApply internal_eq_sym.
    + exfalso. eapply uPred.pure_soundness.
      iPoseProof (Hprf) as "H".
      iDestruct "H" as (β σ') "[Ha Hs]". rewrite Ha.
      iApply (IT_nat_tick_ne with "Ha").
    + exfalso. eapply uPred.pure_soundness.
      iPoseProof (Hprf) as "H".
      iDestruct "H" as (β σ') "[Ha Hs]". rewrite Ha.
      iApply (IT_fun_tick_ne with "Ha").
    + exists α',σ. by econstructor; eauto.
    + exfalso. eapply uPred.pure_soundness.
      iPoseProof (Hprf) as "H".
      iDestruct "H" as (β σ') "[Ha Hs]". rewrite Ha.
      iApply (IT_tick_vis_ne). by iApply internal_eq_sym.
  Qed.

  Local Lemma effect_safe_externalize α σ :
    (⊢ ∃ β σ', (∃ op i k, α ≡ Vis op i k ∧ reify rs α σ ≡ (σ', Tick β)) : iProp) →
    ∃ β σ', sstep rs α σ β σ'.
  Proof.
    intros Hprf.
    destruct (IT_dont_confuse α)
        as [Ha | [[n Ha] | [ [g Ha] | [[α' Ha]|[op [i [k Ha]]]] ]]].
    + exfalso. eapply uPred.pure_soundness.
      iPoseProof (Hprf) as "H".
      iDestruct "H" as (β σ' op i k) "[Ha _]". rewrite Ha.
      iApply (IT_vis_err_ne). by iApply internal_eq_sym.
    + exfalso. eapply uPred.pure_soundness.
      iPoseProof (Hprf) as "H".
      iDestruct "H" as (β σ' op i k) "[Ha _]". rewrite Ha.
      iApply (IT_nat_vis_ne with "Ha").
    + exfalso. eapply uPred.pure_soundness.
      iPoseProof (Hprf) as "H".
      iDestruct "H" as (β σ' op i k) "[Ha _]". rewrite Ha.
      iApply (IT_fun_vis_ne with "Ha").
    + exfalso. eapply uPred.pure_soundness.
      iPoseProof (Hprf) as "H".
      iDestruct "H" as (β σ' op i k) "[Ha _]". rewrite Ha.
      iApply (IT_tick_vis_ne with "Ha").
    + destruct (reify rs (Vis op i k) σ) as [σ1 α1] eqn:Hr.
      assert ((∃ α' : IT, α1 ≡ Tick α') ∨ α1 ≡ Err) as [[α' Ha']| Ha'].
      { eapply (reify_is_always_a_tick _ _ op i k σ).
        by rewrite Hr. }
      * exists α',σ1. eapply sstep_reify; eauto.
        rewrite Hr -Ha'//.
      * exfalso. eapply uPred.pure_soundness.
        iPoseProof (Hprf) as "H".
        iDestruct "H" as (β σ' op' i' k') "[_ Hb]".
        assert (reify rs (Vis op i k) σ ≡ reify rs α σ) as Har.
        { f_equiv. by rewrite Ha. }
        iEval (rewrite -Har) in "Hb".
        iEval (rewrite Hr) in "Hb".
        iPoseProof (prod_equivI with "Hb") as "[_ Hb']".
        simpl. rewrite Ha'.
        iApply (IT_tick_err_ne). by iApply internal_eq_sym.
  Qed.

  Local Lemma istep_safe_disj α σ :
    (∃ β σ', istep α σ β σ')
      ⊢ (∃ β σ', α ≡ Tick β ∧ σ ≡ σ')
      ∨ (∃ β σ', (∃ op i k, α ≡ Vis op i k ∧ reify rs α σ ≡ (σ', Tick β))).
  Proof.
    rewrite -bi.or_exist.
    apply bi.exist_mono=>β.
    rewrite -bi.or_exist//.
  Qed.

  (* this is true only for iProp/uPred? *)
  Local Lemma iprop_disjunction (P Q : iProp) :
    (⊢ P ∨ Q) → (⊢ P) ∨ (⊢ Q).
  Proof. Admitted.

  Lemma istep_safe_sstep α σ :
    (⊢ ∃ β σ', istep α σ β σ') → ∃ β σ', sstep rs α σ β σ'.
  Proof.
    rewrite istep_safe_disj.
    intros [H|H]%iprop_disjunction.
    - by apply tick_safe_externalize.
    - by apply effect_safe_externalize.
  Qed.

  Lemma istep_ITV α αv β σ σ' :
    (IT_to_V α ≡ Some αv ⊢ istep α σ β σ' -∗ False : iProp)%I.
  Proof.
    rewrite /istep/=. iIntros "Hv [[H _]|H]".
    - iRewrite "H" in "Hv". rewrite IT_to_V_Tau.
      iApply (option_equivI with "Hv").
    - iDestruct "H" as (op i k) "[H _]".
      iRewrite "H" in "Hv". rewrite IT_to_V_Vis.
      iApply (option_equivI with "Hv").
  Qed.

  Lemma istep_err σ β σ' : istep Err σ β σ' ⊢ False.
  Proof.
    rewrite /istep/=. iDestruct 1 as "[[H _]|H]".
    - iApply (IT_tick_err_ne).
      by iApply internal_eq_sym.
    - iDestruct "H" as (op i k) "[H _]". iApply (IT_vis_err_ne).
      by iApply internal_eq_sym.
  Qed.
  Lemma istep_tick α β σ σ' : istep (Tick α) σ β σ' ⊢ ▷ (β ≡ α) ∧ σ ≡ σ'.
  Proof.
    simpl. iDestruct 1 as "[[H1 H2]|H]".
    - iFrame. iNext. by iApply internal_eq_sym.
    - iDestruct "H" as (op i k) "[H1 H2]".
      iExFalso. iApply (IT_tick_vis_ne with "H1").
  Qed.
  Lemma istep_vis op i ko β σ σ' :
    istep (Vis op i ko) σ β σ' ⊢ reify rs (Vis op i ko) σ ≡ (σ', Tick β).
  Proof.
    simpl. iDestruct 1 as "[[H1 H2]|H]".
    - iExFalso. iApply IT_tick_vis_ne.
      by iApply internal_eq_sym.
    - iDestruct "H" as (op' i' ko') "[H1 $]".
  Qed.

  Lemma isteps_val βv β k σ σ' :
    isteps (IT_of_V βv) σ β σ' k ⊢ IT_of_V βv ≡ β ∧ σ ≡ σ' ∧ ⌜k = 0⌝.
  Proof.
    destruct k as[|k].
    - rewrite isteps_0. iDestruct 1 as "[$ $]".
      eauto.
    - rewrite isteps_S. iDestruct 1 as (α1 σ1) "[Hs Hss]".
      iExFalso. iClear "Hss".
      unfold istep. simpl. iDestruct "Hs" as "[[Ht Hs]|Hs]".
      + destruct βv as[n|f]; iSimpl in "Ht".
        ++  iApply (IT_nat_tick_ne with "Ht").
        ++  iApply (IT_fun_tick_ne with "Ht").
      + iDestruct "Hs" as (op i ko) "[Ht Hs]".
        destruct βv as[n|f]; iSimpl in "Ht".
        ++  iApply (IT_nat_vis_ne with "Ht").
        ++  iApply (IT_fun_vis_ne with "Ht").
  Qed.
  Lemma isteps_tick α βv σ σ' k :
    isteps (Tick α) σ (IT_of_V βv) σ' k ⊢ ∃ k' : nat, ⌜k = (1 + k')%nat⌝ ∧ ▷ isteps α σ (IT_of_V βv) σ' k'.
  Proof.
    rewrite isteps_unfold.
    iDestruct 1 as "[[Hk [Ht Hs]] | H]".
    - iExFalso. destruct βv; iSimpl in "Ht".
      ++ iApply (IT_nat_tick_ne).
         by iApply (internal_eq_sym).
      ++ iApply (IT_fun_tick_ne).
         by iApply (internal_eq_sym).
    - iDestruct "H" as (k' α1 σ1) "[% [Hs Hss]]".
      iExists k'. iSplit; first by eauto.
      rewrite istep_tick.
      iDestruct "Hs" as "[Ha Hs]". iNext.
      iRewrite -"Ha". iRewrite "Hs". done.
  Qed.

  Lemma istep_hom (f : IT → IT) `{!IT_hom f} α σ β σ' :
    istep α σ β σ' ⊢ istep (f α) σ (f β) σ' : iProp.
  Proof.
    iDestruct 1 as "[[Ha Hs]|H]".
    - iRewrite "Ha". iLeft. iSplit; eauto. iPureIntro. apply hom_tick.
    - iDestruct "H" as (op i k) "[#Ha Hr]".
      pose (f' := OfeMor f).
      iRight. iExists op,i,(laterO_map f' ◎ k).
      iAssert (f (Vis op i k) ≡ Vis op i (laterO_map f' ◎ k))%I as "Hf".
      { iPureIntro. apply hom_vis. }
      iRewrite "Ha". iRewrite "Ha" in "Hr". iRewrite "Hf".
      iSplit; first done.
      iApply (reify_vis_cont with "Hr").
  Qed.

  Lemma istep_hom_inv α σ β σ' `{!IT_hom f} :
    istep (f α) σ β σ' ⊢@{iProp} ⌜is_Some (IT_to_V α)⌝
    ∨ (IT_to_V α ≡ None ∧ ∃ α', istep α σ α' σ' ∧ ▷ (β ≡ f α')).
  Proof.
    iIntros "H".
    destruct (IT_dont_confuse α)
      as [Ha | [[n Ha] | [ [g Ha] | [[la Ha]|[op [i [k Ha]]]] ]]].
    - iExFalso. iApply (istep_err σ β σ').
      iAssert (f α ≡ Err)%I as "Hf".
      { iPureIntro. by rewrite Ha hom_err. }
      iRewrite "Hf" in "H". done.
    - iLeft. iPureIntro. rewrite Ha IT_to_V_Nat. done.
    - iLeft. iPureIntro. rewrite Ha IT_to_V_Fun. done.
    - iAssert (α ≡ Tick la)%I as "Ha"; first by eauto.
      iAssert (f (Tick la) ≡ Tick (f la))%I as "Hf".
      { iPureIntro. rewrite hom_tick. done. }
      iRight. iRewrite "Ha". iRewrite "Ha" in "H".
      iRewrite "Hf" in "H". rewrite istep_tick.
      iDestruct "H" as "[Hb Hs]". iSplit.
      { by rewrite IT_to_V_Tau. }
      iExists la. iSplit; last eauto.
      unfold istep. iLeft. iSplit; eauto.
    - iRight.
      pose (fi:=OfeMor f).
      iAssert (f α ≡ Vis op i (laterO_map fi ◎ k))%I as "Hf".
      { iPureIntro. by rewrite Ha hom_vis. }
      iRewrite "Hf" in "H".
      rewrite {1}/istep. iSimpl in "H".
      iDestruct "H" as "[[H _]|H]".
      + iExFalso. iApply (IT_tick_vis_ne).
        iApply internal_eq_sym. done.
      + iDestruct "H" as (op' i' k') "[#Ha Hr]".
        iPoseProof (Vis_inj_op' with "Ha") as "<-".
        iPoseProof (Vis_inj' with "Ha") as "[Hi Hk]".
        iPoseProof (reify_input_cont_inv _ _ _ _ k fi with "Hr") as (α') "[Hr Ha']".
        iAssert (reify rs α σ ≡ (σ', Tick α'))%I with "[Hr]" as "Hr".
        { iRewrite -"Hr". iPureIntro. repeat f_equiv.
          apply Ha. }
        iSplit. { iPureIntro. by rewrite Ha IT_to_V_Vis. }
        iExists α'. iFrame "Ha'".
        rewrite /istep. iRight.
        iExists op,i,k. iFrame "Hr".
        iPureIntro. apply Ha.
  Qed.


End istep.

Arguments istep {_} rs {_}.
Arguments isteps {_} rs {_}.
