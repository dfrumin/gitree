(* If convergence for the simple_io language *)
From iris.algebra.lib Require Import excl_auth.
From iris.base_logic.lib Require Export own fancy_updates invariants.
From iris.program_logic Require Export language.
From iris.proofmode Require Import base tactics classes modality_instances.
From gitree Require Import prelude.
From gitree.ugitree Require Import core lambda simple_io.

Opaque Nat Fun Tau Err Vis.
Opaque APP APP' IF NATOP.
Opaque INPUT.
Opaque Tick.

Section class_instances.
  (* XXX knkr *)
#[export] Instance elim_modal_step_fupdN_fupd {PROP : bi} `{!BiFUpd PROP} k p (P Q : PROP) :
  ElimModal True p false (|={⊤}=> P) P (|={⊤}[∅]▷=>^k |={⊤}=> Q) (|={⊤}[∅]▷=>^k |={⊤}=> Q).
Proof.
  destruct k; simpl ; apply _.
Qed.

End class_instances.

Class statePreG Σ := statePreG_in :: inG Σ (excl_authUR stateO).
Class stateG (Σ : gFunctors) := {
    stateG_inG :: statePreG Σ;
    stateG_name : gname;
  }.
Definition stateΣ : gFunctors := GFunctor (excl_authUR stateO).
Definition of_state (σ : state) : stateO := σ.
Arguments of_state : simpl never.
Definition state_interp (σ : state) `{!stateG Σ} : iProp Σ :=
  (own stateG_name (●E (of_state σ)))%I.
Definition has_state (σ : state) `{!stateG Σ} : iProp Σ :=
  (own stateG_name (◯E (of_state σ)))%I.

Lemma new_state_interp σ `{!invGS_gen hlc Σ, !statePreG Σ} :
  (⊢ |==> ∃ `{!stateG Σ}, state_interp σ ∗ has_state σ : iProp Σ)%I.
Proof.
  iMod (own_alloc ((●E (of_state σ)) ⋅ (◯E (of_state σ)))) as (γ) "[H1 H2]".
  { apply excl_auth_valid. }
  pose (sg := {| stateG_inG := _; stateG_name := γ |}).
  iModIntro. iExists sg. by iFrame.
Qed.
#[export] Instance subG_stateΣ {Σ} : subG stateΣ Σ → statePreG Σ.
Proof. solve_inG. Qed.

Section ic.
  Context `{!invGS_gen hlc Σ, !stateG Σ}.
  Notation IT := (IT ioΣ).
  Notation ITV := (ITV ioΣ).
  Notation iProp := (iProp Σ).

(*****************************************************)

  Program Definition istep :
    IT -n> stateO -n> IT -n> stateO -n> iProp :=
    λne α σ β σ', ((α ≡ Tick β ∧ σ ≡ σ')
                    ∨ (∃ k, α ≡ INPUT k ∧ reify α σ ≡ (σ', Tick β)))%I.
  Next Obligation. solve_proper. Qed.
  Next Obligation. solve_proper. Qed.
  Next Obligation. solve_proper. Qed.
  Next Obligation. solve_proper. Qed.

  Program Definition isteps_pre
          (self : IT -n> stateO -n> IT -n> stateO -n> natO -n> iProp):
    IT -n> stateO -n> IT -n> stateO -n> natO -n> iProp :=
    λne α σ β σ' n, ((n ≡ 0 ∧ α ≡ β ∧ σ ≡ σ')
                 ∨ (∃ n' α0 σ0, n ≡ (1+n') ∧ istep α σ α0 σ0 ∧
                     ▷ self α0 σ0 β σ' n'))%I.
  Next Obligation. solve_proper. Qed.
  Next Obligation. solve_proper. Qed.
  Next Obligation. solve_proper. Qed.
  Next Obligation. solve_proper. Qed.
  Next Obligation. solve_proper. Qed.

  Global Instance isteps_pre_ne : NonExpansive isteps_pre.
  Proof. solve_proper. Qed.
  Global Instance isteps_pre_contractive : Contractive isteps_pre.
  Proof. solve_contractive. Qed.
  Definition isteps : IT -n> stateO -n> IT -n> stateO -n> natO -n> iProp
    := fixpoint isteps_pre.

  Lemma isteps_unfold α β σ σ' n :
    isteps α σ β σ' n ≡
      ((n ≡ 0 ∧ α ≡ β ∧ σ ≡ σ')
                 ∨ (∃ n' α0 σ0, n ≡ (1+n') ∧ istep α σ α0 σ0 ∧
                     ▷ isteps α0 σ0 β σ' n'))%I.
  Proof. unfold isteps. apply (fixpoint_unfold isteps_pre _ _ _ _ _) . Qed.

  Definition ic (α : IT) (Φ : nat → ITV → iProp) : iProp :=
    (∀ σ σ' βv k, state_interp σ -∗ isteps α σ (IT_of_V βv) σ' k -∗
                  |={⊤}[∅]▷=>^k |={⊤}=> state_interp σ' ∗ Φ k βv)%I.

  Notation "'IC' α {{ k ; β , Φ } }" := (ic α (λ k β, Φ))
    (at level 20, α, Φ at level 200,
     format "'IC'  α  {{ k ;  β ,  Φ  } }") : bi_scope.

  Notation "'IC' α {{ Φ } }" := (ic α Φ)
    (at level 20, α, Φ at level 200,
     format "'IC'  α  {{  Φ  } }") : bi_scope.

  Lemma step_fupdN_ne n Ei Eo k (P Q : iProp) :
    P ≡{n}≡ Q →
    (|={Eo}[Ei]▷=>^k P)%I ≡{n}≡ (|={Eo}[Ei]▷=>^k Q)%I.
  Proof.
    intros HPQ. induction k as [|k]; simpl ; eauto.
    f_equiv. f_equiv. f_equiv. eauto.
  Qed.

  #[export] Instance ic_ne n :
    Proper (dist n ==> (pointwise_relation _ (pointwise_relation _ (dist n))) ==> dist n) ic.
  Proof.
    intros α β Hab Φ Ψ Hp. unfold ic.
    f_equiv. f_equiv. f_equiv. f_equiv. f_equiv.
    f_equiv. f_equiv. intros k. f_equiv. f_equiv.
    { solve_proper. }
    apply step_fupdN_ne.
    by repeat f_equiv.
  Qed.

  (** Properties *)
  Lemma isteps_0 α β σ σ' :
    isteps α σ β σ' 0 ≡ (α ≡ β ∧ σ ≡ σ')%I.
  Proof.
    rewrite isteps_unfold. iSplit.
    - iDestruct 1 as "[(_ & $ & $)|H]".
      iExFalso.
      iDestruct "H" as (n' ? ?) "[% HH]".
      fold_leibniz. lia.
    - iDestruct 1 as "[H1 H2]". iLeft; eauto.
  Qed.
  Lemma isteps_S α β σ σ' k :
    isteps α σ β σ' (S k) ≡ (∃ α1 σ1, istep α σ α1 σ1 ∧ ▷ isteps α1 σ1 β σ' k)%I.
  Proof.
    rewrite isteps_unfold. iSplit.
    - iDestruct 1 as "[(% & _ & _)|H]"; first by fold_leibniz; lia.
      iDestruct "H" as (n' ? ?) "[% HH]".
      fold_leibniz. assert (k = n') as -> by lia.
      iExists _,_. eauto with iFrame.
    - iDestruct 1 as (α1 σ1) "[H1 H2]".
      iRight. iExists k,α1,σ1. eauto with iFrame.
  Qed.
  #[export] Instance isteps_persistent α σ β σ' k : Persistent (isteps α σ β σ' k).
  Proof.
    revert α σ. induction k as [|k]=> α σ.
    - rewrite isteps_0. apply _.
    - rewrite isteps_S. apply _.
  Qed.

  Lemma sstep_equivI1 α1 α2 σ σ' β1 m :
    sstep α1 σ β1 σ' m →
    (α1 ≡ α2 ⊢ ∃ β2, ▷ (β1 ≡ β2) ∧ ⌜sstep α2 σ β2 σ' m⌝ : iProp)%I.
  Proof.
    inversion 1 as [α β σ1 H1 | α β k σ1 σ2 H1 H2 ]; subst.
    - rewrite H1.
      iIntros "H".
      destruct (IT_dont_confuse α2)
        as [Ha2 | [[m Ha2] | [ [g Ha2] | [[la Ha2]|[op [i [k Ha2]]]] ]]].
      all: iEval (rewrite Ha2) in "H".
      + iExFalso.
        iApply (IT_tick_err_ne); eauto.
      + iExFalso.
        iApply IT_nat_tick_ne. by iApply internal_eq_sym.
      + iExFalso.
        iApply IT_fun_tick_ne.  by iApply internal_eq_sym.
      + iExists la. iSplit.
        ++ iNext. done.
        ++ iPureIntro. rewrite Ha2.
           by econstructor.
      + iExFalso. by iApply IT_tick_vis_ne.
    - rewrite H1.
      iIntros "#H".
      destruct (IT_dont_confuse α2)
        as [Ha2 | [[m Ha2] | [ [g Ha2] | [[la Ha2]|[op [i [ko Ha2]]]] ]]].
      all: iEval (rewrite Ha2) in "H".
      + iExFalso.
        iApply (IT_vis_err_ne); eauto.
      + iExFalso.
        iApply IT_nat_vis_ne. by iApply internal_eq_sym.
      + iExFalso.
        iApply IT_fun_vis_ne.  by iApply internal_eq_sym.
      + iExFalso.
        iApply IT_tick_vis_ne.  by iApply internal_eq_sym.
      + iAssert (⌜1%fin = op⌝)%I as %<-.
        { by rewrite Vis_inj_op'. }
        rewrite Vis_inj'. iDestruct "H" as "[%Hfoo H]".
        fold_leibniz. subst i. admit.
  Admitted.

  Lemma sstep_equivI2 α1 σ σ' β1 β2 m :
    sstep α1 σ β1 σ' m →
    (▷ (β1 ≡ β2) ⊢ ⌜sstep α1 σ β2 σ' m⌝ : iProp)%I.
  Proof.
    inversion 1 as [α β σ1 H1 | α β k σ1 σ2 H1 H2 ]; subst.
    - rewrite H1.
      iIntros "H".
      iAssert (Tick β1 ≡ Tick β2)%I as "HH".
      { admit. }
      iPoseProof (sstep_equivI1 _ _ σ' with "HH") as "HHH".
      { by econstructor. }
  Abort.
  Lemma tick_safe_externalize α σ :
    (⊢ ∃ β σ', α ≡ Tick β ∧ σ ≡ σ' : iProp) → ∃ β σ', sstep α σ β σ' 0.
  Proof.
    intros Hprf.
    destruct (IT_dont_confuse α)
        as [Ha | [[n Ha] | [ [g Ha] | [[α' Ha]|[op [i [k Ha]]]] ]]].
    + exfalso. eapply uPred.pure_soundness.
      iPoseProof (Hprf) as "H".
      iDestruct "H" as (β σ') "[Ha Hs]". rewrite Ha.
      iApply (IT_tick_err_ne). by iApply internal_eq_sym.
    + exfalso. eapply uPred.pure_soundness.
      iPoseProof (Hprf) as "H".
      iDestruct "H" as (β σ') "[Ha Hs]". rewrite Ha.
      iApply (IT_nat_tick_ne with "Ha").
    + exfalso. eapply uPred.pure_soundness.
      iPoseProof (Hprf) as "H".
      iDestruct "H" as (β σ') "[Ha Hs]". rewrite Ha.
      iApply (IT_fun_tick_ne with "Ha").
    + exists α',σ. by econstructor; eauto.
    + exfalso. eapply uPred.pure_soundness.
      iPoseProof (Hprf) as "H".
      iDestruct "H" as (β σ') "[Ha Hs]". rewrite Ha.
      iApply (IT_tick_vis_ne). by iApply internal_eq_sym.
  Qed.
  Lemma effect_safe_externalize α σ :
    (⊢ ∃ β σ', (∃ k, α ≡ INPUT k ∧ reify α σ ≡ (σ', Tick β)) : iProp) →
    ∃ β σ', sstep α σ β σ' 1.
  Proof.
    intros Hprf.
    destruct (IT_dont_confuse α)
        as [Ha | [[n Ha] | [ [g Ha] | [[α' Ha]|[op [i [k Ha]]]] ]]].
    + exfalso. eapply uPred.pure_soundness.
      iPoseProof (Hprf) as "H".
      iDestruct "H" as (β σ' k) "[Ha _]". rewrite Ha.
      iApply (IT_vis_err_ne). by iApply internal_eq_sym.
    + exfalso. eapply uPred.pure_soundness.
      iPoseProof (Hprf) as "H".
      iDestruct "H" as (β σ' k) "[Ha _]". rewrite Ha.
      iApply (IT_nat_vis_ne with "Ha").
    + exfalso. eapply uPred.pure_soundness.
      iPoseProof (Hprf) as "H".
      iDestruct "H" as (β σ' k) "[Ha _]". rewrite Ha.
      iApply (IT_fun_vis_ne with "Ha").
    + exfalso. eapply uPred.pure_soundness.
      iPoseProof (Hprf) as "H".
      iDestruct "H" as (β σ' k) "[Ha _]". rewrite Ha.
      iApply (IT_tick_vis_ne with "Ha").
    + assert (op = (1%fin : opid ioΣ)).
      { eapply uPred.pure_soundness.
        iPoseProof (Hprf) as "H".
        iDestruct "H" as (β σ' k') "[Ha _]". rewrite Ha.
        by rewrite Vis_inj_op'. }
      subst op. simpl in *. destruct i as [].
      destruct (reify (INPUT k) σ) as [σ1 α1] eqn:Hr.
      exists α1,σ1. econstructor; eauto.
      rewrite Hr.
      admit.
  Admitted.

  Lemma sstep_istep α β σ σ' m :
    sstep α σ β σ' m → (⊢ istep α σ β σ').
  Proof.
    inversion 1; simplify_eq/=.
    - iLeft. eauto.
    - iRight. iExists _. iSplit; eauto.
      iPureIntro.
      trans (reify (INPUT k) σ); first solve_proper; eauto.
  Qed.
  Lemma ssteps_isteps α β σ σ' n m :
    ssteps α σ β σ' (n,m) → (⊢ isteps α σ β σ' n).
  Proof.
    revert m α σ. induction n=> m α σ; inversion 1; simplify_eq /=.
    - rewrite isteps_unfold. iLeft. eauto.
    - rewrite isteps_unfold. iRight. iExists n, α2,σ2.
      repeat iSplit; eauto.
      + iApply sstep_istep; eauto.
      + iNext. iApply IHn; eauto.
  Qed.

  Lemma istep_ITV α αv β σ σ' :
    (IT_to_V α ≡ Some αv ⊢ istep α σ β σ' -∗ False : iProp)%I.
  Proof.
    rewrite /istep/=. iIntros "Hv [[H _]|H]".
    - iRewrite "H" in "Hv". rewrite IT_to_V_Tau.
      iApply (option_equivI with "Hv").
    - iDestruct "H" as (k) "[H _]".
      iRewrite "H" in "Hv". rewrite IT_to_V_Vis.
      iApply (option_equivI with "Hv").
  Qed.

  Lemma istep_err σ β σ' : istep Err σ β σ' ⊢ False.
  Proof.
    rewrite /istep/=. iDestruct 1 as "[[H _]|H]".
    - iApply (IT_tick_err_ne).
      by iApply internal_eq_sym.
    - iDestruct "H" as (k) "[H _]". iApply (IT_vis_err_ne).
      by iApply internal_eq_sym.
  Qed.
  Lemma istep_tick α β σ σ' : istep (Tick α) σ β σ' ⊢ ▷ (β ≡ α) ∧ σ ≡ σ'.
  Proof.
    simpl. iDestruct 1 as "[[H1 H2]|H]".
    - iFrame. iNext. by iApply internal_eq_sym.
    - iDestruct "H" as (k) "[H1 H2]".
      iExFalso. iApply (IT_tick_vis_ne with "H1").
  Qed.
  Lemma istep_INPUT ko β σ σ' :
    istep (INPUT ko) σ β σ' ⊢ reify (INPUT ko) σ ≡ (σ', Tick β).
  Proof.
    simpl. iDestruct 1 as "[[H1 H2]|H]".
    - iExFalso. iApply IT_tick_vis_ne.
      by iApply internal_eq_sym.
    - iDestruct "H" as (ko') "[H1 $]".
  Qed.

  Lemma isteps_val βv β k σ σ' :
    isteps (IT_of_V βv) σ β σ' k ⊢ IT_of_V βv ≡ β ∧ σ ≡ σ' ∧ ⌜k = 0⌝.
  Proof.
    destruct k as[|k].
    - rewrite isteps_0. iDestruct 1 as "[$ $]".
      eauto.
    - rewrite isteps_S. iDestruct 1 as (α1 σ1) "[Hs Hss]".
      iExFalso. iClear "Hss".
      unfold istep. simpl. iDestruct "Hs" as "[[Ht Hs]|Hs]".
      + destruct βv as[n|f]; iSimpl in "Ht".
        ++  iApply (IT_nat_tick_ne with "Ht").
        ++  iApply (IT_fun_tick_ne with "Ht").
      + iDestruct "Hs" as (ko) "[Ht Hs]".
        destruct βv as[n|f]; iSimpl in "Ht".
        ++  iApply (IT_nat_vis_ne with "Ht").
        ++  iApply (IT_fun_vis_ne with "Ht").
  Qed.
  Lemma isteps_tick α βv σ σ' k :
    isteps (Tick α) σ (IT_of_V βv) σ' k ⊢ ∃ k' : nat, ⌜k = (1 + k')%nat⌝ ∧ ▷ isteps α σ (IT_of_V βv) σ' k'.
  Proof.
    rewrite isteps_unfold.
    iDestruct 1 as "[[Hk [Ht Hs]] | H]".
    - iExFalso. destruct βv; iSimpl in "Ht".
      ++ iApply (IT_nat_tick_ne).
         by iApply (internal_eq_sym).
      ++ iApply (IT_fun_tick_ne).
         by iApply (internal_eq_sym).
    - iDestruct "H" as (k' α1 σ1) "[% [Hs Hss]]".
      iExists k'. iSplit; first by eauto.
      rewrite istep_tick.
      iDestruct "Hs" as "[Ha Hs]". iNext.
      iRewrite -"Ha". iRewrite "Hs". done.
  Qed.

  (** homomorphisms of interaction trees *)
  Class IT_hom (f : IT → IT) := {
      hom_ne :: NonExpansive f;
      hom_tick: ∀ α, f (Tick α) ≡ Tick (f α);
      hom_vis : ∀ op i ko, f (Vis op i ko) ≡ Vis op i (laterO_map (OfeMor f) ◎ ko);
      hom_err : f Err ≡ Err
    }.
  #[export] Instance IT_hom_proper f `{!IT_hom f} : Proper ((≡) ==> (≡)) f.
  Proof. apply ne_proper. apply _. Qed.

  Lemma istep_hom (f : IT → IT) `{IT_hom f} α σ β σ' :
    istep α σ β σ' ⊢ istep (f α) σ (f β) σ'.
  Proof.
    iDestruct 1 as "[[Ha Hs]|H]".
    - iRewrite "Ha". iLeft. iSplit; eauto. iPureIntro. apply hom_tick.
    - iDestruct "H" as (k) "[#Ha Hr]".
      pose (f' := OfeMor f).
      iRight. iExists (laterO_map f' ◎ k).
      iAssert (f (INPUT k) ≡ INPUT (laterO_map f' ◎ k))%I as "Hf".
      { iPureIntro. apply hom_vis. }
      iRewrite "Ha". iRewrite "Ha" in "Hr". iRewrite "Hf".
      iSplit; first done.
      iApply (reify_input_cont with "Hr").
  Qed.

  Lemma IT_hom_val_inv α f `{!IT_hom f} :
    is_Some (IT_to_V (f α)) → is_Some (IT_to_V α).
  Proof.
    destruct (IT_dont_confuse α)
      as [Ha | [[n Ha] | [ [g Ha] | [[la Ha]|[op [i [k Ha]]]] ]]].
    - rewrite Ha hom_err. rewrite IT_to_V_Err. done.
    - rewrite Ha IT_to_V_Nat. done.
    - rewrite Ha IT_to_V_Fun. done.
    - rewrite Ha hom_tick. rewrite IT_to_V_Tau.
      intros []. exfalso. naive_solver.
    - rewrite Ha. rewrite hom_vis.
      rewrite IT_to_V_Vis. intros []. exfalso. naive_solver.
  Qed.

  Lemma istep_hom_inv α σ β σ' `{!IT_hom f} :
    istep (f α) σ β σ' ⊢ ⌜is_Some (IT_to_V α)⌝
    ∨ (IT_to_V α ≡ None ∧ ∃ α', istep α σ α' σ' ∧ ▷ (β ≡ f α')).
  Proof.
    iIntros "H".
    destruct (IT_dont_confuse α)
      as [Ha | [[n Ha] | [ [g Ha] | [[la Ha]|[op [i [k Ha]]]] ]]].
    - iExFalso. iApply (istep_err σ β σ').
      iAssert (f α ≡ Err)%I as "Hf".
      { iPureIntro. by rewrite Ha hom_err. }
      iRewrite "Hf" in "H". done.
    - iLeft. iPureIntro. rewrite Ha IT_to_V_Nat. done.
    - iLeft. iPureIntro. rewrite Ha IT_to_V_Fun. done.
    - iAssert (α ≡ Tick la)%I as "Ha"; first by eauto.
      iAssert (f (Tick la) ≡ Tick (f la))%I as "Hf".
      { iPureIntro. rewrite hom_tick. done. }
      iRight. iRewrite "Ha". iRewrite "Ha" in "H".
      iRewrite "Hf" in "H". rewrite istep_tick.
      iDestruct "H" as "[Hb Hs]". iSplit.
      { by rewrite IT_to_V_Tau. }
      iExists la. iSplit; last eauto.
      unfold istep. iLeft. iSplit; eauto.
    - iRight.
      pose (fi:=OfeMor f).
      iAssert (f α ≡ Vis op i (laterO_map fi ◎ k))%I as "Hf".
      { iPureIntro. by rewrite Ha hom_vis. }
      iRewrite "Hf" in "H".
      rewrite {1}/istep. iSimpl in "H".
      iDestruct "H" as "[[H _]|H]".
      + iExFalso. iApply (IT_tick_vis_ne).
        iApply internal_eq_sym. done.
      + iDestruct "H" as (k') "[#Ha Hr]".
        iPoseProof (Vis_inj_op' with "Ha") as "->".
        iPoseProof (Vis_inj' with "Ha") as "[%Hi Hk]".
        fold_leibniz. subst i.
        iAssert (Vis (1%fin : opid ioΣ) () (laterO_map fi ◎ k) ≡ INPUT (laterO_map fi ◎ k))%I as "HI".
        { done. }
        iSimpl in "Hr".
        iAssert (reify (INPUT (laterO_map fi ◎ k)) σ ≡ (σ', Tick β))%I with "[Hr]" as "Hr".
        { iExact "Hr". }
        iPoseProof (reify_input_cont_inv k fi with "Hr") as (α') "[Hr Ha']".
        iAssert (reify α σ ≡ (σ', Tick α'))%I with "[Hr]" as "Hr".
        { iRewrite -"Hr". iPureIntro. repeat f_equiv.
          apply Ha. }
        iSplit. { iPureIntro. by rewrite Ha IT_to_V_Vis. }
        iExists α'. iFrame "Ha'".
        rewrite /istep. iRight.
        iExists k. iFrame "Hr".
        iPureIntro. apply Ha.
  Qed.

  Definition le_int_pre (self : natO -n> natO -n> iProp) : (natO -n> natO -n> iProp) :=
    λne k1 k2, (k1 ≡ 0 ∨ ∃ k1' k2', k1 ≡ S k1' ∧ k2 ≡ S k2' ∧ ▷ self k1' k2')%I.
  #[export] Instance le_int_contractive : Contractive le_int_pre.
  Proof. solve_contractive. Qed.
  Definition le_int : natO -n> natO -n> iProp := fixpoint le_int_pre.
  Lemma le_int_unfold k1 k2 :
    le_int k1 k2 ≡ (k1 ≡ 0 ∨ ∃ k1' k2', k1 ≡ S k1' ∧ k2 ≡ S k2' ∧ ▷ le_int k1' k2')%I.
  Proof. apply (fixpoint_unfold le_int_pre k1 k2). Qed.
  #[export] Instance le_int_persistent n m : Persistent (le_int n m).
  Proof.
    unfold Persistent.
    iIntros "H".
    iLöb as "IH" forall (n m).
    rewrite le_int_unfold.
    iDestruct "H" as "[#H|H]".
    - iModIntro. eauto with iFrame.
    - iDestruct "H" as (n' m') "(#H1 & #H2 & H)".
      iAssert (<pers> ▷ le_int n' m')%I with "[H]" as "#HH".
      { iApply bi.later_persistently_1.
        iNext. by iApply "IH". }
      iModIntro. iRight. iExists _,_.
      repeat iSplit; eauto.
  Qed.

  Lemma le_int_later_le n m : le_int n m ⊢ ▷^n ⌜n ≤ m⌝.
  Proof.
    iLöb as "IH" forall (n m).
    rewrite le_int_unfold.
    iDestruct 1 as "[%|H]".
    - fold_leibniz. subst.
      simpl. iPureIntro ; lia.
    - iDestruct "H" as (n' m') "(% & % & H)".
      fold_leibniz. subst.
      simpl. iNext.
      iSpecialize ("IH" with "H"). iNext.
      iDestruct "IH" as %fff. iPureIntro; lia.
  Qed.

  Lemma isteps_hom_inv α σ (βv : ITV) σ' `{!IT_hom f} k :
    isteps (f α) σ (IT_of_V βv) σ' k
           ⊢ ∃ k1 σ1 αv, (le_int k1 k) ∧ isteps α σ (IT_of_V αv) σ1 k1 ∧
                         ▷^k1 isteps (f (IT_of_V αv)) σ1 (IT_of_V βv) σ' (k-k1).
  Proof.
    iInduction (k) as [|k] "IH" forall (α σ).
    - rewrite isteps_0. iDestruct 1 as "[Hf Hs]".
      iAssert (⌜is_Some (IT_to_V (f α))⌝)%I as %Hsome.
      {  iRewrite "Hf". iPureIntro. rewrite IT_to_of_V //. }
      apply IT_hom_val_inv in Hsome; last apply _.
      destruct Hsome as [αv Ha].
      iAssert (IT_of_V αv ≡ α)%I as "Ha".
      { iApply IT_of_to_V. by rewrite Ha. }
      iExists 0,σ,αv. iRewrite -"Hs".
      repeat iSplit; eauto.
      ++ rewrite le_int_unfold. eauto.
      ++ rewrite isteps_0. iSplit; eauto.
         (* iModIntro. *)
         iApply internal_eq_sym. done.
      ++ simpl. rewrite isteps_0.
         iSplit; eauto.
         iRewrite -"Hf". iRewrite "Ha". done.
    - iIntros "#H".
      iAssert (isteps (f α) σ (IT_of_V βv) σ' (S k)) as "H2"; first by eauto.
      rewrite {2}isteps_S. iDestruct "H2" as (α1 σ1) "[Hstep Hsteps]".
      rewrite istep_hom_inv. iDestruct "Hstep" as "[%Hfoo | Hfoo]".
      + destruct Hfoo as [αv Hav].
        iAssert (IT_of_V αv ≡ α)%I as "Ha".
        { iApply IT_of_to_V. by rewrite Hav. }
        iExists 0, σ, αv.
        repeat iSplit.
        ++ rewrite le_int_unfold; eauto.
        ++ rewrite isteps_0. iSplit; eauto.
           by iApply internal_eq_sym.
        ++ rewrite Nat.sub_0_r.
           iRewrite "Ha".
           done.
      + iDestruct "Hfoo" as "[Hbar Hfoo]".
        iDestruct "Hfoo" as (β) "[Hst Heq]".
        iAssert (▷ ∃ (k1 : natO) (σ0 : stateO) (αv : ITV),
                      le_int k1 k ∧ isteps β σ1 (IT_of_V αv) σ0 k1
                      ∧ ▷^k1 isteps (f (IT_of_V αv)) σ0 (IT_of_V βv) σ' (k - k1))%I as "HH".
        { iNext. iApply "IH".
          iRewrite -"Heq". done. }
        iDestruct "HH" as (k1 σ2 αv) "[Hk [HH1 HH2]]".
        iExists (S k1),σ2,αv. repeat iSplit.
        ++ rewrite (le_int_unfold (S _)).
           iRight. iExists _, _. eauto.
        ++ rewrite (isteps_S α).
           iExists β,σ1. iFrame "Hst HH1".
        ++ simpl. iFrame "HH2".
  Qed.

  Lemma le_int_step_fupdN_add (n m : nat) (Eo Ei : coPset) (P : iProp) :
    le_int n m ⊢ (|={Eo}[Ei]▷=>^n |={Eo}[Ei]▷=>^(m-n) P) -∗ (|={Eo}[Ei]▷=>^m P).
  Proof.
    iLöb as "IH" forall(n m).
    rewrite le_int_unfold.
    iDestruct 1 as "[Hn|Hn]".
    - iRewrite "Hn". simpl. rewrite Nat.sub_0_r. eauto.
    - iDestruct "Hn" as (n' m') "(-> & -> & Hnm)".
      iIntros "H". simpl.
      iMod "H" as "H".
      iModIntro. iNext.
      iMod "H" as "H". iModIntro.
      iApply ("IH" with "Hnm H").
  Qed.

  Lemma step_fupdN_frame_later_l Eo Ei n {PROP : bi} `{!BiFUpd PROP} (R Q : PROP) :
    (▷^n R ∗ |={Eo}[Ei]▷=>^n Q) ⊢ |={Eo}[Ei]▷=>^n (R ∗ Q).
  Proof.
    iInduction n as [|n] "IH"; first by eauto.
    simpl.
    iIntros "[HR H]".
    iMod "H" as "H". iModIntro. iNext.
    iApply "IH". iMod "H" as "H".
    iModIntro. by iFrame.
  Qed.

  Lemma IC_bind (f : IT → IT) `{IT_hom f} (α : IT) Φ :
    IC α {{ k1; βv, IC (f (IT_of_V βv)) {{ k2; βv, Φ (k1+k2) βv }} }} ⊢ IC (f α) {{ Φ }}.
  Proof.
    iIntros "H".
    iIntros (σ σ' βv k) "Hs Hsts".
    rewrite isteps_hom_inv.
    iDestruct "Hsts" as  (k1 σ1 αv) "[#Hk1 [Ha #Hf]]".
    iSpecialize ("H" with "Hs Ha").
    set (k2 := k-k1).
    iApply (le_int_step_fupdN_add with "Hk1").
    iPoseProof (le_int_later_le with "Hk1") as "Hkk1".
    iPoseProof (step_fupdN_frame_later_l with "[$Hf $H]") as "H".
    iPoseProof (step_fupdN_frame_later_l with "[$Hkk1 $H]") as "H".
    iApply (step_fupdN_wand with "H").
    iIntros "(%Hk1 & Hst & H)". iMod "H" as "[Hs H]".
    iSpecialize ("H" with "Hs Hst").
    unfold k2.
    assert (Hk : k1 + (k - k1) = k) by lia.
    rewrite Hk. done.
  Qed.

  Lemma IC_tick (α : IT) Φ :
    ▷ IC α {{ k; v, Φ (1+k) v }} ⊢ IC (Tick α) {{ Φ }}.
  Proof.
    simpl. rewrite {2}/ic.
    iIntros "H". iIntros (σ σ' βv k) "Hσ Hst".
    destruct k.
    { rewrite isteps_0. iDestruct "Hst" as "[Ha Hs]".
      iExFalso. destruct βv; simpl.
      - iApply IT_nat_tick_ne. iApply internal_eq_sym. done.
      - iApply IT_fun_tick_ne. iApply internal_eq_sym. done.
    }
    rewrite isteps_S. iDestruct "Hst" as (α1 σ1) "[Ha Hst]".
    rewrite istep_tick. iDestruct "Ha" as "[Ha Hs]".
    iSimpl. iApply fupd_mask_intro; first solve_ndisj.
    iIntros "Hcl".
    iNext. iMod "Hcl" as "_". iModIntro.
    iRewrite -"Hs" in "Hst".
    iRewrite "Ha" in "Hst".
    iApply ("H" with "Hσ Hst").
  Qed.

  Lemma IC_refine ko (β : IT) (σ σ' : state) Φ :
    reify (INPUT ko) σ ≡ (σ', Tick β) →
    has_state σ -∗ (has_state σ' -∗ ▷ IC β {{ λ k v, Φ (1+k) v }})
          -∗ IC (INPUT ko) {{ Φ }}.
  Proof.
    intros Hrfy. iIntros "Hs HIC".
    rewrite {2}/ic. iIntros (σ0 σ0' βv k) "Hσ0 Hst".
    iAssert ((σ : stateO) ≡ σ0)%I with "[Hs Hσ0]" as %Hss.
    { rewrite /has_state /state_interp.
      iCombine "Hσ0 Hs" as "Hs".
      iDestruct (own_valid with "Hs") as "Hs".
      rewrite excl_auth_agreeI.
      by iApply (internal_eq_sym (σ0 : stateO) (σ : stateO)). }
    destruct k.
    { rewrite isteps_0. iDestruct "Hst" as "[Ha Hst]".
      iExFalso. destruct βv; simpl.
      - iApply IT_nat_vis_ne. iApply internal_eq_sym. done.
      - iApply IT_fun_vis_ne. iApply internal_eq_sym. done.
    }
    rewrite isteps_S. iDestruct "Hst" as (α1 σ1) "[Ha Hst]".
    rewrite istep_INPUT.
    iEval (rewrite -Hss) in "Hσ0".
    iAssert (▷ (α1 ≡ β) ∧ of_state σ' ≡ σ1)%I with "[Ha]" as "[Ha1 Hss]".
    { rewrite -Hss. rewrite Hrfy.
      iPoseProof (prod_equivI with "Ha") as "[Ha Hs]". simpl.
      unfold of_state; iFrame. iNext.
      by iApply internal_eq_sym. }
    iSimpl.
    iMod (own_update_2 with "Hs Hσ0") as "[Hs Hs0]".
    { apply (excl_auth_update _ _ (of_state σ')). }
    iSpecialize ("HIC" with "Hs0").
    iApply fupd_mask_intro; first solve_ndisj.
    iIntros "Hcl".
    iNext. iMod "Hcl" as "_".
    iModIntro.
    iSpecialize ("HIC" $! σ' σ0' βv k with "Hs").
    iRewrite -"Hss" in "Hst".
    iApply ("HIC" with "[Hst Ha1]").
    { iRewrite -"Ha1". done. }
  Qed.

  Lemma IC_ssteps α σ βv σ' k m (Ψ : nat → ITV → iProp) :
    ssteps α σ (IT_of_V βv) σ' (k, m) →
    state_interp σ ∗ IC α {{ Ψ }}
              ⊢ |={⊤}[∅]▷=>^k |={⊤}=> state_interp σ' ∗ Ψ k βv.
  Proof.
    iIntros (Hst) "[Hs HIC]".
    iAssert (isteps α σ (IT_of_V βv) σ' k) as "Hst".
    { by iApply ssteps_isteps. }
    iApply ("HIC" with "Hs Hst").
  Qed.


  Lemma IC_wand α Φ Φ' :
    IC α {{ Φ }} -∗
    (∀ k βv, Φ k βv -∗ Φ' k βv) -∗
    IC α {{ Φ' }}.
  Proof.
    unfold ic. iIntros "Hic Hphi".
    iIntros (σ σ' βv k) "Hs Hsts".
    iSpecialize ("Hic" with "Hs Hsts").
    iApply (step_fupdN_wand with "Hic").
    iIntros "H1". iMod "H1" as "[$ Hphi1]".
    iModIntro. by iApply "Hphi".
  Qed.

  Lemma IC_val βv Φ `{∀k, NonExpansive (Φ k)}:
    Φ 0 βv ⊢ IC (IT_of_V βv) {{ Φ }}.
  Proof.
    iIntros "Hphi". iIntros (σ σ' αv k) "Hs Hsts".
    rewrite isteps_val. iDestruct "Hsts" as "[Hab [-> ->]]".
    iFrame. iModIntro. rewrite IT_of_V_inj'.
    by iRewrite -"Hab".
  Qed.
End ic.


Section wp.
  Context `{!invGS_gen hlc Σ, !stateG Σ}.
  Notation IT := (IT ioΣ).
  Notation ITV := (ITV ioΣ).
  Notation iProp := (iProp Σ).


  Program Definition wp_pre (Φ : ITV → iProp) (self : IT -n> iProp) : IT -n> iProp
    := λne α,
      ((∃ αv, IT_to_V α ≡ Some αv ∧ Φ αv)
     ∨ (IT_to_V α ≡ None ∧ ∀ σ, state_interp σ -∗
           (∃ α' σ', istep α σ α' σ')  (* α is safe *)
             ∧ (∀ σ' β, istep α σ β σ' ={⊤}[∅]▷=∗ state_interp σ' ∗ self β)))%I.
  Next Obligation. solve_proper. Qed.

  #[local] Instance wp_pre_contractive Φ : Contractive (wp_pre Φ).
  Proof. unfold wp_pre. solve_contractive. Qed.
  Definition wp α Φ := fixpoint (wp_pre Φ) α.
  Lemma wp_unfold α Φ :
    wp α Φ ≡
       ((∃ αv, IT_to_V α ≡ Some αv ∧ Φ αv)
     ∨ (IT_to_V α ≡ None ∧ ∀ σ, state_interp σ -∗
                    (∃ α' σ', istep α σ α' σ')  (* α is safe *)
                  ∧ (∀ σ' β, istep α σ β σ' ={⊤}[∅]▷=∗ state_interp σ' ∗ wp β Φ)))%I.
  Proof. apply (fixpoint_unfold (wp_pre Φ) _). Qed.

  Notation "'WP' α {{ β , Φ } }" := (wp α (λ β, Φ))
    (at level 20, α, Φ at level 200,
     format "'WP'  α  {{  β ,  Φ  } }") : bi_scope.

  Notation "'WP' α {{ Φ } }" := (wp α Φ)
    (at level 20, α, Φ at level 200,
     format "'WP'  α  {{  Φ  } }") : bi_scope.

  #[export] Instance wp_ne n :
    Proper ((dist n) ==> (pointwise_relation _ (dist n)) ==> (dist n)) wp.
  Proof. Admitted.
  (*   intros α1 α2 Ha Φ1 Φ2 Hp. *)
  (*   revert α1 α2 Ha. *)
  (*   induction n as [|n]=>α1 α2 Ha. *)
  (*   - rewrite !wp_unfold. *)
  (*     f_equiv; first by repeat f_equiv. *)
  (*     f_equiv; first solve_proper. f_equiv. f_equiv. f_equiv. *)
  (*     f_equiv. f_equiv. f_equiv. f_equiv. f_equiv. *)
  (*     + solve_proper. *)
  (*     + f_equiv; first solve_proper. *)
  (*       f_equiv. f_contractive. inversion Hlt. *)
  (*   - rewrite !wp_unfold. *)
  (*     f_equiv; first by repeat f_equiv. *)
  (*     f_equiv; first solve_proper. f_equiv. f_equiv. f_equiv. *)
  (*     f_equiv. f_equiv. f_equiv. f_equiv. f_equiv. *)
  (*     + solve_proper. *)
  (*     + f_equiv; first solve_proper. *)
  (*       f_equiv. f_contractive. f_equiv. *)
  (*       f_equiv. admit. *)
  (* Admitted. *)

  Lemma wp_val α αv Φ :
    IT_to_V α ≡ Some αv ⊢ Φ αv -∗ WP α {{ Φ }}.
  Proof.
    iIntros "Ha Hp". rewrite wp_unfold. iLeft.
    iExists αv. by iFrame.
  Qed.

  Lemma wp_val_inv α αv Φ `{!NonExpansive Φ} :
    IT_to_V α ≡ Some αv ⊢ WP α {{ Φ }} -∗ Φ αv.
  Proof.
    iIntros "Ha". rewrite wp_unfold.
    iDestruct 1 as "[H|[Ha2 H]]".
    + iDestruct "H" as (αv2) "[Ha2 Hp]".
      iRewrite "Ha" in "Ha2".
      iPoseProof (option_equivI with "Ha2") as "Ha".
      by iRewrite "Ha".
    + iRewrite "Ha" in "Ha2".
      iPoseProof (option_equivI with "Ha2") as "Ha".
      done.
  Qed.

  Lemma wp_val_inv' α αv Φ `{!NonExpansive Φ} :
    IT_to_V α ≡ Some αv → WP α {{ Φ }} ⊢ Φ αv.
  Proof.
    intros H. iApply wp_val_inv. eauto.
  Qed.

  Lemma wp_bind (f : IT → IT) `{IT_hom f} (α : IT) Φ `{!NonExpansive Φ} :
    WP α {{ βv, WP (f (IT_of_V βv)) {{ βv, Φ βv }} }} ⊢ WP (f α) {{ Φ }}.
  Proof.
    assert (NonExpansive (λ βv0, WP f (IT_of_V βv0) {{ βv1, Φ βv1 }})%I).
    { solve_proper. }
    iIntros "H". iLöb as "IH" forall (α).
    rewrite (wp_unfold (f _)).
    destruct (IT_to_V (f α)) as [βv|] eqn:Hfa.
    - iLeft. iExists βv. iSplit; first done.
      assert (is_Some (IT_to_V α)) as [αv Ha].
      { apply (IT_hom_val_inv _ f). rewrite Hfa.
        done. }
      rewrite wp_val_inv'; last first.
      { by rewrite Ha. }
      iApply wp_val_inv.
      { rewrite -Hfa. done. }
      iAssert (α ≡ IT_of_V αv)%I as "Hav".
      { iApply internal_eq_sym.
        iApply IT_of_to_V. by rewrite Ha. }
      iRewrite "Hav". done.
    - iRight. iSplit; eauto.
      iIntros (σ) "Hs". iSplit.
      { (* safety *)
        rewrite wp_unfold.
        iDestruct "H" as "[H|[Ha H]]".
        - iDestruct "H" as (αv) "[Ha H]".
          iAssert (IT_of_V αv ≡ α)%I with "[Ha]" as "Hav".
          { iApply IT_of_to_V. done. }
          iRewrite "Hav" in "H".
          rewrite wp_unfold. rewrite Hfa.
          iDestruct "H" as "[H | [_ H]]".
          { iDestruct "H" as (?) "[H _]".
            iExFalso. iApply (option_equivI with "H"). }
          iSpecialize ("H" with "Hs").
          iDestruct "H" as "[$ _]".
        - iSpecialize ("H" with "Hs").
          iDestruct "H" as "[H _]".
          iDestruct "H" as (α1 σ1) "Hst".
          iExists (f α1),σ1. iApply (istep_hom with "Hst"). }
      (* preservation *)
      iIntros (σ' β) "#Hst".
      iAssert (istep (f α) σ β σ')%I as "Hst'".
      { done. }
      rewrite {1}istep_hom_inv. iDestruct "Hst" as "[%Ha | Hst]".
      (* looking at whether α is a value *)
      + destruct Ha as [av Hav].
        iAssert (IT_of_V av ≡ α)%I with "[]" as "Hav".
        { iApply IT_of_to_V. by rewrite Hav. }
        rewrite wp_val_inv'; last first.
        { by rewrite Hav. }
        rewrite wp_unfold. iRewrite "Hav" in "H". rewrite Hfa.
        iDestruct "H" as "[H | [_ H]]".
        { iDestruct "H" as (?) "[H _]".
          iExFalso. iApply (option_equivI with "H"). }
        iSpecialize ("H" with "Hs").
        iDestruct "H" as "[_ H]".
        by iApply "H".
      + iDestruct "Hst" as "[Ha Hst]".
        iDestruct "Hst" as (α') "[Hst Hb]".
        rewrite wp_unfold.
        iRewrite "Ha" in "H".
        iDestruct "H" as "[H | [_ H]]".
        { iDestruct "H" as (?) "[H _]".
            iExFalso. iApply (option_equivI with "H"). }
        iSpecialize ("H" with "Hs").
        iDestruct "H" as "[_ H]".
        iSpecialize ("H" with "Hst").
        iMod "H" as "H". iModIntro. iNext.
        iMod "H" as "[$ H]". iModIntro.
        iRewrite "Hb". by iApply "IH".
  Qed.


  Lemma wp_tick α Φ :
    ▷ WP α {{ Φ }} ⊢ WP (Tick α) {{ Φ }}.
  Proof.
    iIntros "H". rewrite (wp_unfold (Tick _)).
    iRight. iSplit.
    { iPureIntro. apply IT_to_V_Tau. }
    iIntros (σ) "Hs". iSplit.
    - iExists α,σ. iLeft. eauto.
    - iIntros (σ' β) "Hst". rewrite istep_tick.
      iDestruct "Hst" as "[Hb <-]".
      iApply step_fupd_intro; first solve_ndisj.
      iNext. iRewrite "Hb". by iFrame.
  Qed.

  Lemma wp_input β σ σ' ko Φ :
    reify (INPUT ko) σ ≡ (σ', Tick β) →
    has_state σ -∗ (has_state σ' -∗ ▷ WP β {{ Φ }})
          -∗ WP (INPUT ko) {{ Φ }}.
  Proof.
    intros Hreify. iIntros "Hs Hb".
    rewrite (wp_unfold (INPUT _)).
    iRight. iSplit.
    { iPureIntro. apply IT_to_V_Vis. }
    iIntros (σ0) "Hσ0".
    (* we know what the real state is *)
    iAssert ((σ : stateO) ≡ σ0)%I with "[Hs Hσ0]" as %Hss.
    { rewrite /has_state /state_interp.
      iCombine "Hσ0 Hs" as "Hs".
      iDestruct (own_valid with "Hs") as "Hs".
      rewrite excl_auth_agreeI.
      by iApply (internal_eq_sym (σ0 : stateO) (σ : stateO)). }
    fold_leibniz.
    rewrite -Hss.
    iSplit.
    { (* it is safe *)
      iExists β,σ'. iRight. iExists ko. eauto. }
    iIntros (σ0' α0) "Hst". rewrite istep_INPUT.
    iAssert (▷ (α0 ≡ β) ∧ of_state σ0' ≡ σ')%I with "[Hst]" as "[Ha1 Hss]".
    { rewrite Hreify.
      iPoseProof (prod_equivI with "Hst") as "[Ha Hs]". simpl.
      unfold of_state. iSplit.
      + iNext. by iApply internal_eq_sym.
      + by iApply internal_eq_sym.  }
    unfold of_state. iDestruct "Hss" as %Hss'.
    rewrite Hss'.
    iMod (own_update_2 with "Hs Hσ0") as "[Hs Hs0]".
    { apply (excl_auth_update _ _ (of_state σ')). }
    iSpecialize ("Hb" with "Hs0"). iFrame "Hs".
    iApply fupd_mask_intro; first solve_ndisj.
    iIntros "Hcl".
    iNext. iMod "Hcl" as "_".
    iRewrite "Ha1". done.
  Qed.

  Lemma wp_istep α σ β σ' Ψ :
    ⊢ istep α σ β σ' -∗ state_interp σ -∗ WP α {{ Ψ }}
    ={⊤}[∅]▷=∗ state_interp σ' ∗ WP β {{ Ψ }}.
  Proof.
    iIntros "Hstep Hs H".
    rewrite (wp_unfold α). iDestruct "H" as "[H|[Ha H]]".
    - iExFalso. iDestruct "H" as (αv) "[H _]".
      iApply (istep_ITV with "H Hstep").
    - iSpecialize ("H" with "Hs"). iDestruct "H" as "[_ H]".
      iApply ("H" with "Hstep").
  Qed.

  Lemma wp_isteps k α σ β σ' Ψ :
    ⊢ isteps α σ β σ' k -∗ state_interp σ -∗ WP α {{ Ψ }}
    ={⊤}[∅]▷=∗^k state_interp σ' ∗ WP β {{ Ψ }}.
  Proof.
    iInduction k as [|k] "IH" forall (α σ).
    - rewrite isteps_0. iIntros "[Ha Hs]".
      simpl. iRewrite "Ha". iRewrite "Hs".
      eauto with iFrame.
    - rewrite isteps_S. iDestruct 1 as (α1 σ1) "[Hstep Hsts]".
      iIntros "Hst H". iSimpl.
      iPoseProof (wp_istep with "Hstep Hst H") as "H".
      iMod "H" as "H". iModIntro. iNext.
      iMod "H" as "[Hs H]". iModIntro.
      iApply ("IH" with "Hsts Hs H").
  Qed.

  Lemma wp_ssteps α σ β σ' k m Ψ :
    ssteps α σ β σ' (k, m) →
    state_interp σ ∗ WP α {{ Ψ }}
              ⊢ |={⊤}[∅]▷=>^k state_interp σ' ∗ WP β {{ Ψ }}.
  Proof.
    iIntros (Hst) "[Hs HIC]".
    iAssert (isteps α σ β σ' k) as "Hst".
    { by iApply ssteps_isteps. }
    iApply (wp_isteps with "Hst Hs HIC").
  Qed.

  Lemma wp_ssteps_safe α σ β σ' k m Ψ :
    ssteps α σ β σ' (k, m) →
    state_interp σ ∗ WP α {{ Ψ }}
                 ⊢ |={⊤}[∅]▷=>^k ⌜is_Some (IT_to_V β)⌝ ∨ ∃ β2 σ2, istep β σ' β2 σ2.
  Proof.
    intros Hst. rewrite wp_ssteps//.
    apply step_fupdN_mono.
    iIntros "[Hst H]".
    rewrite wp_unfold. iDestruct "H" as "[H | [Hb H]]".
    - iLeft. iDestruct "H" as (av) "[H _]".
      destruct (IT_to_V β) as [βv|]; first by eauto.
      iExFalso. iApply (option_equivI with "H").
    - iRight. iDestruct ("H" with "Hst") as "[$ _]".
  Qed.

  Lemma wp_ssteps_val α σ βv σ' k m Ψ `{!NonExpansive Ψ} :
    ssteps α σ (IT_of_V βv) σ' (k, m) →
    state_interp σ ∗ WP α {{ Ψ }}
                 ⊢ |={⊤}[∅]▷=>^k Ψ βv.
  Proof.
    intros Hst. rewrite wp_ssteps//.
    apply step_fupdN_mono.
    iIntros "[Hst H]".
    rewrite wp_unfold. iDestruct "H" as "[H | [Hb H]]".
    - iDestruct "H" as (av) "[H HH]".
      rewrite IT_to_of_V. iPoseProof (option_equivI with "H") as "H".
      by iRewrite "H".
    - rewrite IT_to_of_V.
      iExFalso. iApply (option_equivI with "Hb").
  Qed.

End wp.

Notation "'IC' α {{ k ; β , Φ } }" := (ic α (λ k β, Φ))
    (at level 20, α, Φ at level 200,
     format "'IC'  α  {{ k ;  β ,  Φ  } }") : bi_scope.
Notation "'IC' α {{ Φ } }" := (ic α Φ)
    (at level 20, α, Φ at level 200,
     format "'IC'  α  {{  Φ  } }") : bi_scope.

Lemma IC_adequacy α σ βv σ' k m (ψ : nat → (ITV ioΣ) → Prop) :
  ssteps α σ (IT_of_V βv) σ' (k, m) →
  (∀ `{H1 : !invGS_gen hlc Σ} `{H2: !stateG Σ},
      (has_state σ ⊢ IC α {{ k; w, ⌜ψ k w⌝ }})%I) →
  ψ k βv.
Proof.
  intros Hst Hprf.
  pose (Σ :=  #[invΣ; stateΣ]).
  cut (⊢ ⌜ψ k βv⌝ : iProp Σ)%I.
  { intros HH.
    eapply uPred.pure_soundness;eauto. }
  eapply (step_fupdN_soundness_lc' _ (S k) k).
  intros lc Hinv. iIntros "Hk".
  rewrite step_fupdN_S_fupd. simpl.
  iMod (new_state_interp σ) as (sg) "[Hs Hs2]".
  iPoseProof (Hprf lc Σ Hinv sg with "Hs2") as "Hic".
  iPoseProof (IC_ssteps with "[$Hs $Hic]") as "Hphi".
  { eassumption. }
  iApply fupd_mask_intro; first solve_ndisj.
  iIntros "Hcl". iNext. iMod "Hcl" as "_". iModIntro.
  iApply (step_fupdN_mono with "Hphi").
  apply fupd_mono, bi.sep_elim_r. tc_solve.
Qed.



Section logrel.
  Context `{!invGS_gen hlc Σ, !stateG Σ}.
  Notation IT := (IT ioΣ).
  Notation ITV := (ITV ioΣ).
  Notation iProp := (iProp Σ).
  Canonical Structure exprO S := leibnizO (expr S).
  Canonical Structure valO S := leibnizO (val S).

  Definition interp_E {S} (V : ITV → val S → iProp) (α : IT) (e : expr S) : iProp
    := ∀ σ, has_state σ -∗
           IC α {{ k; βv, ∃ m v σ', ⌜prim_steps e σ (Val v) σ' (k,m)⌝
                                                ∗ V βv v ∗ has_state σ' }}.
  #[export] Instance interp_E_ne {S} (V : ITV → val S → iProp) :
    NonExpansive2 V → NonExpansive2 (interp_E V).
  Proof. solve_proper. Qed.


  Definition interp_nat {S} (βv : ITV) (v : val S) : iProp :=
    (∃ n, βv ≡ NatV n ∧ ⌜v = Lit n⌝)%I.
  Definition interp_arr {S} V1 V2 (βv : ITV) (vf : val S) : iProp :=
    (∃ f, IT_of_V βv ≡ Fun f ∧ ∀ αv v, V1 αv v → interp_E V2 (APP' (Fun f) (IT_of_V αv)) (App (Val vf) (Val v)))%I.

  Fixpoint interp_V (τ : ty) {S} : ITV → (val S) → iProp
    := match τ with
       | Tnat => interp_nat
       | Tarr τ1 τ2 => interp_arr (interp_V τ1) (interp_V τ2)
       end.

  #[export] Instance interp_V_ne (τ : ty) {S} : NonExpansive2 (@interp_V τ S).
  Proof.
    induction τ; simpl; solve_proper.
  Qed.

  Lemma interp_E_ret {S} τ αv (v : val S) :
    (interp_V τ αv v ⊢ interp_E (interp_V τ) (IT_of_V αv) (Val v)).
  Proof.
    iIntros "Hv". unfold ic.
    iIntros (σ) "Hs". iApply IC_val.
    { solve_proper. }
    iExists 0,v,σ. iFrame.
    iPureIntro. constructor.
  Qed.

  Definition logrel {S} (τ : ty) : IT → expr S → iProp :=
    interp_E (interp_V τ).
  #[export] Instance logrel_ne {S} (τ : ty) :
    NonExpansive2 (@logrel S τ).
  Proof. solve_proper. Qed.


  (** We also have a separate description of "semantic contexts" *)
  (* XXX: these can be just separate lemmas *)
Inductive sectx_item :=
| AppLSCtx : ITV → sectx_item
| AppRSCtx : IT → sectx_item
| NatOpLSCtx : nat_op → ITV → sectx_item
| NatOpRSCtx : nat_op → IT → sectx_item
| IfSCtx : IT → IT → sectx_item
.
Definition sectx := list (sectx_item).

Definition fill_sectx_item (K' : sectx_item) : IT → IT := λ α,
  match K' with
  | AppLSCtx v => APP' α (IT_of_V v)
  | AppRSCtx β => APP' β α
  | NatOpLSCtx op βv => NATOP (do_natop op) α (IT_of_V βv)
  | NatOpRSCtx op β => NATOP (do_natop op) β α
  | IfSCtx β1 β2 => IF α β1 β2
  end.
Definition fill_sectx (K : sectx) (α : IT)  : IT := foldl (flip fill_sectx_item) α K.
Global Instance fill_sectx_item_ne Ks : NonExpansive (fill_sectx_item Ks).
Proof. solve_proper. Qed.
Global Instance fill_sectx_ne Ks : NonExpansive (fill_sectx Ks).
Proof. induction Ks; solve_proper. Qed.
Global Instance fill_sectx_item_proper Ks : Proper ((≡) ==> (≡)) (fill_sectx_item Ks).
Proof. solve_proper. Qed.
Global Instance fill_sectx_proper Ks : Proper ((≡) ==> (≡)) (fill_sectx Ks).
Proof. induction Ks; solve_proper. Qed.
Definition fill_sectx_itemO K : IT -n> IT := OfeMor (fill_sectx_item K).
Definition fill_sectxO K : IT -n> IT := OfeMor (fill_sectx K).

Lemma fill_sectx_item_tick Ksi α :
  fill_sectx_item Ksi (Tick α) ≡ Tick $ fill_sectx_item Ksi α.
Proof.
  destruct Ksi; cbn-[Tick].
  - rewrite !APP_APP'_ITV.
    apply APP_Tick.
  - apply APP'_Tick_r.
  - apply NATOP_ITV_Tick_l.
  - apply NATOP_Tick_r.
  - apply IF_Tick.
Qed.

Lemma fill_sectx_tick Ks α :
  fill_sectx Ks (Tick α) ≡ Tick $ fill_sectx Ks α.
Proof.
  revert α. induction Ks; intro α; simpl; first done.
  rewrite -IHKs. f_equiv.
  apply fill_sectx_item_tick.
Qed.

Lemma fill_sectx_item_INPUT Ks op i k :
  fill_sectx_item Ks (Vis op i k) ≡ Vis op i (laterO_map (fill_sectx_itemO Ks) ◎ k).
Proof.
  destruct Ks; cbn-[INPUT IF].
  - rewrite !APP_APP'_ITV.
    rewrite APP_Vis. f_equiv.
    intro. cbn-[laterO_map]. repeat f_equiv.
    intro. simpl. by rewrite APP_APP'_ITV.
  - rewrite APP'_Vis_r. f_equiv. solve_proper.
  - rewrite NATOP_ITV_Vis_l. f_equiv. solve_proper.
  - rewrite NATOP_Vis_r. f_equiv. solve_proper.
  - rewrite IF_Vis. f_equiv. solve_proper.
Qed.

Lemma fill_sectx_INPUT Ks op i k :
  fill_sectx Ks (Vis op i k) ≡ Vis op i (laterO_map (fill_sectxO Ks) ◎ k).
Proof.
  revert k. induction Ks as [|Ki Ks]; intro k; simpl.
  - f_equiv. intro x. symmetry. eapply laterO_map_id.
  - trans (Vis op i (laterO_map (fill_sectxO Ks) ◎ (laterO_map (fill_sectx_itemO Ki) ◎ k))); last first.
    { f_equiv. intro x. simpl. rewrite -later_map_compose.
      f_equiv. }
    rewrite -IHKs. f_equiv.
    apply fill_sectx_item_INPUT.
Qed.

Lemma fill_sectx_item_err Ks : fill_sectx_item Ks Err ≡ Err.
Proof.
  destruct Ks; cbn-[INPUT IF].
  - rewrite !APP_APP'_ITV.
    by rewrite APP_Err.
  - by rewrite APP'_Err.
  - by rewrite NATOP_ITV_Err_l.
  - by rewrite NATOP_Err_r.
  - by rewrite IF_Err.
Qed.
Lemma fill_sectx_err Ks : fill_sectx Ks Err ≡ Err.
Proof.
  induction Ks; simpl; first done.
  by rewrite fill_sectx_item_err.
Qed.

#[export] Instance fill_sectx_hom  Ks : IT_hom (fill_sectx Ks)
  := {| hom_ne   := fill_sectx_ne Ks
      ; hom_tick := fill_sectx_tick Ks
      ; hom_vis  := fill_sectx_INPUT Ks
      ; hom_err  := fill_sectx_err Ks
     |}.

End logrel.

Local Definition κ {S} : ITV ioΣ → val S :=  λ x,
    match x with
    | NatV n => Lit n
    | _ => Lit 0
    end.
Lemma interp_E_nat_adequacy {S} (α : IT ioΣ) (e : expr S) n σ σ' k m :
  (∀ {Σ:gFunctors}`{H1 : !invGS_gen hlc Σ} `{H2: !stateG Σ},
      (True ⊢ interp_E interp_nat α e)%I) →
  ssteps α σ (Nat n) σ' (k,m) → ∃ m σ', prim_steps e σ (Val $ Lit n) σ' (k,m).
Proof.
  intros Hlog Hst.
  pose (ϕ := λ (k : nat) (βv : ITV ioΣ),
          ∃ m σ', prim_steps e σ (Val $ κ βv) σ' (k,m)).
  cut (ϕ k (NatV n)).
  { destruct 1 as ( m' & σ2 & Hm). simpl in Hm.
    eexists; eauto. }
  eapply (IC_adequacy).
  { simpl; eassumption. }
  intros hlc Σ Hinv1 Hst1.
  iIntros "Hs".
  iPoseProof (Hlog with "[//] Hs") as "Hlog".
  iApply (IC_wand with"Hlog").
  iIntros (k' βv). iIntros "H".
  iDestruct "H" as (m' v σ1' Hsts) "[Hi Hsts]".
  unfold ϕ.
  unfold interp_nat. iDestruct "Hi" as (l) "[Hβ %]".
  iAssert (IT_of_V βv ≡ IT_of_V (NatV l))%I with "[Hβ]" as "H".
  { iApply f_equivI. iExact "Hβ". }
  iAssert (⌜βv = NatV l⌝)%I with "[-]" as %Hfoo.
  { destruct βv as [r|f]; simpl.
    - iPoseProof (Nat_inj' with "H") as "%Hr".
      fold_leibniz. eauto.
    - iExFalso. iApply (IT_nat_fun_ne).
      iApply internal_eq_sym. iExact "H". }
  rewrite Hfoo.
  iSimpl in "H".
  iPoseProof (Nat_inj' with "H") as "%Hbar".
  fold_leibniz. subst. iPureIntro.
  simpl. eauto.
Qed.
 
