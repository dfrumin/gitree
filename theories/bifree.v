From iris.algebra Require Import cofe_solver.
From iris.base_logic Require Export base_logic.
From iris.prelude Require Import options.
From gitree Require Import prelude.

Section recursion.
  Variable (F : oFunctor).
  Context `{oFunctorContractive F}.
  Context (F_cofe : forall T {H:Cofe T}, Cofe (oFunctor_apply F T)).

  Variable (S : solution F).
  Context `{!Inhabited S}.

  Variable (P : ofe).
  Context `{!Cofe P, !Inhabited P}.

  Variable (P_unfold : P -n> oFunctor_apply F P).
  Variable (P_fold : oFunctor_car F (sumO S P) (prodO S P) -n> P).

  Definition S_fold : oFunctor_apply F S -n> S := ofe_iso_1 S.
  Definition S_unfold : S -n> oFunctor_apply F S := ofe_iso_2 S.

  Program Definition F_rec_pre
          (self : prodO (S -n> P) (P -n> S))
     : prodO (S -n> P) (P -n> S).
  Proof using Cofe0 F P P_fold P_unfold S.
    set (self1 := fst self).
    set (self2 := snd self).
    simple refine (_,_).
    { refine (_ ◎ S_unfold).
      refine (P_fold ◎ oFunctor_map F (_,_)).
      - refine (sumO_rec idfun self2).
      - refine (λne x, (x, self1 x)).
        solve_proper. }
    { refine (S_fold ◎ oFunctor_map F (self1,self2) ◎ P_unfold). }
  Defined.

  Instance F_rec_pre_contractive : Contractive F_rec_pre.
  Proof using Cofe0 F H P P_fold P_unfold S.
    unfold F_rec_pre.
    intros n [h1 k1] [h2 k2] Hhk.
    repeat (f_contractive
            || f_equiv || destruct Hhk as [Hh Hk]); eauto.
    intro x. simpl. f_equiv. apply Hh.
  Qed.


  Definition F_rec : prodO (S -n> P) (P -n> S) :=
    let _ := _ : Inhabited P in
    fixpoint F_rec_pre.

  Lemma F_rec_unfold :
    F_rec ≡ F_rec_pre F_rec.
  Proof. apply (fixpoint_unfold F_rec_pre). Qed.

End recursion.
