From Equations Require Import Equations.
From iris.proofmode Require Import classes tactics.
From iris.algebra Require Import gmap.
From iris.heap_lang Require Export locations.
From gitree Require Import prelude.
From gitree.ugitree Require Import core reductions.

Opaque laterO_map.
(* why do i need to repeat this?*)
Opaque Nat Fun Tau Err Vis.

(** State and operations *)
Canonical Structure locO := leibnizO loc.
Record state := { state_heap : gmap loc nat }.
#[export] Instance state_inhabited : Inhabited state
  := populate ({| state_heap := inhabitant |}).
Notation stateO := (leibnizO state).

Definition state_read  : loc * state → option (nat * state)
  := λ '(l,σ), n ← state_heap σ !! l;
               Some (n, σ).
#[export] Instance state_read_ne : NonExpansive (state_read : prodO locO stateO → optionO (prodO natO stateO)).
Proof.
  intros n [l1 s1] [l2 s2]. simpl. intros [-> Hs].
  apply (option_mbind_ne _ (λ n, Some (n, s1)) (λ n, Some (n, s2)));
    solve_proper.
Qed.

Definition state_write : (loc * nat) * state → option (unit * state)
  := λ '((l,n),s), let s' := {| state_heap := <[l:=n]>(state_heap s) |}
                   in Some ((), s').
#[export] Instance state_write_ne : NonExpansive (state_write : prodO (prodO locO natO) stateO → optionO (prodO unitO stateO)).
Proof.
  intros n [[l1 m1] s1] [[l2 m2] s2]. simpl.
  intros [[Hl Hm] Hs]. solve_proper.
Qed.

Definition state_alloc : nat * state → option (loc * state)
  := λ '(n,s), let l := Loc.fresh (dom (state_heap s)) in
               let s' := {| state_heap := <[l:=n]>(state_heap s) |} in
               Some (l, s').
#[export] Instance state_alloc_ne : NonExpansive (state_alloc : prodO natO stateO → optionO (prodO locO stateO)).
Proof.
  intros n [m1 s1] [m2 s2]. simpl.
  intros [Hm Hs]. solve_proper.
Qed.


Program Definition ReadE : opInterp :=  {|
  Ins := locO;
  Outs := natO;
|}.
Program Definition WriteE : opInterp :=  {|
  Ins := prodO locO natO;
  Outs := unitO;
|}.
Program Definition AllocE : opInterp :=  {|
  Ins := natO;
  Outs := locO;
|}.

Definition storeE : opsInterp := @[ReadE;WriteE;AllocE].

Section constructors.

  Program Definition READ : locO -n> (natO -n> laterO (IT storeE)) -n> IT storeE
    := λne l k, Vis (E:=storeE) (inl ()) l k.
  Solve Obligations with solve_proper.

  Program Definition WRITE : locO -n> natO -n> (laterO (IT storeE)) -n> IT storeE
    := λne l n k, Vis (E:=storeE) (inr (inl ())) (l,n) (λne _, k).
  Next Obligation. solve_proper. Qed.
  Next Obligation.
    simpl. repeat intro.
    repeat f_equiv. intro; done.
  Qed.
  Next Obligation. solve_proper. Qed.
  Next Obligation. solve_proper. Qed.

  Program Definition ALLOC : natO -n> (locO -n> laterO (IT storeE)) -n> IT storeE
    := λne n k, Vis (E:=storeE) (inr (inr (inl ()))) n k.
  Solve Obligations with solve_proper.


  (* Lemma APP_INPUT (α : laterO (IT storeE)) k : *)
  (*   APP (INPUT k) α ≡ INPUT (laterO_map (Ppa α) ◎ k). *)
  (* Proof. by rewrite APP_Vis. Qed. *)
  (* Lemma APP'_INPUT (α : IT storeE) k : *)
  (*   APP' α (INPUT k) ≡ INPUT (laterO_map (APP' α) ◎ k). *)
  (* Proof. by rewrite APP'_Vis_r. Qed. *)
  (* Lemma IF_INPUT (α1 α2 : IT storeE) k : *)
  (*   IF (INPUT k) α1 α2 ≡ INPUT (laterO_map (IF_last α1 α2) ◎ k). *)
  (* Proof. by rewrite IF_Vis. Qed. *)
  (* Lemma NATOP_INPUT_r (α : IT storeE) f k : *)
  (*   NATOP f α (INPUT k) ≡ INPUT (laterO_map (NATOP f α) ◎ k). *)
  (* Proof. by rewrite NATOP_Vis_r. Qed. *)
  (* Lemma NATOP_INPUT_l (βv : ITV storeE) f k : *)
  (*   NATOP f (INPUT k) (IT_of_V βv) ≡ INPUT (laterO_map (flipO (NATOP f) (IT_of_V βv)) ◎ k). *)
  (* Proof. by rewrite NATOP_ITV_Vis_l. Qed. *)
End constructors.

(** * Reification of the top-level events *)
Section reify_io.
  Notation IT := (IT storeE).

  Definition reify_io : reify_eff storeE stateO.
  Proof.
    intros op X HX.
    set (P:=λ (op : fin 3), prodO (oFunctor_apply (Ins (storeE op)) X) stateO -n>
  optionO (prodO (oFunctor_apply (Outs (storeE op)) X) stateO)).
    apply (Fin.caseS' op P); unfold P; clear P op.
    { simpl. (* READ *)
      simple refine
        (λne (us : prodO locO stateO), state_read us : optionO (prodO natO stateO)). }
    intro op.
    set (P:=λ (op : fin 2), prodO (oFunctor_apply (Ins (storeE (FS op))) X) stateO -n>
  optionO (prodO (oFunctor_apply (Outs (storeE (FS op))) X) stateO)).
    apply (Fin.caseS' op P); unfold P; clear P; clear op.
    { simpl. (* WRITE *)
      simple refine
        (λne (us : prodO (prodO locO natO) stateO),
          state_write us : optionO (prodO unitO stateO)). }
    intro op.
    set (P:=λ (op : fin 1), prodO (oFunctor_apply (Ins (storeE (FS (FS op)))) X) stateO -n>
  optionO (prodO (oFunctor_apply (Outs (storeE (FS (FS op)))) X) stateO)).
    apply (Fin.caseS' op P); unfold P; clear P; clear op.
    { simpl. (* ALLOC *)
      simple refine
        (λne (us : prodO natO stateO),
          state_alloc us : optionO (prodO locO stateO)). }
    apply Fin.case0.
  Defined.

  #[export] Instance reify_io_reifier : Reifier storeE stateO reify_io.
  Proof. split; apply _. Qed.

  Notation reify := (reify reify_io).
  Notation sstep := (sstep reify_io).
  Notation ssteps := (ssteps reify_io).

End reify_io.
