From Equations Require Import Equations.
From iris.proofmode Require Import classes tactics.
From iris.si_logic Require Export siprop.
From gitree Require Import prelude.
From gitree.ugitree Require Import core lambda reductions weakestpre.
From gitree Require Export iolang.

Opaque laterO_map.
(* why do i need to repeat this?*)
Opaque Nat Fun Tau Err Vis Tick.
Opaque APP APP' IF NATOP.

(** language *)
Notation stateO := (leibnizO state).

Program Definition InputΣ : opInterp :=  {|
  Ins := unitO;
  Outs := (natO)%OF;
|}.

Definition ioΣ : opsInterp := @[InputΣ].

Section constructors.
  Context {E : opsInterp}.
  Context `{!subEff ioΣ E}.
  Notation IT := (IT E).
  Notation ITV := (ITV E).

  Definition INPUT : (natO -n> laterO IT) -n> IT.
  Proof using E subEff0.
    simple refine (λne k, Vis (E:=E) (subEff_opid (inl ()))
                            (subEff_conv_ins (F:=ioΣ) (op:=(inl ())) ())
                            (k ◎ subEff_conv_outs2 (F:=ioΣ) (op:=(inl ())))).
    solve_proper.
  Defined.

  (* Lemma APP_INPUT (α : laterO IT) k : *)
  (*   APP (INPUT k) α ≡ INPUT (laterO_map (Ppa α) ◎ k). *)
  (* Proof. *)
  (*   rewrite APP_Vis. unfold INPUT. simpl. f_equiv. *)
  (*   by rewrite ccompose_assoc. *)
  (* Qed. *)
  (* Lemma APP'_INPUT (α : IT) k : *)
  (*   APP' α (INPUT k) ≡ INPUT (laterO_map (APP' α) ◎ k). *)
  (* Proof. *)
  (*   rewrite APP'_Vis_r. unfold INPUT. simpl. f_equiv. *)
  (*   by rewrite ccompose_assoc. *)
  (* Qed. *)
  (* Lemma IF_INPUT (α1 α2 : IT) k : *)
  (*   IF (INPUT k) α1 α2 ≡ INPUT (laterO_map (IF_last α1 α2) ◎ k). *)
  (* Proof. *)
  (*   rewrite IF_Vis. unfold INPUT. simpl. f_equiv. *)
  (*   by rewrite ccompose_assoc. *)
  (* Qed. *)
  (* Lemma NATOP_INPUT_r (α : IT) f k : *)
  (*   NATOP f α (INPUT k) ≡ INPUT (laterO_map (NATOP f α) ◎ k). *)
  (* Proof. *)
  (*   rewrite NATOP_Vis_r. unfold INPUT. simpl. f_equiv. *)
  (*   by rewrite ccompose_assoc. *)
  (* Qed. *)
  (* Lemma NATOP_INPUT_l (β : IT @[ioΣ]) f k `{!AsVal β} : *)
  (*   NATOP f (INPUT k) β ≡ INPUT (laterO_map (flipO (NATOP f) β) ◎ k). *)
  (* Proof. by rewrite NATOP_ITV_Vis_l. Qed. *)
End constructors.
Opaque INPUT.

  Definition reify_io : reify_eff InputΣ stateO.
  Proof.
    intros op X HX.
    destruct op as []; simpl.
    simple refine (λne (us : prodO unitO stateO), Some $ update_input (sndO us) : optionO (prodO natO stateO)).
    intros n [[] s1] [[] s2] [_ Hs].
    repeat f_equiv. apply Hs.
  Defined.

(** * Reification of the top-level events *)
Section reify_io.
  Notation IT := (IT ioΣ).
  Notation ITV := (ITV ioΣ).
  Definition rs : reifiers ioΣ := reifiers_cons _ _ reify_io (reifiers_nil).

  Notation reify := (reify rs).
  Notation sstep := (sstep rs).
  Notation ssteps := (ssteps rs).

  (** * The interpretation *)

  Definition do_natop (op : nat_op) (x y : nat) : nat :=
    match op with
    | Add => x+y
    | Sub => x-y
    end.

  (** Interpreting individual operators *)
  Fixpoint interp_scope (S : scope) : ofe :=
    match S with
    | [] => unitO
    | τ::Sc => prodO IT (interp_scope Sc)
    end.

  Instance interp_scope_cofe S : Cofe (interp_scope S).
  Proof. induction S; simpl; apply _. Qed.

  Instance interp_scope_inhab S : Inhabited (interp_scope S).
  Proof. induction S; simpl; apply _. Defined.

  Equations interp_var {S : scope} (v : var S) : interp_scope S -n> IT :=
    interp_var (S:=(_::_))     Vz := fstO;
    interp_var (S:=(_::Sc)) (Vs v) := interp_var v ◎ sndO.

  Instance interp_var_ne S (v : var S) : NonExpansive (@interp_var S v).
  Proof.
    intros n D1 D2 HD12. induction v; simp interp_var.
    - by f_equiv.
    - eapply IHv. by f_equiv.
  Qed.

  Global Instance interp_var_proper S (v : var S) : Proper ((≡) ==> (≡)) (interp_var v).
  Proof. apply ne_proper. apply _. Qed.

  Program Definition interp_input {A} : A -n> IT := λne env, INPUT (NextO ◎ Nat).

  Program Definition interp_natop {A} (op : nat_op) (t1 t2 : A -n> IT) : A -n> IT :=
    λne env, NATOP (do_natop op) (t1 env) (t2 env).
  Next Obligation.
    repeat intro. repeat f_equiv; eauto.
  Qed.
  Global Instance interp_natop_ne A op : NonExpansive2 (@interp_natop A op).
  Proof.
    unfold interp_natop. repeat intro. simpl.
    repeat f_equiv; eauto.
  Qed.
  Typeclasses Opaque interp_natop.

  Program Definition interp_rec_pre {A} (body : prodO IT (prodO IT A) -n> IT)
    : laterO (A -n> IT) -n> A -n> IT :=
    λne self env, Fun $ laterO_map (λne (self : A -n> IT) (a : IT),
                                     body (self env,(a,env))) self.
  Next Obligation. solve_proper. Qed.
  Next Obligation. solve_proper. Qed.
  Next Obligation.
    repeat intro. simpl. repeat f_equiv; eauto.
    repeat intro. simpl. solve_proper.
  Qed.
  Next Obligation. solve_proper. Qed.


  Definition interp_rec {A} body : A -n> IT := mmuu (interp_rec_pre body).
  Program Definition ir_unf {A} (body : prodO IT (prodO IT A) -n> IT) env : IT -n> IT :=
    λne a, body (interp_rec body env, (a,env)).
  Next Obligation. solve_proper. Qed.

  Lemma interp_rec_unfold {A} (body : prodO IT (prodO IT A) -n> IT) env :
    interp_rec body env ≡ Fun $ Next $ ir_unf body env.
  Proof.
    trans (interp_rec_pre body (Next (interp_rec body)) env).
    { f_equiv. rewrite /interp_rec. apply mmuu_unfold. }
    simpl. rewrite laterO_map_Next. repeat f_equiv.
    simpl. unfold ir_unf. intro. simpl. reflexivity.
  Qed.

  Program Definition interp_app {A} (t1 t2 : A -n> IT) : A -n> IT :=
    λne env, APP' (t1 env) (t2 env).
  Next Obligation.
    repeat intro. repeat f_equiv; eauto.
  Qed.
  Global Instance interp_app_ne A : NonExpansive2 (@interp_app A).
  Proof. solve_proper. Qed.
  Typeclasses Opaque interp_app.

  Program Definition interp_if {A} (t0 t1 t2 : A -n> IT) : A -n> IT :=
    λne env, IF (t0 env) (t1 env) (t2 env).
  Next Obligation. solve_proper. Qed.
  Global Instance interp_if_ne A n :
    Proper ((dist n) ==> (dist n) ==> (dist n) ==> (dist n)) (@interp_if A).
  Proof. solve_proper. Qed.

  Program Definition interp_nat (n : nat) {A} : A -n> IT :=
    λne env, Nat n.

  (** Interpretation for all the syntactic categories: values, expressions, contexts *)
  Fixpoint interp_val {S} (v : val S) : interp_scope S -n> IT :=
    match v with
    | Lit n => interp_nat n
    | RecV e => interp_rec (interp_expr e)
    end
  with interp_expr {S} (e : expr S) : interp_scope S -n> IT :=
    match e with
    | Val v => interp_val v
    | Var x => interp_var x
    | Rec e => interp_rec (interp_expr e)
    | App e1 e2 => interp_app (interp_expr e1) (interp_expr e2)
    | NatOp op e1 e2 => interp_natop op (interp_expr e1) (interp_expr e2)
    | If e e1 e2 => interp_if (interp_expr e) (interp_expr e1) (interp_expr e2)
    | Input => interp_input
    end.

  Program Definition interp_ctx_item {S : scope} (K : ectx_item S) : interp_scope S -n> IT -n> IT :=
    match K with
    | AppLCtx v2 => λne env t, interp_app (constO t) (interp_val v2) env
    | AppRCtx e1 => λne env t, interp_app (interp_expr e1) (constO t) env
    | NatOpLCtx op v2 => λne env t, interp_natop op (constO t) (interp_val v2) env
    | NatOpRCtx op e1 => λne env t, interp_natop op (interp_expr e1) (constO t) env
    | IfCtx e1 e2 => λne env t, interp_if (constO t) (interp_expr e1) (interp_expr e2) env
    end.
  Opaque interp_app.
  Next Obligation. repeat intro; repeat f_equiv; eauto. Qed.
  Next Obligation. solve_proper. Qed.
  Next Obligation. repeat intro; repeat f_equiv; eauto. Qed.
  Next Obligation. solve_proper. Qed.
  Next Obligation. repeat intro; repeat f_equiv; eauto. Qed.
  Next Obligation.
    repeat intro; repeat f_equiv; eauto.
    intro. simpl. f_equiv; eauto; solve_proper.
  Qed.
  Next Obligation. solve_proper. Qed.
  Next Obligation. solve_proper. Qed.
  Next Obligation. solve_proper. Qed.
  Next Obligation. solve_proper. Qed.
  Transparent interp_app.

  #[global] Instance interp_val_asval {S} (v : val S) E : AsVal (interp_val v E).
  Proof.
    destruct v; simpl; first apply _.
    rewrite interp_rec_unfold. apply _.
  Qed.
  Program Fixpoint interp_ectx {S} (K : ectx S) : interp_scope S -n> IT -n> IT
    :=
      match K with
      | [] => λne env, idfun
      | Ki::K => λne env, interp_ectx K env ◎ interp_ctx_item Ki env
      end.
  Next Obligation. solve_proper. Defined. (* XXX why can't i qed here? *)

  Lemma interp_ctx_item_fill {S} (Ki : ectx_item S) e env :
    interp_expr (fill_item Ki e) env ≡ interp_ctx_item Ki env (interp_expr e env).
  Proof. destruct Ki; reflexivity. Qed.

  Lemma interp_ectx_fill {S} (K : ectx S) e env :
    interp_expr (fill K e) env ≡ interp_ectx K env (interp_expr e env).
  Proof.
    revert e; induction K as [|Ki K]=>e; first done.
    rewrite IHK. simpl. rewrite interp_ctx_item_fill. done.
  Qed.

  (** Applying renamings and subsitutions to the interpretation of scopes *)
  Equations interp_rens_scope {S S' : scope}
            (E : interp_scope S') (s : rens S S') : interp_scope S :=
    interp_rens_scope (S:=[]) E s := tt : interp_scope [];
    interp_rens_scope (S:=_::_) E s :=
      (interp_var (hd_ren s) E, interp_rens_scope E (tl_ren s)).

  Equations interp_subs_scope {S S' : scope}
            (E : interp_scope S') (s : subs S S') : interp_scope S :=
    interp_subs_scope (S:=[]) E s := tt : interp_scope [];
    interp_subs_scope (S:=_::_) E s :=
      (interp_expr (hd_sub s) E, interp_subs_scope E (tl_sub s)).


  Global Instance interp_rens_scope_ne S D n :
    Proper ((dist n) ==> (≡) ==> (dist n)) (@interp_rens_scope S D).
  Proof.
    intros E E' HE s1 s2 Hs.
    induction S as [|τ' S]; simp interp_rens_scope; auto.
    f_equiv.
    - unfold hd_ren; rewrite Hs.
      by rewrite HE.
    - apply IHS. intros v. unfold tl_ren; by rewrite Hs.
  Qed.
  Global Instance interp_subs_scope_ne S D n :
    Proper ((dist n) ==> (≡) ==> (dist n)) (@interp_subs_scope S D).
  Proof.
    intros E E' HE s1 s2 Hs.
    induction S as [|τ' S]; simp interp_subs_scope; auto.
    f_equiv.
    - unfold hd_sub; by rewrite Hs HE.
    - apply IHS. intros v. unfold tl_sub; by rewrite Hs.
  Qed.
  Global Instance interp_rens_scope_proper S D :
    Proper ((≡) ==> (≡) ==> (≡)) (@interp_rens_scope S D).
  Proof.
    intros E E' HE s1 s2 Hs.
    induction S as [|τ' S]; simp interp_rens_scope; auto.
    f_equiv.
    - unfold hd_ren; rewrite Hs.
      by rewrite HE.
    - apply IHS. intros v. unfold tl_ren; by rewrite Hs.
  Qed.
  Global Instance interp_subs_scope_proper S D :
    Proper ((≡) ==> (≡) ==> (≡)) (@interp_subs_scope S D).
  Proof.
    intros E E' HE s1 s2 Hs.
    induction S as [|τ' S]; simp interp_subs_scope; auto.
    f_equiv.
    - unfold hd_sub; by rewrite Hs HE.
    - apply IHS. intros v. unfold tl_sub; by rewrite Hs.
  Qed.

  (** ** The substituion lemma, for renamings and substitutions *)
  Lemma interp_rens_scope_tl_ren {S D} x E (r : rens S D) :
    interp_rens_scope ((x, E) : interp_scope (()::D)) (tl_ren (rens_lift r))
                    ≡ interp_rens_scope E r.
  Proof.
    induction S as [|τ' S]; simp interp_rens_scope; eauto.
    f_equiv.
    { unfold hd_ren, tl_ren. simp rens_lift interp_var.
      done. }
    { rewrite -IHS. f_equiv. clear.
      intros v. dependent elimination v;
        unfold hd_ren, tl_ren; simp rens_lift; auto. }
  Qed.

  Lemma interp_rens_scope_idren {S} (E : interp_scope S) :
    interp_rens_scope E (@idren S) ≡ E.
  Proof.
    induction S as [|[] S]; simp interp_rens_scope.
    { by destruct E. }
    destruct E as [x E]. simp interp_var. simpl.
    f_equiv.
    trans (interp_rens_scope ((x, E) : interp_scope (()::S)) (tl_ren (rens_lift idren))).
    { f_equiv. intros v. unfold tl_ren.
      reflexivity. }
    rewrite interp_rens_scope_tl_ren.
    apply IHS.
  Qed.

  Lemma interp_expr_ren {S D : scope} (M : expr S) (r : rens S D) :
    ∀ (E : interp_scope D),
      interp_expr (ren_expr M r) E ≡ interp_expr M (interp_rens_scope E r)
  with interp_val_ren {S D : scope} (v : val S) (r : rens S D) :
    ∀ (E : interp_scope D),
      interp_val (ren_val v r) E ≡ interp_val v (interp_rens_scope E r).
  Proof.
    - revert D r. induction M=> D r E; simpl; simp ren_expr.
      all: try by (simpl; repeat intro; simpl; repeat f_equiv; eauto).
      + (* variable *) revert r.
        induction v=>r.
        * simp interp_var interp_rens_scope. done.
        * simp interp_var interp_rens_scope. simpl.
          apply (IHv (tl_ren r)).
      + (* recursive functions *) simp ren_expr. simpl.
        apply bi.siProp.internal_eq_soundness.
        iLöb as "IH".
        rewrite {2}interp_rec_unfold.
        rewrite {2}(interp_rec_unfold (interp_expr M)).
        iApply f_equivI. iNext. iApply internal_eq_pointwise.
        rewrite /ir_unf. iIntros (x). simpl.
        rewrite interp_expr_ren.
        iApply f_equivI.
        simp interp_rens_scope interp_var. simpl.
        rewrite !interp_rens_scope_tl_ren.
        iRewrite "IH".
        done.
    - revert D r. induction v=> D r E; simpl; simp ren_val; eauto.
      (* recursive functions *)
      simp ren_expr. simpl.
      apply bi.siProp.internal_eq_soundness.
      iLöb as "IH".
      rewrite {2}interp_rec_unfold.
      rewrite {2}(interp_rec_unfold (interp_expr e)).
      iApply f_equivI. iNext. iApply internal_eq_pointwise.
      rewrite /ir_unf. iIntros (x). simpl.
      rewrite interp_expr_ren.
      iApply f_equivI.
      simp interp_rens_scope interp_var. simpl.
      rewrite !interp_rens_scope_tl_ren.
      iRewrite "IH".
      done.
  Qed.

  Lemma interp_subs_scope_tl_sub {S D} x E (s : subs S D) :
    interp_subs_scope ((x, E) : interp_scope (()::D)) (tl_sub (subs_lift s))
                    ≡ interp_subs_scope E s.
  Proof.
    induction S as [|[] S]; simp interp_subs_scope; first done.
    f_equiv.
    { unfold hd_sub, tl_sub. simp subs_lift interp_var.
      unfold expr_lift. rewrite interp_expr_ren. f_equiv.
      trans (interp_rens_scope ((x, E) : interp_scope (()::D)) (tl_ren (rens_lift idren))).
      { f_equiv. intros v. unfold tl_ren.
        simp rens_lift idren. done. }
      rewrite interp_rens_scope_tl_ren.
      apply interp_rens_scope_idren. }
    { rewrite -IHS. f_equiv. clear.
      intros v. dependent elimination v;
        unfold hd_sub, tl_sub; simp subs_lift; auto. }
  Qed.

  Lemma interp_subs_scope_idsub {S} (env : interp_scope S) :
    interp_subs_scope env idsub ≡ env.
  Proof.
    induction S as [|[] S]; simp interp_subs_scope.
    { by destruct env. }
    destruct env as [x env].
    unfold hd_sub, idsub. simpl.
    simp interp_var. simpl. f_equiv.
    etrans; last first.
    { apply IHS. }
    rewrite -(interp_subs_scope_tl_sub x env idsub).
    repeat f_equiv. intro v. unfold tl_sub, idsub; simpl.
    simp subs_lift. unfold expr_lift. simp ren_expr. done.
  Qed.

  Lemma interp_expr_subst {S D : scope} (M : expr S) (s : subs S D) :
    ∀ (E : interp_scope D),
      interp_expr (subst_expr M s) E ≡ interp_expr M (interp_subs_scope E s)
  with interp_val_subst {S D : scope} (v : val S) (s : subs S D) :
    ∀ (E : interp_scope D),
      interp_val (subst_val v s) E ≡ interp_val v (interp_subs_scope E s).
  Proof.
    - revert D s. induction M=> D r E; simpl; simp subst_expr.
      all: try by (simpl; repeat intro; simpl; repeat f_equiv; eauto).
      + (* variable *) revert r.
        induction v=>r.
        * simp interp_var interp_rens_scope. done.
        * simp interp_var interp_rens_scope. simpl.
          apply (IHv (tl_sub r)).
      + (* recursive functions *) simpl.
        apply bi.siProp.internal_eq_soundness.
        iLöb as "IH".
        rewrite {2}interp_rec_unfold.
        rewrite {2}(interp_rec_unfold (interp_expr M)).
        iApply f_equivI. iNext. iApply internal_eq_pointwise.
        rewrite /ir_unf. iIntros (x). simpl.
        rewrite interp_expr_subst.
        iApply f_equivI.
        simp interp_subs_scope interp_var. simpl.
        rewrite !interp_subs_scope_tl_sub.
        iRewrite "IH".
        done.
    - revert D s. induction v=> D r E; simpl; simp subst_val; eauto.
      (* recursive functions *)
      simp subst_expr. simpl.
      apply bi.siProp.internal_eq_soundness.
      iLöb as "IH".
      rewrite {2}interp_rec_unfold.
      rewrite {2}(interp_rec_unfold (interp_expr e)).
      iApply f_equivI. iNext. iApply internal_eq_pointwise.
      rewrite /ir_unf. iIntros (x). simpl.
      rewrite interp_expr_subst.
      iApply f_equivI.
      simp interp_subs_scope interp_var. simpl.
      rewrite !interp_subs_scope_tl_sub.
      iRewrite "IH".
      done.
  Qed.

  (** ** Interpretation commutes with ticks and INPUT *)
  Lemma interp_ectx_item_INPUT {S} (Ki : ectx_item S) f env :
    interp_ctx_item Ki env (INPUT f) ≡ INPUT (laterO_map (interp_ctx_item Ki env) ◎ f).
  Proof.
    destruct Ki; cbn-[IF APP' INPUT Tick Tick_n get_nat2].
    Transparent INPUT.
    - rewrite !APP_APP'_ITV.
      rewrite APP_Vis. unfold INPUT. simpl. repeat f_equiv.
      intro.
      cbn-[APP]. repeat f_equiv. intro. simpl.
      by rewrite -APP_APP'_ITV.
    - rewrite APP'_Vis_r. unfold INPUT. simpl. repeat f_equiv.
      intro. simpl. repeat f_equiv. by intro.
    - rewrite NATOP_ITV_Vis_l/=.
      repeat f_equiv. intro x. simpl. reflexivity.
    - rewrite NATOP_Vis_r/=.
      repeat f_equiv. intro x. simpl. reflexivity.
    - rewrite IF_Vis/=. repeat f_equiv.
      by intro.
    Opaque INPUT.
  Qed.

  Lemma interp_ectx_INPUT {S} (K : ectx S) f env :
    interp_ectx K env (INPUT f) ≡ INPUT (laterO_map (interp_ectx K env) ◎ f).
  Proof.
    revert f. induction K as [|Ki K]=>f.
    - simpl. f_equiv. intro; simpl; by rewrite laterO_map_id.
    - simpl. rewrite interp_ectx_item_INPUT.
      rewrite IHK. f_equiv. intro. simpl.
      by rewrite -laterO_map_compose.
  Qed.

  Lemma interp_ectx_item_tick {S} (Ki : ectx_item S) env t n :
    interp_ctx_item Ki env (Tick_n n t) ≡ Tick_n n (interp_ctx_item Ki env t).
  Proof.
    destruct Ki; cbn-[Tick_n IF].
    - rewrite APP_APP'_ITV. rewrite APP_Tick_n.
      rewrite -APP_APP'_ITV. done.
    - rewrite APP'_Tick_r_n. done.
    - rewrite NATOP_ITV_Tick_n_l//.
    - rewrite NATOP_Tick_n_r//.
    - rewrite IF_Tick_n//.
  Qed.

  Lemma interp_ectx_tick {S} (K : ectx S) env t n :
    interp_ectx K env (Tick_n n t) ≡ Tick_n n (interp_ectx K env t).
  Proof.
    revert t. induction K as [|Ki K]=>t; first done.
    cbn-[Tick_n].
    trans (interp_ectx K env (Tick_n n $ interp_ctx_item Ki env t)).
    { f_equiv. apply interp_ectx_item_tick. }
    by rewrite IHK.
  Qed.

  (** ** Finally, preservation of reductions *)
  Lemma interp_expr_head_step {S} env (e : expr S) e' σ σ' n :
    head_step e σ e' σ' (n,0) →
    interp_expr e env ≡ Tick_n n $ interp_expr e' env.
  Proof.
    inversion 1; cbn-[IF APP' INPUT Tick get_nat2].
    - (*fun->val*)
      reflexivity.
    - (* app lemma *)
      rewrite APP_APP'_ITV.
      trans (APP (Fun (Next (ir_unf (interp_expr e1) env))) (Next $ interp_val v2 env)).
      { repeat f_equiv. apply interp_rec_unfold. }
      rewrite APP_Fun. simpl. rewrite Tick_eq. do 2 f_equiv.
      simplify_eq.
      rewrite interp_expr_subst. f_equiv.
      simp interp_subs_scope. unfold hd_sub, tl_sub. simp conssub.
      simpl. repeat f_equiv.
      generalize (Val (RecV e1)).
      generalize (Val v2).
      clear.
      intros e1 e2.
      trans (interp_subs_scope env idsub); last first.
      {  f_equiv. intro v. simp conssub. done. }
      symmetry.
      apply interp_subs_scope_idsub.
    - (* the natop stuff *)
      simplify_eq.
      destruct v1,v2; try naive_solver. simpl in *.
      rewrite NATOP_Nat.
      destruct op; simplify_eq/=; done.
    - by rewrite IF_True.
    - rewrite IF_False; eauto. lia.
  Qed.

  Lemma interp_expr_fill_no_reify {S} K env (e e' : expr S) σ σ' n :
    head_step e σ e' σ' (n,0) →
    interp_expr (fill K e) env ≡ Tick_n n $ interp_expr (fill K e') env.
  Proof.
    intros He.
    trans (interp_ectx K env (interp_expr e env)).
    { apply interp_ectx_fill. }
    trans (interp_ectx K env (Tick_n n (interp_expr e' env))).
    {  f_equiv. apply (interp_expr_head_step env) in He. apply He. }
    trans (Tick_n n $ interp_ectx K env (interp_expr e' env)); last first.
    { f_equiv. symmetry. apply interp_ectx_fill. }
    apply interp_ectx_tick.
  Qed.

  Lemma interp_expr_fill_yes_reify {S} K env (e e' : expr S) σ σ' n :
    head_step e σ e' σ' (n,1) →
    reify (interp_expr (fill K e) env) (σ,()) ≡ ((σ',()), Tick_n n $ interp_expr (fill K e') env).
  Proof.
    inversion 1; simplify_eq.
    trans (reify (interp_ectx K env (interp_expr Input env)) (σ,())).
    { f_equiv. by rewrite interp_ectx_fill. }
    simpl.
    trans (reify (INPUT (laterO_map (interp_ectx K env) ◎ (NextO ◎ Nat))) (σ,())).
    { f_equiv. rewrite interp_ectx_INPUT. done. }
    rewrite reify_vis_eq /= //; last first. { rewrite H5. done. }
    simpl. repeat f_equiv.
    simpl. rewrite laterO_map_Next Tick_eq. f_equiv. rewrite interp_ectx_fill. done.
  Qed.

  Lemma soundness {S} (e1 e2 : expr S) σ1 σ2 n m env :
    prim_step e1 σ1 e2 σ2 (n,m) →
    ssteps (interp_expr e1 env) (σ1,()) (interp_expr e2 env) (σ2,()) n.
  Proof.
    inversion 1; simplify_eq/=.
    destruct (head_step_io_01 _ _ _ _ _ _ H2); subst.
    - assert (σ1 = σ2) as ->.
      { eapply head_step_no_io; eauto. }
      eapply (interp_expr_fill_no_reify K) in H2.
      rewrite H2. eapply ssteps_tick_n.
    - inversion H2;subst. eapply (interp_expr_fill_yes_reify K env) in H2.
      rewrite interp_ectx_fill interp_ectx_INPUT.
      change 1 with (1+0). econstructor; last first.
      { apply ssteps_zero; reflexivity. }
      eapply sstep_reify.
      { Transparent INPUT. unfold INPUT. simpl.
        f_equiv. reflexivity. }
      rewrite -H2.
      do 2 f_equiv.
      rewrite interp_ectx_fill interp_ectx_INPUT.
      eauto.
  Qed.


  (** The logical relation *)
  Context `{!invGS_gen hlc Σ, !stateG rs Σ}.
  Notation iProp := (iProp Σ).
  Notation istep := (istep rs).
  Notation isteps := (isteps rs).
  Canonical Structure exprO S := leibnizO (expr S).
  Canonical Structure valO S := leibnizO (val S).

  Notation "'WP' α {{ β , Φ } }" := (wp rs α (λ β, Φ))
    (at level 20, α, Φ at level 200,
     format "'WP'  α  {{  β ,  Φ  } }") : bi_scope.

  Notation "'WP' α {{ Φ } }" := (wp rs α Φ)
    (at level 20, α, Φ at level 200,
     format "'WP'  α  {{  Φ  } }") : bi_scope.

  Definition logrel_expr {S} V (α : IT) (e : expr S) : iProp :=
    (∀ σ K, has_state (σ,()) -∗ WP α {{ βv, ∃ m v σ', ⌜prim_steps (fill K e) σ (fill K (Val v)) σ' m⌝
                                   ∗ V βv v ∗ has_state (σ',()) }})%I.
  Definition logrel_nat {S} (βv : ITV) (v : val S) : iProp :=
    (∃ n, βv ≡ NatV n ∧ ⌜v = Lit n⌝)%I.
  Definition logrel_arr {S} V1 V2 (βv : ITV) (vf : val S) : iProp :=
    (∃ f, IT_of_V βv ≡ Fun f ∧ ∀ αv v, V1 αv v → □ logrel_expr V2 (APP' (Fun f) (IT_of_V αv)) (App (Val vf) (Val v)))%I.

  Fixpoint logrel_val (τ : ty) {S} : ITV → (val S) → iProp
    := match τ with
       | Tnat => logrel_nat
       | Tarr τ1 τ2 => logrel_arr (logrel_val τ1) (logrel_val τ2)
       end.

  Definition logrel (τ : ty) {S} : IT → (expr S) → iProp
    := logrel_expr (logrel_val τ).

  #[export] Instance logrel_expr_ne {S} (V : ITV → val S → iProp) :
    NonExpansive2 V → NonExpansive2 (logrel_expr V).
  Proof. solve_proper. Qed.
  #[export] Instance logrel_nat_ne {S} : NonExpansive2 (@logrel_nat S).
  Proof. solve_proper. Qed.
  #[export] Instance logrel_val_ne (τ : ty) {S} : NonExpansive2 (@logrel_val τ S).
  Proof. induction τ; simpl; solve_proper. Qed.
  #[export] Instance logrel_expr_proper {S} (V : ITV → val S → iProp) :
    Proper ((≡) ==> (≡) ==> (≡)) V →
    Proper ((≡) ==> (≡) ==> (≡)) (logrel_expr V).
  Proof. solve_proper. Qed.
  #[export] Instance logrel_val_proper (τ : ty) {S} :
    Proper ((≡) ==> (≡) ==> (≡)) (@logrel_val τ S).
  Proof. induction τ; simpl; solve_proper. Qed.
  #[export] Instance logrel_persistent (τ : ty) {S} α v :
    Persistent (@logrel_val τ S α v).
  Proof.
    induction τ; simpl.
    - unfold logrel_nat. apply _.
    - unfold logrel_arr. admit.
  Admitted.

  Lemma logrel_bind {S} (f : IT → IT) (K : ectx S) `{!IT_hom f} e α τ1 V2 `{!NonExpansive2 V2} :
    ⊢ logrel_expr (logrel_val τ1) α e -∗
      (∀ v βv, logrel_val τ1 βv v -∗
                   logrel_expr V2 (f (IT_of_V βv)) (fill K (Val v))) -∗
      logrel_expr V2 (f α) (fill K e).
  Proof.
    iIntros "H1 H2".
    iLöb as "IH" forall (α e).
    iIntros (σ K1) "Hs".
    iApply wp_bind.
    { solve_proper. }
    iSpecialize ("H1" $! _ (K++K1) with "Hs").
    iApply (wp_wand with "H1").
    iIntros (αv). iDestruct 1 as ([m m'] v σ' Hsteps) "[H1 Hs]".
    iSpecialize ("H2" with "H1 Hs").
    iApply (wp_wand with "H2").
    iIntros (βv). iDestruct 1 as ([m2 m2'] v2 σ2' Hsteps2) "[H2 Hs]".
    iExists (m + m2, m' + m2'),v2,σ2'. iFrame "H2 Hs".
    iPureIntro. eapply (prim_steps_app (m,m') (m2,m2')); eauto.
    rewrite -!fill_app. eauto.
  Qed.

  Lemma logrel_of_val {S} αv (v : val S) V :
    V αv v -∗ logrel_expr V (IT_of_V αv) (Val v).
  Proof.
    iIntros "H1". iIntros (σ K) "Hs".
    iApply wp_val.
    { by rewrite IT_to_of_V. }
    iExists (0,0),v,σ. iFrame. iPureIntro.
    by econstructor.
  Qed.

  Lemma logrel_step_pure {S} (e' e : expr S) α V :
    (∀ σ, prim_step e σ e' σ (0,0)) →
    logrel_expr V α e' ⊢ logrel_expr V α e.
  Proof.
    intros Hpure.
    iIntros "H".
    iIntros (σ K) "Hs".
    iSpecialize ("H" $! _ K with "Hs").
    iApply (wp_wand with "H").
    iIntros (βv). iDestruct 1 as ([m m'] v σ' Hsteps) "[H2 Hs]".
    iExists (m,m'),v,σ'. iFrame "H2 Hs".
    iPureIntro.
    eapply (prim_steps_app (0,0) (m,m')); eauto.
    { eapply prim_step_steps.
      eapply prim_step_ctx; done. }
  Qed.

  (* Evaluation contexts *)
  Definition AppLSCtx (β α : IT) := APP' α β.
  Definition AppRSCtx (β α : IT) := APP' β α.
  Definition NatOpLSCtx (op : nat_op) (β α : IT) := NATOP (do_natop op) α β.
  Definition NatOpRSCtx (op : nat_op) (β α : IT) := NATOP (do_natop op) β α.
  Definition IFSCtx (β1 β2 α : IT) := IF α β1 β2.

  #[export] Instance AppLSCtx_hom (β : IT) : AsVal β → IT_hom (AppLSCtx β) | 0.
  Proof.
    intros Hb. unfold AppLSCtx.
    simple refine (IT_HOM _ _ _ _ _); simpl.
    - solve_proper.
    - intros a. rewrite !APP_APP'_ITV.
      by rewrite APP_Tick.
    - intros op i k. rewrite !APP_APP'_ITV.
      rewrite APP_Vis. repeat f_equiv.
      intro x ; simpl. by rewrite APP_APP'_ITV.
    - by rewrite !APP_APP'_ITV APP_Err.
  Qed.
  #[export] Instance AppRSCtx_hom (β : IT) : IT_hom (AppRSCtx β) | 0.
  Proof.
    unfold AppRSCtx.
    simple refine (IT_HOM _ _ _ _ _); simpl.
    - solve_proper.
    - intros a. by rewrite APP'_Tick_r.
    - intros op i k. rewrite APP'_Vis_r. repeat f_equiv.
    - by rewrite APP'_Err.
  Qed.
  #[local] Instance NatOpLSCtx_ne op (β : IT) : NonExpansive (NatOpLSCtx op β).
  Proof.
    intro n. unfold NatOpLSCtx.
    repeat intro. repeat f_equiv; eauto.
  Qed.
  #[export] Instance NatOpLSCtx_hom op (β : IT) : AsVal β → IT_hom (NatOpLSCtx op β).
  Proof.
    intros Hb. unfold NatOpLSCtx.
    simple refine (IT_HOM _ _ _ _ _).
    - intro a. simpl. rewrite NATOP_ITV_Tick_l//.
    - intros op' i k. simpl. rewrite NATOP_ITV_Vis_l//.
      repeat f_equiv. intro. reflexivity.
    - simpl. rewrite NATOP_Err_l//.
  Qed.
  #[local] Instance NatOpRSCtx_ne op (β : IT) : NonExpansive (NatOpRSCtx op β).
  Proof.
    intro n. unfold NatOpRSCtx.
    repeat intro. by apply (NATOP (do_natop op) β).
    (* why doesn't f_equiv work here? *)
  Qed.
  #[export] Instance NatOpRSCtx_hom op (β : IT) : IT_hom (NatOpRSCtx op β).
  Proof.
    unfold NatOpRSCtx.
    simple refine (IT_HOM _ _ _ _ _).
    - intro a. simpl. rewrite NATOP_Tick_r//.
    - intros op' i k. simpl. rewrite NATOP_Vis_r//.
      repeat f_equiv. intro. reflexivity.
    - simpl. rewrite NATOP_Err_r//.
  Qed.
  #[local] Instance IFSCtx_ne (β1 β2 : IT) : NonExpansive (IFSCtx β1 β2).
  Proof. unfold IFSCtx. solve_proper. Qed.
  #[export] Instance IFSCtx_hom (β1 β2 : IT) : IT_hom (IFSCtx β1 β2).
  Proof.
    unfold IFSCtx.
    simple refine (IT_HOM _ _ _ _ _).
    - intro a. simpl. rewrite IF_Tick//.
    - intros op i k. simpl. rewrite IF_Vis.
      repeat f_equiv. intro α. reflexivity.
    - simpl. rewrite IF_Err//.
  Qed.

  (* a matching list of closing substitutions *)
  Inductive subs2 : scope → Type :=
  | emp_subs2 : subs2 []
  | cons_subs2 {S} : val [] → ITV → subs2 S → subs2 (()::S)
  .

  Equations subs_of_subs2 {S} (ss : subs2 S) : subs S [] :=
    subs_of_subs2 emp_subs2 v => idsub v;
    subs_of_subs2 (cons_subs2 t α ss) Vz := Val t;
    subs_of_subs2 (cons_subs2 t α ss) (Vs v) := subs_of_subs2 ss v.

  Equations its_of_subs2 {S} (ss : subs2 S) : interp_scope S :=
    its_of_subs2 emp_subs2 := ();
    its_of_subs2 (cons_subs2 t α ss) := (IT_of_V α, its_of_subs2 ss).

  Equations list_of_subs2 {S} (ss : subs2 S) : list (val []*ITV) :=
    list_of_subs2 emp_subs2 := [];
    list_of_subs2 (cons_subs2 v α ss) := (v,α)::(list_of_subs2 ss).

  Lemma subs_of_emp_subs2 : subs_of_subs2 emp_subs2 ≡ idsub.
  Proof. intros v. dependent elimination v. Qed.

  Definition subs2_valid {S} (Γ : tyctx S) (ss : subs2 S) : iProp :=
    ([∗ list] τx ∈ zip (list_of_tyctx Γ) (list_of_subs2 ss),
      logrel_val (τx.1) (τx.2.2) (τx.2.1))%I.

  Definition logrel_valid {S} (Γ : tyctx S) (e : expr S) (α : interp_scope S -n> IT) (τ : ty) : iProp :=
    (∀ ss, subs2_valid Γ ss → logrel τ
                                  (α (its_of_subs2 ss))
                                  (subst_expr e (subs_of_subs2 ss)))%I.

  Lemma compat_var {S} (Γ : tyctx S) (x : var S) τ :
    typed_var Γ x τ → ⊢ logrel_valid Γ (Var x) (interp_var x) τ.
  Proof.
    intros Hx. iIntros (ss) "Hss".
    simp subst_expr.
    iInduction Hx as [|Hx] "IH".
    - dependent elimination ss. simp subs_of_subs2.
      simp interp_var. rewrite /subs2_valid.
      simp list_of_tyctx list_of_subs2 its_of_subs2. simpl.
      iDestruct "Hss" as "[Hv Hss]".
      iApply (logrel_of_val with "Hv").
    - dependent elimination ss. simp subs_of_subs2.
      simp interp_var. rewrite /subs2_valid.
      simp list_of_tyctx list_of_subs2 its_of_subs2. simpl.
      iDestruct "Hss" as "[Hv Hss]". by iApply "IH".
  Qed.

  Lemma compat_if {S} (Γ : tyctx S) (e0 e1 e2 : expr S) α0 α1 α2 τ :
    ⊢ logrel_valid Γ e0 α0  Tnat -∗
      logrel_valid Γ e1 α1 τ -∗
      logrel_valid Γ e2 α2 τ -∗
      logrel_valid Γ (If e0 e1 e2) (interp_if α0 α1 α2) τ.
  Proof.
    iIntros "H0 H1 H2". iIntros (ss) "#Hss".
    simpl. simp subst_expr.
    pose (s := (subs_of_subs2 ss)). fold s.
    iSpecialize ("H0" with "Hss").
    iApply (logrel_bind (IFSCtx (α1 (its_of_subs2 ss)) (α2 (its_of_subs2 ss)))
              [IfCtx (subst_expr e1 s) (subst_expr e2 s)]
                        with "H0").
    iIntros (v βv). iDestruct 1 as (n) "[Hb ->]".
    iRewrite "Hb". simpl.
    unfold IFSCtx.
    destruct (decide (0 < n)).
    - rewrite IF_True//.
      iSpecialize ("H1" with "Hss").
      iApply (logrel_step_pure with "H1").
      intros ?. apply (Ectx_step' []).
      econstructor; eauto.
    - rewrite IF_False; last lia.
      iSpecialize ("H2" with "Hss").
      iApply (logrel_step_pure with "H2").
      intros ?. apply (Ectx_step' []).
      econstructor; eauto. lia.
  Qed.

  Lemma compat_recV {S} Γ (e : expr (()::()::S)) τ1 τ2 α :
    ⊢ □ logrel_valid (consC (Tarr τ1 τ2) (consC τ1 Γ)) e α τ2 -∗
      logrel_valid Γ (Val $ RecV e) (interp_rec α) (Tarr τ1 τ2).
  Proof.
    iIntros "#H". iIntros (ss) "#Hss".
    pose (s := (subs_of_subs2 ss)). fold s.
    pose (env := (its_of_subs2 ss)). fold env.
    simp subst_expr.
    pose (f := (ir_unf α env)).
    iAssert (interp_rec α env ≡ IT_of_V $ FunV (Next f))%I as "Hf".
    { iPureIntro. apply interp_rec_unfold. }
    iRewrite "Hf".
    iApply logrel_of_val. iLöb as "IH". iSimpl.
    iExists (Next f). iSplit; eauto.
    iIntros (βv w) "#Hw".
    iAssert ((APP' (Fun $ Next f) (IT_of_V βv)) ≡ (Tick (ir_unf α env (IT_of_V βv))))%I
      as "Htick".
    { iPureIntro. rewrite APP_APP'_ITV.
      rewrite APP_Fun. simpl. done. }
    iRewrite "Htick". iClear "Htick".
    iModIntro. iIntros (σ K) "Hs".
    iApply wp_tick. iNext. simpl.
    pose (ss' := cons_subs2 (RecV (subst_expr e (subs_lift (subs_lift s)))) (FunV (Next (ir_unf α env))) (cons_subs2 w βv  ss)).
    iSpecialize ("H" $! ss' with "[Hss]").
    { rewrite {2}/subs2_valid /ss'. simp list_of_tyctx list_of_subs2.
      cbn-[logrel_val]. iFrame "Hss Hw". fold f. iRewrite -"Hf".
      by iApply "IH". }
    iSpecialize ("H" $! _ K with "Hs").
    iClear "IH Hss Hw".
    unfold ss'. simpl. simp its_of_subs2. fold f env.
    iRewrite "Hf". simpl.
    iApply (wp_wand with "H").
    iIntros (v).
    iDestruct 1 as ([m m'] v0  σ0 Hsteps) "[Hv Hs]".
    iExists (1+m,1+m'),v0,σ0. iFrame "Hv Hs".
    iPureIntro. econstructor; eauto.
    apply (Ectx_step' K).
    admit.
  Admitted.

  Lemma compat_rec {S} Γ (e : expr (()::()::S)) τ1 τ2 α :
    ⊢ □ logrel_valid (consC (Tarr τ1 τ2) (consC τ1 Γ)) e α τ2 -∗
      logrel_valid Γ (Rec e) (interp_rec α) (Tarr τ1 τ2).
  Proof.
    iIntros "#H". iIntros (ss) "#Hss".
    pose (s := (subs_of_subs2 ss)). fold s.
    pose (env := (its_of_subs2 ss)). fold env.
    simp subst_expr.
    iApply (logrel_step_pure (Val (RecV (subst_expr e (subs_lift (subs_lift s)))))).
    { intros ?. eapply (Ectx_step' []). econstructor. }
    iPoseProof (compat_recV with "H") as "H2".
    iSpecialize ("H2" with "Hss").
    simp subst_expr. iApply "H2".
  Qed.

  Lemma compat_app {S} Γ (e1 e2 : expr S) τ1 τ2 α1 α2 :
  ⊢ logrel_valid Γ e1 α1 (Tarr τ1 τ2) -∗
    logrel_valid Γ e2 α2 τ1 -∗
    logrel_valid Γ (App e1 e2) (interp_app α1 α2) τ2.
  Proof.
    iIntros "H1 H2".  iIntros (ss) "#Hss".
    iSpecialize ("H1" with "Hss").
    iSpecialize ("H2" with "Hss").
    pose (s := (subs_of_subs2 ss)). fold s.
    pose (env := its_of_subs2 ss). fold env.
    simp subst_expr. simpl.
    assert (IT_hom (AppRSCtx (α1 env))).
    { (* Typeclasses Opaque AppLSCtx AppRSCtx NatOpLSCtx NatOpRSCtx IFSCtx. *)
      (* apply _. *)
      apply AppRSCtx_hom.
      (** XXX! What is wrong here? *)
    }
    iApply (logrel_bind (AppRSCtx (α1 env)) [AppRCtx (subst_expr e1 s)] with "H2").
    iIntros (v2 β2) "H2". iSimpl.
    assert (IT_hom (AppLSCtx (IT_of_V β2))).
    { apply AppLSCtx_hom. apply _. (** XXX same shit here *) }
    iApply (logrel_bind (AppLSCtx (IT_of_V β2)) [AppLCtx v2] with "H1").
    iIntros (v1 β1) "H1". simpl.
    iDestruct "H1" as (f) "[Hα H1]".
    simpl.
    unfold AppLSCtx. iRewrite "Hα". (** XXX why doesn't simpl work here? *)
    iApply ("H1" with "H2").
  Qed.

  Lemma compat_input {S} Γ :
    ⊢ logrel_valid Γ (Input : expr S) interp_input Tnat.
  Proof.
    iIntros (ss) "Hss".
    iIntros (σ K) "Hs".
    destruct (update_input σ) as [n σ'] eqn:Hinp.
    iApply (wp_reify with "Hs []").
    { eapply reify_vis_eq.
      simpl. rewrite Hinp. done. }
    iIntros "Hs". iNext. simpl.
    iApply wp_val.
    { rewrite IT_to_V_Nat//. }
    iExists (1,1),(Lit n),σ'.
    iFrame "Hs". iSplit.
    { iPureIntro.
      simp subst_expr.
      apply prim_step_steps.
      apply (Ectx_step' K).
      by constructor. }
    iExists n. eauto.
  Qed.

  Lemma compat_natop {S} (Γ : tyctx S) e1 e2 α1 α2 op :
    ⊢ logrel_valid Γ e1 α1 Tnat -∗
      logrel_valid Γ e2 α2 Tnat -∗
      logrel_valid Γ (NatOp op e1 e2) (interp_natop op α1 α2) Tnat.
  Proof.
    iIntros "H1 H2".  iIntros (ss) "#Hss".
    iSpecialize ("H1" with "Hss").
    iSpecialize ("H2" with "Hss").
    pose (s := (subs_of_subs2 ss)). fold s.
    pose (env := its_of_subs2 ss). fold env.
    simp subst_expr. simpl.
    assert (IT_hom (NatOpRSCtx op (α1 env))).
    {  apply NatOpRSCtx_hom. (** FIXME *) }
    iApply (logrel_bind (NatOpRSCtx op (α1 env)) [NatOpRCtx op (subst_expr e1 s)] with "H2").
    iIntros (v2 β2) "H2". iSimpl.
    assert (IT_hom $ NatOpLSCtx op (IT_of_V β2)).
    { apply NatOpLSCtx_hom. apply _. }
    iApply (logrel_bind (NatOpLSCtx op (IT_of_V β2)) [NatOpLCtx op v2] with "H1").
    iIntros (v1 β1) "H1". simpl.
    iDestruct "H1" as (n1) "[Hn1 ->]".
    iDestruct "H2" as (n2) "[Hn2 ->]".
    unfold NatOpLSCtx.
    iAssert ((NATOP (do_natop op) (IT_of_V β1) (IT_of_V β2)) ≡ Nat (do_natop op n1 n2))%I with "[Hn1 Hn2]" as "Hr".
    { iRewrite "Hn1". simpl.
      iRewrite "Hn2". simpl.
      iPureIntro. apply NATOP_Nat. }
    iApply (logrel_step_pure (Val (Lit (do_natop op n1 n2)))).
    { intro. apply (Ectx_step' []). constructor.
      destruct op; simpl; eauto. }
    iRewrite "Hr".
    iApply (logrel_of_val (NatV $ do_natop op n1 n2)).
    iExists _. iSplit; eauto.
  Qed.

  Lemma fundamental {S} (Γ : tyctx S) τ e :
    typed Γ e τ → ⊢ logrel_valid Γ e (interp_expr e) τ
  with fundamental_val {S} (Γ : tyctx S) τ v :
    typed_val Γ v τ → ⊢ logrel_valid Γ (Val v) (interp_val v) τ.
  Proof.
    - induction 1; simpl.
      + by apply fundamental_val.
      + by apply compat_var.
      + iApply compat_rec. iApply IHtyped.
      + iApply compat_app.
        ++ iApply IHtyped1.
        ++ iApply IHtyped2.
      + iApply compat_natop.
        ++ iApply IHtyped1.
        ++ iApply IHtyped2.
      + iApply compat_if.
        ++ iApply IHtyped1.
        ++ iApply IHtyped2.
        ++ iApply IHtyped3.
      + iApply compat_input.
    - induction 1; simpl.
      + iIntros (ss) "Hss". simp subst_expr. simpl.
        iApply (logrel_of_val (NatV n)). iExists n. eauto.
      + iApply compat_recV. by iApply fundamental.
  Qed.


End reify_io.
Definition κ {S} : ITV ioΣ → val S :=  λ x,
    match x with
    | NatV n => Lit n
    | _ => Lit 0
    end.
Lemma logrel_nat_adequacy {S} (α : IT ioΣ) (e : expr S) n σ σ' k :
  (∀ {Σ:gFunctors}`{H1 : !invGS_gen hlc Σ} `{H2: !stateG rs Σ},
      (True ⊢ logrel Tnat α e)%I) →
  ssteps rs α (σ,()) (Nat n) σ' k → ∃ m σ', prim_steps e σ (Val $ Lit n) σ' m.
Proof.
  intros Hlog Hst.
  pose (ϕ := λ (βv : ITV ioΣ),
          ∃ m σ', prim_steps e σ (Val $ κ βv) σ' m).
  cut (ϕ (NatV n)).
  { destruct 1 as ( m' & σ2 & Hm). simpl in Hm.
    eexists; eauto. }
  eapply (wp_adequacy).
  { simpl; eassumption. }
  intros hlc Σ Hinv1 Hst1.
  pose (Φ := (λ (βv : ITV ioΣ), ∃ n, logrel_val Tnat (Σ:=Σ) (S:=S) βv (Lit n)
          ∗ ⌜∃ m σ', prim_steps e σ (Val $ Lit n) σ' m⌝)%I).
  assert (NonExpansive Φ).
  { unfold Φ.
    intros l a1 a2 Ha. repeat f_equiv. done. }
  exists Φ. split; first assumption. split.
  { iIntros (βv). iDestruct 1 as (n'') "[H %]".
    iDestruct "H" as (n') "[#H %]". simplify_eq/=.
    iAssert (IT_of_V βv ≡ Nat n')%I as "#Hb".
    { change (Nat n') with (IT_of_V (E:=ioΣ) (NatV n')).
      by iApply f_equivI. }
    iAssert (⌜βv = NatV n'⌝)%I with "[-]" as %Hfoo.
    { destruct βv as [r|f]; simpl.
      - iPoseProof (Nat_inj' with "Hb") as "%Hr".
        fold_leibniz. eauto.
      - iExFalso. iApply (IT_nat_fun_ne).
        iApply internal_eq_sym. iExact "Hb". }
    iPureIntro. rewrite Hfoo. unfold ϕ.
    eauto. }
  iIntros "Hs".
  iPoseProof (Hlog with "[//]") as "Hlog".
  iSpecialize ("Hlog" $! _ [] with "Hs").
  iApply (wp_wand with"Hlog").
  iIntros ( βv). iIntros "H".
  iDestruct "H" as (m' v σ1' Hsts) "[Hi Hsts]".
  unfold Φ. iDestruct "Hi" as (l) "[Hβ %]". simplify_eq/=.
  iExists l. iSplit; eauto.
  iExists l. iSplit; eauto.
Qed.


Theorem adequacy (e : expr []) (k : nat) σ σ' n :
  typed empC e Tnat →
  ssteps rs (interp_expr e ()) (σ,()) (Nat k) σ' n →
  ∃ σ' mm, prim_steps e σ (Val $ Lit k) σ' mm.
Proof.
  pose (Σ := #[stateΣ rs; invΣ; wsatΣ]).
  intros Hty Hst. Abort.
(*   assert (invGS Σ). *)
(*   { apply InvG; try apply _. Search invGS_gen. *)
(*   apply (fundamental (Σ:=Σ)) in Hty. *)
(*   eapply logrel_adequate; eauto. *)
(*   iPoseProof (Hty $! emp_subs2 σ) as "H". *)
(*   simp its_of_subs2. *)
(*   assert (subst_expr e (subs_of_subs2 emp_subs2) = e) as ->. *)
(*   { rewrite subs_of_emp_subs2. apply subst_expr_idsub. } *)
(*   iApply "H". *)
(*   unfold subs2_valid. simp list_of_tyctx list_of_subs2. simpl. done. *)
(* Qed. *)
