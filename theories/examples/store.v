From Equations Require Import Equations.
From iris.proofmode Require Import classes tactics.
From iris.algebra Require Import gmap.
From iris.heap_lang Require Export locations.
From gitree Require Import prelude.
From gitree.ugitree Require Import core reductions weakestpre.

Opaque laterO_map.
(* why do i need to repeat this?*)
Opaque Nat Fun Tau Err Vis.

(** State and operations *)
Canonical Structure locO := leibnizO loc.
Definition stateF : oFunctor := (gmapOF locO (▶ ∙))%OF.
Notation "F ♯ E" := (oFunctor_apply F E) (at level 20, right associativity).

#[local] Instance state_inhabited : Inhabited (stateF ♯ unitO).
Proof. apply _. Qed.
#[local] Instance state_cofe X `{!Cofe X} : Cofe (stateF ♯ X).
Proof. apply _. Qed.

Definition state_read X `{!Cofe X} : loc * (stateF ♯ X) → option (laterO X * (stateF ♯ X))
  := λ '(l,σ), x ← σ !! l;
               Some (x, σ).
#[export] Instance state_read_ne X `{!Cofe X} :
  NonExpansive (state_read X : prodO locO (stateF ♯ X) → optionO (prodO (laterO X) (stateF ♯ X))).
Proof.
  intros n [l1 s1] [l2 s2]. simpl. intros [-> Hs].
  apply (option_mbind_ne _ (λ n, Some (n, s1)) (λ n, Some (n, s2)));
    solve_proper.
Qed.

Definition state_write X `{!Cofe X} :
  (loc * (laterO X)) * (stateF ♯ X) → option (unit * (stateF ♯ X))
  := λ '((l,n),s), let s' := <[l:=n]>s
                   in Some ((), s').
#[export] Instance state_write_ne X `{!Cofe X} :
  NonExpansive (state_write X : prodO (prodO locO _) (stateF ♯ _) → optionO (prodO unitO (stateF ♯ X))).
Proof.
  intros n [[l1 m1] s1] [[l2 m2] s2]. simpl.
  intros [[Hl%leibnizO_leibniz Hm] Hs]. simpl in Hl.
  rewrite Hl. solve_proper.
Qed.

Definition state_alloc X `{!Cofe X} : (laterO X) * (stateF ♯ X) → option (loc * (stateF ♯ X))
  := λ '(n,s), let l := Loc.fresh (dom s) in
               let s' := <[l:=n]>s in
               Some (l, s').
#[export] Instance state_alloc_ne X `{!Cofe X} :
  NonExpansive (state_alloc X : prodO _ (stateF ♯ X) → optionO (prodO locO (stateF ♯ X))).
Proof.
  intros n [m1 s1] [m2 s2]. simpl.
  intros [Hm Hs]. simpl in *.
  set (l1 := Loc.fresh (dom s1)).
  set (l2 := Loc.fresh (dom s2)).
  assert (l1 = l2) as ->.
  { unfold l1,l2. f_equiv. eapply gmap_dom_ne, Hs. }
  solve_proper.
Qed.

Program Definition ReadE : opInterp :=  {|
  Ins := locO;
  Outs := (▶ ∙);
|}.
Program Definition WriteE : opInterp :=  {|
  Ins := (locO * (▶ ∙))%OF;
  Outs := unitO;
|}.
Program Definition AllocE : opInterp :=  {|
  Ins := (▶ ∙);
  Outs := locO;
|}.

Definition storeE : opsInterp := @[ReadE;WriteE;AllocE].
Definition reify_store : reify_eff storeE stateF.
Proof.
  intros op X HX.
  destruct op as [[]|[[]|[[]|[]]]]; simpl.
  - simple refine (OfeMor (state_read X)).
  - simple refine (OfeMor (state_write X)).
  - simple refine (OfeMor (state_alloc X)).
Defined.

Section constructors.
  Context {E : opsInterp}.
  Context `{!subEff storeE E}.
  Notation IT := (IT E).
  Notation ITV := (ITV E).

  Program Definition READ : locO -n> (laterO IT -n> laterO IT) -n> IT :=
    λne l k, Vis (E:=E) (subEff_opid $ inl ())
                  (subEff_conv_ins (F:=storeE) (op:=(inl ())) l)
                  (k ◎ subEff_conv_outs2 (F:=storeE) (op:=(inl ()))).
  Solve Obligations with solve_proper.

  (* Program Definition WRITE : locO -n> laterO IT -n> (laterO IT) -n> IT *)
  (*   := λne l n k, Vis (E:=storeE) (inl ())) (l,n) (λne _, k). *)
  (* Next Obligation. solve_proper. Qed. *)
  (* Next Obligation. *)
  (*   simpl. repeat intro. *)
  (*   repeat f_equiv. intro; done. *)
  (* Qed. *)
  (* Next Obligation. solve_proper. Qed. *)
  (* Next Obligation. solve_proper. Qed. *)

  (* Program Definition ALLOC : laterO IT -n> (locO -n> laterO IT) -n> IT *)
  (*   := λne n k, Vis (E:=storeE) 2%fin n k. *)
  (* Solve Obligations with solve_proper. *)



  (* Notation reify := (reify reify_io). *)
  (* Notation sstep := (sstep reify_io). *)
  (* Notation ssteps := (ssteps reify_io). *)
End constructors.

Section wp.
  Context {E : opsInterp}.
  Variable (rs : reifiers E).
  Notation IT := (IT E).
  Notation ITV := (ITV E).
  Notation stateO := (stateF ♯ IT).

  Context `{!subReifier reify_store rs rest}.
  Context `{!invGS_gen hlc Σ, !stateG rs Σ}.
  Notation iProp := (iProp Σ).
  (* Notation istep := (istep reify_io). *)
  (* Notation isteps := (isteps reify_io). *)

  Notation "'WP' α {{ β , Φ } }" := (wp rs α (λ β, Φ))
    (at level 20, α, Φ at level 200,
     format "'WP'  α  {{  β ,  Φ  } }") : bi_scope.

  Notation "'WP' α {{ Φ } }" := (wp rs α Φ)
    (at level 20, α, Φ at level 200,
     format "'WP'  α  {{  Φ  } }") : bi_scope.

  Lemma wp_read (σ : stateO) σr (l : loc) (α : IT) Φ :
    σ !! l ≡ Some (Next α) →
    has_state (subState_conv_state σr σ) -∗
    (has_state (subState_conv_state σr σ) -∗ ▷ WP α {{ Φ }}) -∗
    WP (READ l idfun) {{ Φ }}.
  Proof.
    intros Hs. iIntros "Hs Ha".
    unfold READ. simpl.
    iApply (wp_subreify with "Hs Ha").
    { simpl. trans (Some (Next α) ≫= (λ x : laterO IT, Some (x, σ))).
      - apply option_bind_proper; solve_proper.
      - simpl. reflexivity. }
    { apply ofe_iso_21. }
  Qed.

End wp.
