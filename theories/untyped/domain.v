(* Interpretation of PCF in the untyped model *)
From Equations Require Import Equations.
From iris.algebra Require Import gmap.
From iris.algebra Require cofe_solver.
From iris.base_logic Require Export base_logic.
From iris.prelude Require Import options.
From iris.heap_lang Require Import lang primitive_laws.
From gitree Require Import prelude pcf untyped.scott.

Program Definition interp_app {A} `{!Cofe A}:
  (A -n> IT) -n> (A -n> IT) -n> (A -n> IT)
  := λne f1 f2 a, APP (f1 a) (f2 a).
Next Obligation. repeat intro; repeat f_equiv; eauto. Qed.
Next Obligation.
  repeat intro; simpl; repeat f_equiv; eauto.
  repeat intro; simpl; repeat f_equiv; eauto.
Qed.
Next Obligation.
  repeat intro; simpl; repeat f_equiv; eauto.
Qed.

Program Definition interp_lam {A} `{!Cofe A} :
  (prodO IT A -n> IT) -n> (A -n> IT)
  := λne t1 ta,
                  IT_arr $ Next (λne x, t1 (x, ta)).
Solve Obligations with solve_proper_ish ; solve_proper.

Program Definition interp_Y_pre :=
  λne self t, let self' := laterO_ap self (NextO t) in
         get_fun (λne f, IT_thunk (later_ap f self')) t.
Next Obligation.
  intros self t self' n f1 f2 Hf.
  by repeat f_equiv.
Qed.
Next Obligation.
  intros t n s1 s2 Hs.
  cbn-[IT_thunk]. do 2 (f_equiv; eauto).
  intro f; simpl. solve_proper.
Qed.
Next Obligation.
  intros n self1 self2 Hs t.
  cbn-[later_ap]. do 2 f_equiv.
  intro f. cbn-[later_ap].
  by repeat f_equiv.
Qed.

Program Definition interp_Y_ish : IT -n> IT :=
  mmuu interp_Y_pre.

Program Definition interp_Y {A} `{!Cofe A}:
  (A -n> IT) -n> (A -n> IT)
  := λne f a, interp_Y_ish (f a).
Solve Obligations with repeat solve_proper_ish.

Program Definition interp_succ {A} `{!Cofe A}:
  (A -n> IT) -n> (A -n> IT)
  := λne f a, SUCC (f a).
Solve Obligations with repeat solve_proper_ish.

Program Definition interp_pred {A} `{!Cofe A}:
  (A -n> IT) -n> (A -n> IT)
  := λne f a, PRED (f a).
Solve Obligations with repeat solve_proper_ish.


Fixpoint interp_ctx (Γ : ctx) : ofe :=
  match Γ with
  | [] => unitO
  | τ::Γ => prodO IT (interp_ctx Γ)
  end.

#[export] Instance interp_ctx_cofe Γ : Cofe (interp_ctx Γ).
Proof.
  induction Γ; simpl; apply _.
Qed.

#[export] Instance interp_ctx_inhab Γ : Inhabited (interp_ctx Γ).
Proof.
  induction Γ; simpl; apply _.
Defined.

Equations interp_var {Γ : ctx} {τ : ty} (v : var Γ τ) : interp_ctx Γ → IT :=
interp_var (Γ:=(τ::_))     Vz D := fst D;
interp_var (Γ:=(_::Γ)) (Vs v) D := interp_var v (snd D).

#[export]  Instance interp_var_ne Γ (τ : ty) (v : var Γ τ) : NonExpansive (@interp_var Γ _ v).
Proof.
  intros n D1 D2 HD12. induction v; simp interp_var.
  - by f_equiv.
  - eapply IHv. by f_equiv.
Qed.
Global Instance interp_var_proper Γ (τ : ty) (v : var Γ τ) : Proper ((≡) ==> (≡)) (interp_var v).
Proof. apply ne_proper. apply _. Qed.

Program Definition interp_bot {Γ} : Γ -n> IT := λne _, IT_err.

Equations interp_tm {Γ : ctx} {τ : ty} (M : tm Γ τ) : interp_ctx Γ -n> IT :=
interp_tm (Num n) := λne _, IT_nat n;
interp_tm (Var v) := OfeMor (interp_var v);
interp_tm (App f a) := interp_app (interp_tm f) (interp_tm a);
interp_tm (Lam N) := interp_lam (interp_tm N);
interp_tm (Succ M) := interp_succ (interp_tm M);
interp_tm (Pred M) := interp_pred (interp_tm M);
interp_tm (Y M) := interp_Y (interp_tm M).

Equations interp_rens_ctx {Γ D : ctx}
          (E : interp_ctx D) (s : rens Γ D) : interp_ctx Γ :=
  interp_rens_ctx (Γ:=[]) E s := tt : interp_ctx [];
  interp_rens_ctx (Γ:=_::_) E s :=
    (interp_var (hd_ren s) E, interp_rens_ctx E (tl_ren s)).

Equations interp_subs_ctx {Γ D : ctx}
          (E : interp_ctx D) (s : subs Γ D) : interp_ctx Γ :=
  interp_subs_ctx (Γ:=[]) E s := tt : interp_ctx [];
  interp_subs_ctx (Γ:=_::_) E s :=
    (interp_tm (hd_sub s) E, interp_subs_ctx E (tl_sub s)).


Global Instance interp_rens_ctx_proper Γ D :
  Proper ((≡) ==> (≡) ==> (≡)) (@interp_rens_ctx Γ D).
Proof.
  intros E E' HE s1 s2 Hs.
  induction Γ as [|τ' Γ]; simp interp_rens_ctx; auto.
  f_equiv.
  - unfold hd_ren; rewrite Hs.
    by rewrite HE.
  - apply IHΓ. intros ? v. unfold tl_ren; by rewrite Hs.
Qed.
Global Instance interp_subs_ctx_proper Γ D :
  Proper ((≡) ==> (≡) ==> (≡)) (@interp_subs_ctx Γ D).
Proof.
  intros E E' HE s1 s2 Hs.
  induction Γ as [|τ' Γ]; simp interp_subs_ctx; auto.
  f_equiv.
  - unfold hd_sub; by rewrite Hs HE.
  - apply IHΓ. intros ? v. unfold tl_sub; by rewrite Hs.
Qed.



Lemma interp_rens_ctx_tl_ren {Γ D τ} x E (r : rens Γ D) :
  interp_rens_ctx ((x, E) : interp_ctx (τ::D)) (tl_ren (rens_lift r))
                  ≡ interp_rens_ctx E r.
Proof.
  induction Γ as [|τ' Γ]; simp interp_rens_ctx; eauto.
  f_equiv.
  { unfold hd_ren, tl_ren. simp rens_lift interp_var.
    done. }
  { rewrite -IHΓ. f_equiv. clear.
    intros τ2 v. dependent elimination v;
                   unfold hd_ren, tl_ren; simp rens_lift; auto. }
Qed.


Lemma interp_tm_ren {Γ D : ctx} {τ} (M : tm Γ τ) (r : rens Γ D) :
  ∀ (E : interp_ctx D),
    interp_tm (ren_tm M r) E ≡ interp_tm M (interp_rens_ctx E r).
Proof.
  revert D r. induction M=> D r E; simp ren_tm interp_tm ; first done.
  all: try by (simp interp_tm; repeat intro; simpl; f_equiv; eauto).
  - simpl. revert r.
    induction v=>r.
    + simp interp_var interp_rens_ctx. done.
    + simp interp_var interp_rens_ctx. simpl.
      apply (IHv (tl_ren r)).
  - simp interp_tm.
    cbn-[IT_thunk  IT_arr mmuu get_nat].
    f_equiv. f_equiv.
    intros x. simpl.
    rewrite IHM. f_equiv.
    clear.
    destruct Γ as [|τ' Γ]; first done.
    rewrite interp_rens_ctx_equation_2.
    simp interp_var. f_equiv.
    by rewrite interp_rens_ctx_tl_ren.
  - simp interp_tm.
    cbn-[IT_thunk IT_arr mmuu get_nat later_ap].
    f_equiv; eauto.
    f_equiv; eauto.
    intros g; simpl. repeat f_equiv. eauto.
Qed.

Lemma interp_rens_ctx_id {Γ} (E : interp_ctx Γ) :
  interp_rens_ctx E (@idren Γ) ≡ E.
Proof.
  induction Γ as [|τ' Γ]; simp interp_rens_ctx.
  { by destruct E. }
  destruct E as [x E]. simp interp_var. simpl.
  f_equiv.
  trans (interp_rens_ctx ((x, E) : interp_ctx (τ'::Γ)) (tl_ren (rens_lift idren))).
  { f_equiv. intros ? v. unfold tl_ren.
    reflexivity. }
  rewrite interp_rens_ctx_tl_ren.
  apply IHΓ.
Qed.

Lemma interp_subs_ctx_tl_sub {Γ D τ} x E (s : subs Γ D) :
  interp_subs_ctx ((x, E) : interp_ctx (τ::D)) (tl_sub (subs_lift s))
                  ≡ interp_subs_ctx E s.
Proof.
  induction Γ as [|τ' Γ]; simp interp_subs_ctx; first done.
  f_equiv.
  { unfold hd_sub, tl_sub. simp subs_lift interp_var.
    unfold tm_lift. rewrite interp_tm_ren. f_equiv.
    trans (interp_rens_ctx ((x, E) : interp_ctx (τ::D)) (tl_ren (rens_lift idren))).
    { f_equiv. intros ? v. unfold tl_ren.
      simp rens_lift idren. done. }
    rewrite interp_rens_ctx_tl_ren.
    apply interp_rens_ctx_id. }
  { rewrite -IHΓ. f_equiv. clear.
    intros τ2 v. dependent elimination v;
                   unfold hd_sub, tl_sub; simp subs_lift; auto. }
Qed.

Lemma interp_tm_subst {Γ D : ctx} {τ} (M : tm Γ τ) (s : subs Γ D) :
  ∀ (E : interp_ctx D),
    interp_tm (subst_tm M s) E ≡ interp_tm M (interp_subs_ctx E s).
Proof.
  revert D s. induction M=>D s E; simp subst_tm interp_tm ; first done.
  all: try by (simp interp_tm; repeat intro; simpl; f_equiv; eauto).
  - simpl. revert s.
    induction v=>r.
    + simp interp_var interp_subs_ctx. done.
    + simp interp_var interp_subs_ctx. simpl.
      apply (IHv (tl_sub r)).
  - simp interp_tm.
    cbn-[IT_thunk IT_arr mmuu get_nat].
    f_equiv. f_equiv.
    intros x. simpl.
    rewrite IHM. f_equiv.
    clear.
    destruct Γ as [|τ' Γ]; first done.
    rewrite interp_subs_ctx_equation_2.
    unfold hd_sub. simp subs_lift.
    simp interp_tm interp_var.
    f_equiv; first done.
    by rewrite interp_subs_ctx_tl_sub.
  - simp interp_tm.
    cbn-[IT_thunk IT_arr mmuu get_nat].
    repeat f_equiv; eauto.
    intros g; simpl.
    repeat f_equiv; eauto.
Qed.

Lemma interp_subs_ctx_id {Γ} (E : interp_ctx Γ) :
  interp_subs_ctx E (@idsub Γ) ≡ E.
Proof.
  induction Γ as [|τ' Γ]; simp interp_subs_ctx.
  { by destruct E. }
  destruct E as [x E].
  unfold hd_sub, idsub. simp interp_tm ; simpl; simp interp_var.
  f_equiv; first eauto.
  trans (interp_subs_ctx ((x, E) : interp_ctx (τ'::Γ)) (tl_sub (subs_lift idsub))).
  { f_equiv. intros ? v. unfold tl_sub.
    reflexivity. }
  rewrite interp_subs_ctx_tl_sub.
  apply IHΓ.
Qed.

Ltac simplify_tm :=
  repeat progress first [ simp interp_tm | unfold subst1
                          | simp subst_tm | simp interp_var ].


(** * Soundness *)
Lemma interp_tm_step_beta {C : ctx} {τ : ty} (M N : tm C τ) k :
  step_beta M k N → interp_tm M ≡ OfeMor (IT_tick_n k) ◎ interp_tm N.
Proof.
  induction 1; intro D.
  - simplify_tm. cbn-[IT_thunk IT_arr mmuu get_nat].
    rewrite get_fun_fun. cbn-[IT_thunk IT_arr mmuu get_nat].
    f_equiv.
    rewrite interp_tm_subst.
    repeat f_equiv.
    simp interp_subs_ctx. f_equiv; eauto.
    rewrite -{1}(interp_subs_ctx_id D). f_equiv.
    intros ? x. reflexivity.
  - simplify_tm.
    rewrite {1}/interp_Y.
    unfold interp_Y_ish.
    cbn-[interp_Y_ish IT_tick get_fun mmuu interp_app].
    etrans.
    { eapply (mmuu_unfold interp_Y_pre). }
    unfold interp_app.
    cbn-[IT_tick mmuu IT_thunk].
    f_equiv; eauto. f_equiv.
    intro f. reflexivity.
  - simplify_tm.
    cbn-[get_nat IT_err IT_arr IT_nat IT_tick].
    rewrite get_nat_nat. done.
  - simplify_tm.
    cbn-[get_nat IT_err IT_arr IT_nat IT_tick].
    rewrite -get_nat_tick_n. f_equiv; eauto.
  - simplify_tm.
    cbn-[get_nat IT_err IT_arr IT_nat IT_tick].
    rewrite get_nat_nat. done.
  - simplify_tm.
    cbn-[get_nat IT_err IT_arr IT_nat IT_tick].
    rewrite -get_nat_tick_n. f_equiv; eauto.
  - simplify_tm.
    cbn-[get_nat get_fun IT_err IT_arr IT_nat IT_tick IT_thunk later_ap].
    rewrite -get_fun_tick_n. f_equiv; eauto.
Qed.

Lemma interp_sound {Γ : ctx} {τ : ty} (M N : tm Γ τ) k :
  step_beta_clos M k N → interp_tm M ≡ OfeMor (IT_tick_n k) ◎ interp_tm N.
Proof.
  induction 1.
  - intros E. done.
  - intros E; cbn.
    rewrite IT_tick_add.
    apply interp_tm_step_beta in H.
    rewrite (H _). cbn. f_equiv. eauto.
Qed.


