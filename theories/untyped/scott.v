(* Untyped/unityped model through a universal domain *)
From Equations Require Import Equations.
From iris.algebra Require Import gmap.
From iris.algebra Require cofe_solver.
From iris.base_logic Require Export base_logic.
From iris.prelude Require Import options.
From iris.si_logic Require Import bi siprop.
From iris.proofmode Require Import tactics.
From gitree Require Import prelude.


Opaque laterO_map.

(* Defining the universal domain.
   In order to define `app', we need
   the domain to have the ▶ -algebra structure.
 *)

Module IT_pre.
Definition ITOF : oFunctor :=
  ( natO
  + unitO
  + ▶ ∙
  + ▶ (∙ -n> ∙) ).


#[export] Instance ITOF_contractive :
  oFunctorContractive (ITOF).
Proof. apply _. Qed.

#[export] Instance ITOF_inhabited :
  Inhabited (oFunctor_apply (ITOF) unitO).
Proof.
  refine (populate _).
  refine (inl (inl (inr (())))).
Defined.

#[export] Instance ITOF_cofe T `{!Cofe T, !Cofe A}:
  Cofe (oFunctor_apply (ITOF) T).
Proof.
  apply _.
Defined.
End IT_pre.

Module Export ITF_solution.
  Import IT_pre.
  Import cofe_solver.
  Definition IT_result :
    solution (ITOF)
   := solver.result (ITOF).

  Definition IT : ofe := (IT_result).
  Global Instance IT_cofe : Cofe (IT) := _.


  Definition IT_unfold :
    IT -n> sumO (sumO (sumO natO unitO)
                      (laterO IT))
                (laterO (IT -n> IT))
    := ofe_iso_2 (IT_result).

  Definition IT_fold :
    sumO (sumO (sumO natO unitO)
               (laterO IT))
         (laterO (IT -n> IT)) -n> IT
    := ofe_iso_1 (IT_result).

  Lemma IT_fold_unfold (T : IT) :
    IT_fold (IT_unfold T) ≡ T.
  Proof. apply ofe_iso_12. Qed.
  Lemma IT_unfold_fold T :
    IT_unfold (IT_fold T) ≡ T.
  Proof. apply ofe_iso_21. Qed.

  Definition IT_nat (n : nat) : IT :=
    IT_fold (OfeMor inl (inl (inl n))).
  Definition IT_err : IT :=
    IT_fold (OfeMor inl (inl (inr ()))).
  Definition IT_thunk : laterO IT -n> IT :=
    IT_fold ◎ OfeMor inl ◎ OfeMor inr.
  Definition IT_arr : laterO (IT -n> IT) -n> IT :=
    IT_fold ◎ OfeMor inr.
End ITF_solution.

Global Instance IT_inhabited : Inhabited IT :=
  populate IT_err.

Section IT_rec.
  Variable (P : ofe).
  Context `{!Cofe P, !Inhabited P}.
  Variable (Perr : P)
           (Pnat : nat → P)
           (Pthunk : laterO IT -n> laterO P -n> P)
           (Parr : laterO (IT -n> IT) -n> laterO (P -n> P) -n> P)
           (Pback : P -n>
                      sumO (sumO (sumO natO unitO)
                                 (laterO P))
                                (laterO (P -n> P))).

  Program Definition IT_rec_pre
          (self12 : prodO (IT -n> P) (P -n> IT))
    : prodO (IT -n> P) (P -n> IT).
  Proof using P Parr Pback Perr Pnat Pthunk.
    set (self1 := fst self12).
    set (self2 := snd self12).
    simple refine (_,_).
    { refine (_ ◎ IT_unfold).
      repeat refine (sumO_rec _ _).
      - simple refine (λne n, Pnat n).
      - simple refine (λne _, Perr).
      - simple refine (λne t, Pthunk t (laterO_map self1 t)).
        solve_proper.
      - simple refine (λne lf, Parr lf (laterO_map _ lf)).
        + simple refine (λne (f : IT -n> IT) p,
                   self1 (f (self2 p))).
          all: solve_proper.
        + repeat intro. repeat f_equiv; eauto. }
    { refine (_ ◎ Pback).
      repeat refine (sumO_rec _ _).
      - refine (OfeMor IT_nat).
      - refine (λne _, IT_err).
      - refine (IT_thunk ◎ laterO_map self2).
      - refine (IT_arr ◎ laterO_map _).
        simple refine (λne (f : P -n> P) t,
                 self2 (f (self1 t))).
        all: solve_proper. }
  Defined.

  (* TODO: look into dist_later_prod *)
  Instance IT_rec_pre_contractive : Contractive IT_rec_pre.
  Proof.
    rewrite /IT_rec_pre.
    intros n [h1 k1] [h2 k2] Hhk.
    repeat (f_contractive
            || f_equiv
            || destruct Hhk as [Hh Hk]); eauto.
    { repeat intro; cbn. f_equiv.
      eapply laterO_map_contractive.
      destruct n; [ by apply dist_later_0 | apply dist_later_S ].
      apply (Hh n). lia. }
    { repeat intro. simpl. f_equiv.
      eapply laterO_map_contractive.
      destruct n; [ by apply dist_later_0 | apply dist_later_S ].
      do 2 intro; simpl. f_equiv; eauto.
      + apply (Hh n). lia.
      + repeat f_equiv; eauto.
        apply (Hh n). lia.
    }
    { do 2 intro; simpl.
      repeat f_equiv; eauto. }
  Qed.

  Definition IT_rec : prodO (IT -n> P) (P -n> IT) :=
    fixpoint IT_rec_pre.

  Definition IT_rec_1 : IT -n> P := fst IT_rec.
  Definition IT_rec_2 : P -n> IT := snd IT_rec.

  Lemma IT_rec_unfold :
    IT_rec ≡ IT_rec_pre IT_rec.
  Proof. apply (fixpoint_unfold IT_rec_pre). Qed.
  Lemma IT_rec_1_unfold t :
    IT_rec_1 t ≡ (IT_rec_pre IT_rec).1 t.
  Proof. apply (fixpoint_unfold IT_rec_pre). Qed.

  Lemma IT_rec_1_nat (n : nat) :
    IT_rec_1 (IT_nat n) ≡ Pnat n.
  Proof.
    rewrite IT_rec_1_unfold /IT_nat.
    rewrite /IT_rec_pre.
    cbn-[sumO_rec].
    (* Here we need to be careful not to simplify sum_rec, which will
    unfold into a `match` and we wouldnt be able to use setoid
    rewriting. *)
    rewrite IT_unfold_fold.
    simpl. reflexivity.
  Qed.

  Lemma IT_rec_1_err :
    IT_rec_1 IT_err ≡ Perr.
  Proof.
    rewrite IT_rec_1_unfold /IT_nat.
    rewrite /IT_rec_pre.
    cbn-[sumO_rec].
    rewrite IT_unfold_fold.
    simpl. reflexivity.
  Qed.

  Program Definition sandwich : (IT -n> IT) -n> P -n> P :=
    λne f, IT_rec_1 ◎ f ◎ IT_rec_2.
  Next Obligation. solve_proper. Defined.

  Lemma IT_rec_1_arr f :
    IT_rec_1 (IT_arr f) ≡ Parr f (laterO_map sandwich f).
  Proof.
    rewrite IT_rec_1_unfold /IT_nat.
    rewrite /IT_rec_pre.
    cbn-[sumO_rec].
    rewrite IT_unfold_fold.
    simpl. f_equiv.
    unfold sandwich. repeat f_equiv.
    do 2 intro. simpl. reflexivity.
  Qed.

  Lemma IT_rec_1_thunk f :
    IT_rec_1 (IT_thunk f) ≡ Pthunk f (laterO_map IT_rec_1 f).
  Proof.
    rewrite IT_rec_1_unfold /IT_nat.
    rewrite /IT_rec_pre.
    cbn-[sumO_rec].
    by rewrite IT_unfold_fold.
  Qed.


End IT_rec.

Global Instance IT_rec_ne {P : ofe} `{!Cofe P, !Inhabited P} n :
  Proper (dist n ==> (pointwise_relation _ (dist n)) ==> (dist n) ==> (dist n) ==> (dist n) ==> (dist n)) (IT_rec P).
Proof.
  intros e1 e2 He nt1 nt2 Hnt th1 th2 Hth a1 a2 Ha b1 b2 Hb.
  unfold IT_rec.
  eapply fixpoint_ne. intros [h k].
  unfold IT_rec_pre. repeat f_equiv; eauto.
  all: repeat intro; simpl; repeat f_equiv; eauto.
Qed.
Global Instance IT_rec_1_ne {P : ofe} `{!Cofe P, !Inhabited P} n :
  Proper (dist n ==> (pointwise_relation _ (dist n)) ==> (dist n) ==> (dist n) ==> (dist n) ==> (dist n)) (IT_rec_1 P).
Proof. unfold IT_rec_1. solve_proper. Qed.
Global Instance IT_rec_proper {P : ofe} `{!Cofe P, !Inhabited P} :
  Proper ((≡) ==> (pointwise_relation _ (≡)) ==> (≡) ==> (≡) ==> (≡) ==> (≡)) (IT_rec P).
Proof.
  intros e1 e2 He nt1 nt2 Hnt th1 th2 Hth a1 a2 Ha b1 b2 Hb.
  unfold IT_rec.
  eapply fixpoint_proper. intros [h k].
  unfold IT_rec_pre. repeat f_equiv; eauto.
  all: repeat intro; simpl; repeat f_equiv; eauto.
Qed.
Global Instance IT_rec_1_proper {P : ofe} `{!Cofe P, !Inhabited P} :
  Proper ((≡) ==> (pointwise_relation _ (≡)) ==> (≡) ==> (≡) ==> (≡) ==> (≡)) (IT_rec_1 P).
Proof. unfold IT_rec_1. solve_proper. Qed.

(* ▷-algebra *)
Definition IT_tick := IT_thunk ◎ NextO.
Definition IT_tick_n n := Nat.iter n IT_tick.
Global Instance IT_tick_n_ne n : NonExpansive (IT_tick_n n).
Proof. induction n; solve_proper. Qed.
Global Instance IT_tick_proper n : Proper ((≡) ==> (≡)) (IT_tick_n n).
Proof. induction n; solve_proper. Qed.

Lemma IT_tick_n_O α : IT_tick_n 0 α = α.
Proof. reflexivity. Qed.

Lemma IT_tick_n_S k α : IT_tick_n (S k) α = IT_tick (IT_tick_n k α).
Proof. reflexivity. Qed.

Lemma IT_tick_add k l α : IT_tick_n (k + l) α = IT_tick_n k (IT_tick_n l α).
Proof.
  induction k; first done.
  cbn[plus]. by rewrite !IT_tick_n_S IHk.
Qed.

Global Instance IT_tick_inj k : Inj (dist k) (dist (S k)) IT_tick.
Proof.
  intros x y. intros H1.
  assert (IT_unfold (IT_tick x) ≡{S k}≡ IT_unfold (IT_tick y)) as H2.
  { by rewrite H1. }
  revert H2. rewrite /IT_tick /=.
  rewrite !IT_unfold_fold. intros H2.
  apply (Next_inj (S k) x y); last lia.
  by eapply inr_ne_inj, inl_ne_inj.
Qed.

Global Instance IT_tick_contractive : Contractive IT_tick.
Proof. solve_contractive. Qed.

Section IT_destructors.
  Definition ignore1 {A : ofe} : A -n> IT :=
    λne _, IT_err.
  Definition ignore2 {A B : ofe} : A -n> B -n> IT :=
    λne _ _, IT_err.
  Program Definition get_nat (f : nat -> IT) : IT -n> IT
    := IT_rec_1 IT IT_err f (λne _, IT_thunk) ignore2 IT_unfold.
  Program Definition get_fun (f : laterO (IT -n> IT) -n> IT) : IT -n> IT
    := IT_rec_1 IT IT_err ignore1 (λne _, IT_thunk) (λne g _, f g) IT_unfold.
  Solve Obligations with solve_proper.

  Lemma get_nat_nat f n : get_nat f (IT_nat n) ≡ f n.
  Proof. by rewrite IT_rec_1_nat. Qed.
  Lemma get_nat_tick f t : get_nat f (IT_tick t) ≡ IT_tick (get_nat f t).
  Proof. by rewrite IT_rec_1_thunk. Qed.
  Lemma get_nat_tick_n f t n : get_nat f (IT_tick_n n t) ≡ IT_tick_n n (get_nat f t).
  Proof.
    induction n; first reflexivity.
    by rewrite get_nat_tick IHn.
  Qed.
  Lemma get_nat_fun f t : get_nat f (IT_arr t) ≡ IT_err.
  Proof. by rewrite IT_rec_1_arr. Qed.

  Lemma get_fun_fun f g : get_fun f (IT_arr g) ≡ f g.
  Proof. by rewrite IT_rec_1_arr. Qed.
  Lemma get_fun_tick f t : get_fun f (IT_tick t) ≡ IT_tick (get_fun f t).
  Proof. by rewrite IT_rec_1_thunk. Qed.
  Lemma get_fun_tick_n f t n : get_fun f (IT_tick_n n t) ≡ IT_tick_n n (get_fun f t).
  Proof.
    induction n; first reflexivity.
    by rewrite get_fun_tick IHn.
  Qed.

End IT_destructors.

Global Instance get_nat_ne n :
  Proper (pointwise_relation _ (dist n) ==> (dist n)) (get_nat ).
Proof. unfold get_nat. solve_proper. Qed.
Global Instance get_nat_proper :
  Proper ((≡@{natO -n> IT}) ==> (≡)) (get_nat ).
Proof. unfold get_nat. solve_proper. Qed.
Global Instance get_fun_ne : NonExpansive get_fun.
Proof.
  unfold get_fun.
  intros n F1 F2 HF. f_equiv.
  intros g ?; simpl. by f_equiv.
Qed.
Global Instance get_fun_ne2 : NonExpansive2 get_fun.
Proof.
  unfold get_fun.
  intros n F1 F2 HF x1 x2 Hx. repeat f_equiv; eauto.
  intros g ?; simpl. by f_equiv.
Qed.

Global Instance get_fun_proper :
  Proper ((≡) ==> (≡)) (get_fun).
Proof.
  unfold get_fun.
  intros F1 F2 HF. f_equiv.
  intros g ?; simpl. by f_equiv.
Qed.

Program Definition APP : IT -n> IT -n> IT := λne f1 f2,
    get_fun
      (λne g, IT_thunk $ later_ap g (Next f2))
      f1.
Next Obligation.
  intros _ f2 n g1 g2 Hg.
  by repeat f_equiv.
Qed.
Next Obligation.
  repeat intro; cbn-[IT_thunk  IT_arr mmuu get_nat get_fun];
  repeat f_equiv; eauto. intro g. simpl.
  by repeat f_equiv.
Qed.
Next Obligation.
  repeat intro; cbn-[IT_thunk  IT_arr mmuu get_nat get_fun];
  repeat f_equiv; eauto.
Qed.

Definition SUCC : IT -n> IT := get_nat (λ n, IT_nat (S n)).
Definition PRED : IT -n> IT := get_nat (λ n, IT_nat (pred n)).

(** ** injectivety/inversion lemmas *)
Global Instance IT_nat_inj n : Inj (=) (dist n) IT_nat.
Proof.
  intros x y. intros H1.
  assert (IT_unfold (IT_nat x) ≡{n}≡ IT_unfold (IT_nat y)) as H2.
  { by rewrite H1. }
  revert H2. rewrite /IT_nat /=.
  rewrite !IT_unfold_fold. intros H2.
  unfold_leibniz.
  eapply discrete_iff; first apply _.
  by eapply inl_ne_inj, inl_ne_inj, inl_ne_inj.
Qed.

Lemma IT_nat_inj' n m : IT_nat n ≡ IT_nat m ⊢ (⌜n = m⌝ : siProp).
Proof.
  iIntros "H1".
  iAssert (IT_unfold (IT_nat n) ≡ IT_unfold (IT_nat m))%I with "[H1]" as "H2".
  { by iRewrite "H1". }
  rewrite /IT_nat /=.
  rewrite !IT_unfold_fold.
  iDestruct "H2" as "%Hfoo".
  unfold_leibniz. iPureIntro.
  by eapply inl_equiv_inj, inl_equiv_inj, inl_equiv_inj.
Qed.

Lemma IT_nat_not_thunk n α :
  (IT_nat n ≡ IT_thunk α ⊢ False : siProp).
Proof.
  iIntros "H1".
  iAssert (IT_unfold (IT_nat n) ≡ IT_unfold (IT_thunk α))%I with "[H1]" as "H2".
  { by iRewrite "H1". }
  rewrite /IT_nat /IT_thunk /=.
  rewrite !IT_unfold_fold.
  iDestruct "H2" as "%Hfoo".
  exfalso.
  inversion Hfoo; simplify_eq/=.
  by inversion H1.
Qed.

Ltac simpl_me := cbn-[IT_thunk  IT_arr mmuu get_nat get_fun APP].

Ltac solve_proper_ish :=
  repeat intro; cbn-[IT_thunk  IT_arr mmuu get_nat get_fun];
  repeat f_equiv; eauto.

Lemma APP_tick_l α β : (APP (IT_tick α) β) ≡ IT_tick (APP α β).
Proof.
  rewrite /IT_tick /APP.
  simpl_me. rewrite get_fun_tick. reflexivity.
Qed.


Opaque IT_thunk IT_tick IT_nat.
