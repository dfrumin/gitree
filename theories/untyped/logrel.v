From Equations Require Import Equations.
From iris.algebra Require Import gmap.
From iris.algebra Require cofe_solver.
From iris.base_logic Require Export base_logic.
From iris.prelude Require Import options.
From iris.heap_lang Require Import lang primitive_laws.
From gitree Require Import prelude pcf.
From gitree.untyped Require Export scott domain.
From iris.prelude Require Import options.
From iris.si_logic Require Import bi siprop.
From iris.proofmode Require Import tactics.

(** ** Preliminary: *)
(* register typeclasses for the proofmode *)
Global Instance into_laterN_tick only_head n n' x y :
  nat_cancel.NatCancel n 1 n' 0 →
  IntoLaterN (PROP:=siPropI) only_head n (IT_tick x ≡ IT_tick y) (x ≡ y) | 2.
Proof. apply into_laterN_inj. apply _. Qed.

Global Instance from_modal_tick x y      {PROP : bi} `{!BiInternalEq PROP} :
  FromModal (PROP1:=PROP) (PROP2:=PROP) True (modality_instances.modality_laterN 1)
    (▷ (x ≡ y) : PROP)%I (IT_tick x ≡ IT_tick y) (x ≡ y).
Proof. apply from_modal_contractive. apply _. Qed.


(** * Logical relation *)

Canonical Structure termO Γ τ := leibnizO (tm Γ τ).

(** Case for the natural nubmers *)
Program Definition logrel_nat_pre {Γ}
        (self : termO Γ tnat -n> IT -n> siPropO)
 : termO Γ tnat -n> IT -n> siPropO := λne M t,
  ((∃ (n : nat), t ≡ IT_nat n ∧ ⌜step_beta_clos M 0 (Num n)⌝)
   ∨ (∃ (β : IT) (M' M'' : tm Γ tnat),
         t ≡ IT_tick β ∧ ⌜step_beta_clos M 0 M'⌝ ∧ ⌜step_beta M' 1 M''⌝ ∧
         ▷ self M'' β))%I.
Next Obligation.
Opaque later_ap. solve_proper. Qed.
Next Obligation. solve_proper. Qed.

Global Instance logrel_nat_pre_contractive {Γ} :
  Contractive (@logrel_nat_pre Γ).
Proof. solve_contractive. Qed.

Definition logrel_nat {Γ} := fixpoint (@logrel_nat_pre Γ).

Lemma logrel_nat_unfold {Γ} M α :
  logrel_nat M α ≡
   ((∃ (n : nat), α ≡ IT_nat n ∧ ⌜step_beta_clos M 0 (Num n)⌝)
     ∨ (∃ (β : IT) (M' M'' : tm Γ tnat),
         α ≡ IT_tick β ∧ ⌜step_beta_clos M 0 M'⌝ ∧ ⌜step_beta M' 1 M''⌝ ∧
         ▷ logrel_nat M'' β))%I.
Proof.
  unfold logrel_nat.
  apply (fixpoint_unfold logrel_nat_pre M _).
Qed.

Global Instance logrel_nat_proper {Γ} :
  Proper ((≡) ==> (≡)) ((@logrel_nat Γ)).
Proof. solve_proper. Qed.

Global Instance logrel_nat_ne {Γ} n :
  Proper ((dist n) ==> ((dist n))) ((@logrel_nat Γ)).
Proof. solve_proper. Qed.


(** Definition by recursion on the type *)
Fixpoint logrel {Γ} (τ : ty) : tm Γ τ → IT → siProp :=
  match τ with
  | tnat => λ M x, logrel_nat M x
  | tarr τ1 τ2 => λ M α, ∀ (N : tm Γ τ1) (β : IT),
        ▷ logrel τ1 N β → logrel τ2 (M · N) (APP α β)
  end%I.


#[export] Instance logrel_proper {Γ} τ :
  Proper ((≡) ==> (≡) ==> (≡)) (@logrel Γ τ).
Proof. induction τ; simpl; solve_proper. Qed.
#[export] Instance logrel_ne {Γ} τ n :
  Proper ((dist n) ==> (dist n) ==> (dist n)) (@logrel Γ τ).
Proof. induction τ; simpl; solve_proper. Qed.

(** ** Additional rules for the logical relation.
Kind of "symbolic execution" rules.
*)
Program Definition logrel_step_0 Γ (τ : ty) (M N : tm Γ τ) (Hst : step_beta M 0 N) :
  (⊢ ∀ α : IT, logrel τ N α → logrel τ M α)%I.
Proof.
  induction τ.
  - simpl. iIntros (α).
    rewrite logrel_nat_unfold.
    iDestruct 1 as "[H | H]".
    + iDestruct "H" as (n) "[Heq H]".
      rewrite logrel_nat_unfold. iLeft.
      iExists n. iFrame. iDestruct "H" as %Hstep.
      iPureIntro. change 0 with (0+0); econstructor; eauto.
    + iDestruct "H" as (β M' M'') "(Heq & %HM & %HM'' & H)".
      rewrite (logrel_nat_unfold M α). iRight.
      iExists β, M', M''. repeat iSplit; eauto.
      iPureIntro. change 0 with (0+0).
      econstructor; eauto.
  - iIntros (α) "HN". simpl.
    iIntros (β M') "HM'".
    iSpecialize ("HN" with "HM'").
    iApply IHτ2; eauto.
    by econstructor.
Qed.

Program Definition logrel_step_1 Γ (τ : ty) (M N : tm Γ τ) (Hst : step_beta M 1 N) :
  (⊢ ∀ α : IT, ▷ logrel τ N α → logrel τ M (IT_tick α))%I.
Proof.
  induction τ.
  - simpl. iIntros (α).
    iIntros "H".
    rewrite (logrel_nat_unfold M). iRight.
    iExists α, M, N. repeat iSplit; eauto.
    { iPureIntro. econstructor. }
  - iIntros (α) "H". cbn[logrel]. iIntros (P β) "H2".
    iSpecialize ("H" with "[H2]").
    { iNext. iExact "H2". }
    iPoseProof (IHτ2 (M · P) (N · P)) as "Hs".
    { econstructor; eauto. }
    rewrite APP_tick_l.
    iApply ("Hs" with "H").
Qed.

(** * Adequacy *)
Lemma logrel_adequacy (M : tm [] tnat) (n k : nat) :
  (⊢ logrel tnat M (IT_tick_n k (IT_nat n)))
  → step_beta_clos M k (Num n).
Proof.
  intros Hlog.
  cut (⊢ ▷^k ⌜step_beta_clos M k (Num n)⌝ : siProp)%I.
  { intros HH.
    eapply siProp.pure_soundness.
    by eapply (laterN_soundness _ k). }
  iPoseProof Hlog as "H". clear Hlog.
  iInduction k as [|k] "IH" forall (M).
  - iSimpl in "H". rewrite logrel_nat_unfold.
    iDestruct "H" as "[H | H]".
    + iDestruct "H" as (m) "[Hunit %]".
      rewrite IT_nat_inj'. iDestruct "Hunit" as %->.
      iPureIntro; eauto.
    + iDestruct "H" as (β M' M'') "(Hβ & %HM & %HM' & H)".
      by rewrite IT_nat_not_thunk.
  - iSimpl in "H". rewrite logrel_nat_unfold.
    iDestruct "H" as "[H | H]".
    + iDestruct "H" as (n0) "[Heq1 %]". iExFalso.
      iExFalso. iAssert (IT_nat n0 ≡ IT_tick_n (S k) (IT_nat n))%I with "[Heq1]" as "H".
      { by iRewrite -"Heq1". }
      by rewrite IT_nat_not_thunk.
    + iDestruct "H" as (β M' M'') "(Hβ & %HM & %HM' & H)".
      iNext. iRewrite -"Hβ" in "H".
      iSpecialize ("IH" with "H"). iNext.
      iDestruct "IH" as %IH. iPureIntro.
      (* need some computations of step_clos *)
      eapply step_beta_clos_trans; eauto.
      change (S k) with (1 + k). econstructor; eauto.
Qed.

(** * Fundamental property *)

(** Compatibility lemmas *)
Lemma interp_Y_app (α : IT) :
  interp_Y_ish α  ≡ APP α (interp_Y_ish α).
Proof.
  rewrite {1}/interp_Y_ish.
  etrans.
  { eapply (mmuu_unfold interp_Y_pre). }
  cbn-[interp_Y_ish IT_tick get_fun mmuu interp_Y_ish].
  repeat f_equiv. intro f.
  simpl_me. reflexivity.
Qed.

Lemma compat_Y Γ (σ : ty) (M : tm Γ (tarr σ σ)) α :
  ⊢ logrel (tarr σ σ) M α → logrel σ (Y M) (interp_Y_ish α).
Proof.
  iIntros "#HM".
  iLöb as "IH".
  iApply logrel_step_0.
  { econstructor. }
  simpl_me. rewrite {2}(interp_Y_app α).
  by iApply "HM".
Qed.

Lemma compat_app Γ (σ σ' : ty) (M : tm Γ (tarr σ σ')) (N : tm Γ σ) α β :
  ⊢ logrel _ M α → ▷ logrel _ N β → logrel _ (M · N) (APP α β).
Proof.
  iIntros "#HM #HN".
  simpl_me. by iApply "HM".
Qed.

Lemma compat_succ Γ (M : tm Γ tnat) α :
  ⊢ logrel tnat M α → logrel _ (Succ M) (SUCC α).
Proof.
  iLöb as "IH" forall (M α).
  iIntros "#H".
  simpl_me.
  rewrite {1}logrel_nat_unfold. iDestruct "H" as "[H1|H1]".
  + iDestruct "H1" as (n) "[Ht %Hn]".
    iRewrite "Ht". rewrite (logrel_nat_unfold (Succ _)). iLeft.
    iExists (S n). iSplit; last first.
    { iPureIntro. eapply (step_beta_clos_trans _ (Succ (Num n))); eauto.
      - eapply step_beta_close_Succ; eauto.
      - eapply step_beta_clos_once; econstructor. }
    by rewrite /SUCC get_nat_nat.
  + iDestruct "H1" as (β M' M'') "[Ht [%HM [%HM' H1]]]".
    rewrite (logrel_nat_unfold (Succ M)). iRight.
    iExists (SUCC β), (Succ M'), (Succ M'').
    repeat iSplit; eauto.
    * iRewrite "Ht". by rewrite /SUCC get_nat_tick.
    * iPureIntro. eapply step_beta_close_Succ; eauto.
    * iPureIntro. by econstructor.
    * iNext. by iApply "IH".
Qed.


Lemma compat_pred Γ (M : tm Γ tnat) α :
  ⊢ logrel tnat M α → logrel _ (Pred M) (PRED α).
Proof.
  iLöb as "IH" forall (M α).
  iIntros "#H".
  simpl_me.
  rewrite {1}logrel_nat_unfold. iDestruct "H" as "[H1|H1]".
  + iDestruct "H1" as (n) "[Ht %Hn]".
    iRewrite "Ht". rewrite (logrel_nat_unfold (Pred _)). iLeft.
    iExists (pred n). iSplit; last first.
    { iPureIntro. eapply (step_beta_clos_trans _ (Pred (Num n))); eauto.
      - eapply step_beta_close_Pred; eauto.
      - eapply step_beta_clos_once; econstructor. }
    by rewrite /PRED get_nat_nat.
  + iDestruct "H1" as (β M' M'') "[Ht [%HM [%HM' H1]]]".
    rewrite (logrel_nat_unfold (Pred _)). iRight.
    iExists (PRED β), (Pred M'), (Pred M'').
    repeat iSplit; eauto.
    * iRewrite "Ht". by rewrite /PRED get_nat_tick.
    * iPureIntro. eapply step_beta_close_Pred; eauto.
    * iPureIntro. by econstructor.
    * iNext. by iApply "IH".
Qed.

(** * Fundamental property *)

Inductive subs2 : list ty → Type :=
| emp_subs2 : subs2 []
| cons_subs2 {Γ τ} :
    forall (t : tm [] τ) (α : IT),
    subs2 Γ →
    subs2 (τ::Γ)
.

Equations subs_of_subs2 {Γ} (ss : subs2 Γ) : subs Γ [] :=
  subs_of_subs2 emp_subs2 _ v => idsub _ v;
  subs_of_subs2 (cons_subs2 t α ss) _ Vz := t;
  subs_of_subs2 (cons_subs2 t α ss) _ (Vs v) := subs_of_subs2 ss _ v.

Lemma subs_of_emp_subs2 : subs_of_subs2 emp_subs2 ≡ idsub.
Proof. intros τ v. dependent elimination v. Qed.

Equations interp_ctx_of_subs2 {Γ} (ss : subs2 Γ) : interp_ctx Γ :=
  interp_ctx_of_subs2 emp_subs2 := ();
  interp_ctx_of_subs2 (cons_subs2 t α ss) := (α, interp_ctx_of_subs2 ss).

Equations list_of_subs2 {Γ} (ss : subs2 Γ) : list ({ τ : ty & (tm [] τ * IT)%type}) :=
  list_of_subs2 emp_subs2 := [];
  list_of_subs2 (@cons_subs2 _ τ t α ss) := (existT τ (t, α))::list_of_subs2 ss.

Definition subs2_valid {Γ} (ss : subs2 Γ) : siProp :=
  ([∧ list] s∈(list_of_subs2 ss), logrel (projT1 s) (fst (projT2 s)) (snd (projT2 s)))%I.


Lemma logrel_fundamental Γ τ (t : tm Γ τ) :
  ⊢ ∀ ss : subs2 Γ, subs2_valid ss →
                  logrel τ (subst_tm t (subs_of_subs2 ss))
                           (interp_tm t (interp_ctx_of_subs2 ss)).
Proof.
  induction t.
  - (* Nat const *)
    iIntros (ss) "Hss". simpl.
    rewrite logrel_nat_unfold. iLeft.
    iExists n. simp subst_tm interp_tm. simpl.
    iSplit; eauto. iPureIntro. econstructor.
  - (* Var *)
    iIntros (ss) "Hss".
    simp subst_tm interp_tm.
    iInduction ss as [|] "IH"; dependent elimination v.
    + rewrite /subs2_valid. simp list_of_subs2. simpl.
      iDestruct "Hss" as "[H Hss]".
      simp subs_of_subs2 subst_var.
      simp interp_ctx_of_subs2. simpl.
      simp interp_var. simpl. done.
    + rewrite /subs2_valid. simp list_of_subs2. simpl.
      iDestruct "Hss" as "[H Hss]".
      simp subs_of_subs2 interp_var.
      simp interp_ctx_of_subs2. simpl.
      by iApply "IH".
  - (* Lam *)
    iIntros (ss) "Hss". simpl_me.
    iIntros (N β) "HN".
    simp subst_tm. simp interp_tm.
    unfold APP. simpl_me.
    rewrite get_fun_fun. simpl_me.
    Transparent later_ap.
    unfold later_ap. simpl_me.
    Opaque later_ap.
    iApply logrel_step_1.
    { econstructor. } iNext.
    unfold subst1.
    rewrite -subst_comp_s.
    assert ((β, interp_ctx_of_subs2 ss) ≡
        (interp_ctx_of_subs2 (cons_subs2 N β ss))) as Hsbs1.
    { by simp interp_ctx_of_subs2. }
    rewrite Hsbs1.
    assert ((comp_s (subs_lift (subs_of_subs2 ss)) {|N|})
              ≡ subs_of_subs2 (cons_subs2 N β ss)) as Hsbs2.
    { clear. unfold comp_s. intros ? v.
      dependent elimination v;
        simp subs_of_subs2 subs_lift subst_tm; first done.
      unfold tm_lift.
      rewrite -subst_comp_r_s.
      assert ((comp_r_s (λ (τ : ty) (v0 : var [] τ), Vs v0) {|N|})
                ≡ idsub) as ->.
      { clear. intros ? v. unfold comp_r_s.
        reflexivity. }
      apply subst_tm_id. }
    rewrite (subst_tm_proper _ _ _ t _ _ Hsbs2).
    iApply IHt. rewrite /subs2_valid.
    simp list_of_subs2. simpl; eauto.
  - (* App *)
    iIntros (ss) "#Hss".
    simp subst_tm interp_tm. simpl_me.
    iApply compat_app.
    + by iApply IHt1.
    + by iApply IHt2.
  - (* Succ *)
    iIntros (ss) "#Hss".
    simp subst_tm interp_tm. simpl_me.
    iPoseProof (IHt with "Hss") as "H1".
    iSimpl in "H1".
    iApply (compat_succ with "H1").
  - (* Pred *)
    iIntros (ss) "#Hss". simpl.
    simp subst_tm interp_tm. simpl.
    iPoseProof (IHt with "Hss") as "H1".
    iSimpl in "H1".
    iApply (compat_pred with "H1").
  - (* Y *)
    iIntros (ss) "#Hss".
    simp subst_tm interp_tm. simpl.
    iApply compat_Y.
    by iApply IHt.
Qed.

Theorem interp_adequacy (M : tm [] tnat) (k n : nat) :
  interp_tm M () ≡ IT_tick_n k (IT_nat n) →
  step_beta_clos M k (Num n).
Proof.
  intros H.
  apply logrel_adequacy.
  rewrite -H.
  iPoseProof (logrel_fundamental [] tnat M) as "H".
  iSpecialize ("H" $! emp_subs2 with "[]").
  { rewrite /subs2_valid. simp list_of_subs2. by simpl. }
  simp subs_of_subs2 interp_ctx_of_subs2.
  assert (subst_tm M (subs_of_subs2 emp_subs2) = subst_tm M idsub) as ->.
  { by rewrite subs_of_emp_subs2. }
  by rewrite subst_tm_id.
Qed.

(** * Unary logical predicate *)

Program Definition safe_nat_pre (self : IT -n> siPropO) :
  IT -n> siPropO := λne t,
  ((∃ (n : nat), t ≡ IT_nat n)
   ∨ (∃ (β : IT),
         t ≡ IT_tick β ∧ ▷ self β))%I.
Next Obligation.
Opaque later_ap. solve_proper. Qed.

Global Instance safe_nat_pre_contractive :
  Contractive (safe_nat_pre).
Proof. solve_contractive. Qed.

Definition safe_nat := fixpoint (safe_nat_pre).

Global Instance safe_nat_proper :
  Proper ((≡) ==> (≡)) (safe_nat ).
Proof. solve_proper. Qed.

Global Instance safe_nat_ne n :
  Proper ((dist n) ==> ((dist n))) (safe_nat).
Proof. solve_proper. Qed.

Fixpoint safe (τ : ty) {struct τ} : IT → siPropO :=
  match τ with
  | tnat => safe_nat
  | tarr τ1 τ2 => λ M,
      ∀ N, ▷ safe τ1 N →
           safe τ2 (APP M N)
  end%I.

Global Instance safe_ne {τ} : NonExpansive (safe τ).
Proof.
  induction τ; simpl; solve_proper.
Qed.
Global Instance safe_proper {τ} : Proper ((≡) ==> (≡)) (safe τ).
Proof. apply ne_proper. apply _. Qed.

Lemma safe_nat_unfold t :
  safe_nat t ≡
           ((∃ (n : nat), t ≡ IT_nat n)
            ∨ (∃ (β : IT),
                  t ≡ IT_tick β ∧ ▷ safe_nat β))%I.
Proof.
  unfold safe_nat.
  apply (fixpoint_unfold safe_nat_pre t).
Qed.

Lemma safe_nat_succ t :
  ⊢ safe_nat t → safe_nat (SUCC t).
Proof.
  iLöb as "IH" forall (t).
  rewrite (safe_nat_unfold t).
  iDestruct 1 as "[[%n H] | bH]".
  - iRewrite "H". rewrite safe_nat_unfold.
    iLeft. iExists (S n).
    rewrite /SUCC. rewrite get_nat_nat.
    done.
  - iDestruct "bH" as (t') "[Ht' H]".
    iRewrite "Ht'". rewrite (safe_nat_unfold (SUCC _)).
    iRight. iExists (SUCC t').
    iSplit.
    + rewrite /SUCC. rewrite get_nat_tick. done.
    + iNext. by iApply "IH".
Qed.

Lemma safe_nat_pred t :
  ⊢ safe_nat t → safe_nat (PRED t).
Proof.
  iLöb as "IH" forall (t).
  rewrite (safe_nat_unfold t).
  iDestruct 1 as "[[%n H] | bH]".
  - iRewrite "H". rewrite safe_nat_unfold.
    iLeft. iExists (pred n).
    rewrite /PRED. rewrite get_nat_nat.
    done.
  - iDestruct "bH" as (t') "[Ht' H]".
    iRewrite "Ht'". rewrite (safe_nat_unfold (PRED _)).
    iRight. iExists (PRED t').
    iSplit.
    + rewrite /PRED. rewrite get_nat_tick. done.
    + iNext. by iApply "IH".
Qed.

Inductive subs1 : list ty → Type :=
| emp_subs1 : subs1 []
| cons_subs1 {Γ τ} :
    forall (α : IT),
    subs1 Γ →
    subs1 (τ::Γ)
.

Equations interp_ctx_of_subs1 {Γ} (ss : subs1 Γ) : interp_ctx Γ :=
  interp_ctx_of_subs1 emp_subs1 := ();
  interp_ctx_of_subs1 (cons_subs1 α ss) := (α, interp_ctx_of_subs1 ss).

Equations list_of_subs1 {Γ} (ss : subs1 Γ) : list (ty * IT) :=
  list_of_subs1 emp_subs1 := [];
  list_of_subs1 (@cons_subs1 _ τ α ss) := (τ,α)::list_of_subs1 ss.

Definition subs1_valid {Γ} (ss : subs1 Γ) : siProp :=
  ([∧ list] s∈(list_of_subs1 ss), safe (fst s) (snd s))%I.

Lemma safe_tick τ α :
  ⊢ ▷ safe τ α → safe τ (IT_tick α).
Proof.
  (* iLöb as "IH". *)
  revert α. induction τ=>α.
  - (* Nat *)
    simpl_me.
    iIntros "H".
    rewrite (safe_nat_unfold (IT_tick _)).
    iRight. iExists α. iSplit; eauto.
  - iIntros "H1". iEval (simpl_me). iIntros (N) "H2".
    rewrite /APP. simpl_me. rewrite get_fun_tick.
    iApply IHτ2. iNext.
    iApply ("H1" with "H2").
Qed.

Lemma interp_Y_ish_unfold α :
  interp_Y_ish α ≡ APP α (interp_Y_ish α).
Proof.
  etrans.
  { rewrite /interp_Y_ish.
    eapply (mmuu_unfold interp_Y_pre _). }
  unfold interp_Y_pre, APP.
  simpl_me. f_equiv. f_equiv. intro f. simpl_me.
  reflexivity.
Qed.

Lemma safe_Y τ α :
  ⊢ safe (tarr τ τ) α → safe τ (interp_Y_ish α).
Proof.
  iIntros "#H".
  iLöb as "IH".
  rewrite /interp_Y_ish.
  rewrite {2}interp_Y_ish_unfold.
  iApply "H". iNext. iApply "IH".
Qed.

Lemma safe_fundamental Γ τ (t : tm Γ τ) :
  ⊢ ∀ ss : subs1 Γ, subs1_valid ss →
                    safe τ (interp_tm t (interp_ctx_of_subs1 ss)).
Proof.
  induction t.
  - (* Nat const *)
    iIntros (ss) "Hss". simpl.
    rewrite safe_nat_unfold. iLeft.
    iExists n. simp interp_tm. simpl.
    done.
  - (* Var *)
    iIntros (ss) "Hss".
    simp interp_tm. cbn.
    iInduction ss as [|] "IH"; dependent elimination v.
    + rewrite /subs1_valid. simp list_of_subs1. simpl.
      iDestruct "Hss" as "[H Hss]".
      simp interp_ctx_of_subs1 interp_var. simpl.
      done.
    + rewrite /subs1_valid. simp list_of_subs1. simpl.
      iDestruct "Hss" as "[H Hss]".
      simp interp_ctx_of_subs1 interp_var. simpl.
      by iApply "IH".
  - (* Lam *)
    iIntros (ss) "Hss". simpl_me.
    iIntros (β) "Hβ".
    simp interp_tm.
    rewrite /APP; simpl_me.
    rewrite get_fun_fun. simpl_me.
    iApply safe_tick. iNext. simpl_me.
    assert ((β, interp_ctx_of_subs1 ss) ≡
        (interp_ctx_of_subs1 (@cons_subs1 _ τ1 β ss))) as ->.
    { by simp interp_ctx_of_subs1. }
    iApply IHt.
    unfold subs1_valid; simp list_of_subs1.
    simpl ; eauto.
  - (* App *)
    iIntros (ss) "#Hss". simpl_me.
    simp subst_tm interp_tm. simpl_me.
    iPoseProof (IHt1 with "Hss") as "H1".
    iPoseProof (IHt2 with "Hss") as "H2".
    iApply ("H1" with "H2").
  - (* Succ *)
    iIntros (ss) "#Hss". simpl.
    simp subst_tm interp_tm. simpl_me.
    iApply safe_nat_succ. by iApply IHt.
  - (* Pred *)
    iIntros (ss) "#Hss". simpl.
    simp subst_tm interp_tm. simpl_me.
    iApply safe_nat_pred. by iApply IHt.
  - (* Y *)
    iIntros (ss) "#Hss".
    simp subst_tm interp_tm. simpl_me.
    iApply safe_Y. by iApply IHt.
Qed.

Global Instance IT_err_discrete : Discrete IT_err.
Proof. Abort.

Global Instance IT_nat_discrete n : Discrete (IT_nat n).
Proof. Abort.

Lemma nat_is_nat (t : tm [] tnat) (k : nat) :
  ⊢@{siPropI} ¬ (IT_tick_n k IT_err ≡ interp_tm t ()).
Proof.
  (* eapply siProp.pure_soundness. *)
  iAssert (safe tnat (interp_tm t ())) as "H".
  { admit. }
  simpl. generalize ((interp_tm t ()))=>α. clear t.
  iRevert "H". iLöb as "IH" forall (α k). iIntros "#H".
  rewrite safe_nat_unfold.
  iDestruct "H" as "[H | H]".
  { iDestruct "H" as (n) "H". iRewrite "H".
    iIntros "S". admit. }
  iDestruct "H" as (β) "[H Hl]".
  iInduction (k) as [|k] "IHk".
  - simpl_me. iRewrite "H".
    admit.
  - rewrite IT_tick_n_S.
    iSpecialize ("IH" with "Hl").
    iRewrite "H". iIntros "S".
    rewrite inj_later_eq.
Abort.
